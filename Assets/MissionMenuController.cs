﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissionMenuController : MonoBehaviour
{
    public void OpenMissionsMenu()
    {
        string pathToResource = "GlobalWindows/MissionHandler";
        Instantiate(Resources.Load(pathToResource));
    }
}
