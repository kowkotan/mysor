﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class RobotPickupTriggerEffect : MonoBehaviour
{
    RobotPickupTrigger robotPickupTrigger;


    void Start()
    {
        robotPickupTrigger = GetComponent<RobotPickupTrigger>();
        robotPickupTrigger.PickupAction += RobotPickupTrigger_PickupAction;
    }

    void RobotPickupTrigger_PickupAction(Transform pickupedItem)
    {
        Vector3 offset = new Vector3(0, 1.0f, 0); // to hide 2d object against the robot
        pickupedItem.DOMove(transform.position + offset, 0.5f).OnComplete( ()=>
        {
            Destroy(pickupedItem.gameObject);
        });
    }

    private void OnDestroy()
    {
        robotPickupTrigger.PickupAction -= RobotPickupTrigger_PickupAction; ;
    }
}
