﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Adic;

public class IAPBuyButton : MonoBehaviour
{

    public enum ItemType
    {
        diamond_20,
        diamond_55,
        diamond_240,
        diamond_750,
        diamond_2000
    }

    public enum SocialType
    {
        Mobiles,
        MM,
        OK,
        VK
    }

    public ItemType itemType;
    public SocialType socialType;
    public Text priceText;
    private string defaultText;

    [Header("Info")]
    public string title;
    public string stackSize;
    [Header("Platforms Price String")]
    public string mobilePriceString;
    public string mmPriceString;
    public string okPriceString;
    public string vkPriceString;
    [Header("Social Price")]
    public int mailiki_price;
    public int vk_price;
    public int ok_price;

    [Header("Sources")]
    [SerializeField] Text itemTitle;
    [SerializeField] Text stackSizeText;
    [SerializeField] AudioSource audioSource;

    private void Start() {
        Init();
        defaultText = priceText.text;
        //StartCoroutine(LoadPriceRoutine());
    }

    void Init()
    {
        itemTitle.text = title;
        stackSizeText.text = stackSize;
        if(socialType == SocialType.Mobiles)
            priceText.text = mobilePriceString;
        else if(socialType == SocialType.MM)
            priceText.text = mmPriceString;
        else if(socialType == SocialType.OK)
            priceText.text = okPriceString;
        else if(socialType == SocialType.VK)
            priceText.text = vkPriceString;
    }

    public void CallShopWindow(int _id)
    {
        if(socialType == SocialType.MM)
            Application.ExternalCall("ShowShop", _id, title, mailiki_price);
        else if(socialType == SocialType.OK)
            Application.ExternalCall("showPayment", title, "Покупка", _id, ok_price);
        else if(socialType == SocialType.VK)
            Application.ExternalCall("ShowShop", _id, title, vk_price);
    }

    public void ClickBuy()
    {
        switch(itemType)
        {
            case ItemType.diamond_20:
                IAPManager.Instance.Buy20Diamonds();
                //ShowPaymentDialog(1, "test_payment", 1);
                break;
            case ItemType.diamond_55:
                IAPManager.Instance.Buy55Diamonds();
                break;
            case ItemType.diamond_240:
                IAPManager.Instance.Buy240Diamonds();
                break;
            case ItemType.diamond_750:
                IAPManager.Instance.Buy750Diamonds();
                break;
            case ItemType.diamond_2000:
                IAPManager.Instance.Buy2000Diamonds();
                break;
        }
        audioSource.Play();
    }

    // private IEnumerator LoadPriceRoutine()
    // {
    //     while(IAPManager.Instance.IsInitialized())
    //         yield return null;

    //     string loadedPrice = "";

    //     switch(itemType)
    //     {
    //         case ItemType.diamond_20:
    //             loadedPrice = IAPManager.Instance.GetProductPriceFromStore(IAPManager.Instance.diamond_20);
    //             break;
    //         case ItemType.diamond_55:
    //             loadedPrice = IAPManager.Instance.GetProductPriceFromStore(IAPManager.Instance.diamond_55);
    //             break;
    //         case ItemType.diamond_240:
    //             loadedPrice = IAPManager.Instance.GetProductPriceFromStore(IAPManager.Instance.diamond_240);
    //             break;
    //         case ItemType.diamond_750:
    //             loadedPrice = IAPManager.Instance.GetProductPriceFromStore(IAPManager.Instance.diamond_750);
    //             break;
    //         case ItemType.diamond_2000:
    //             loadedPrice = IAPManager.Instance.GetProductPriceFromStore(IAPManager.Instance.diamond_2000);
    //             break;
    //     }

    //     priceText.text = loadedPrice;
    // }
}
