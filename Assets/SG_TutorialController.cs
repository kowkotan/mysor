﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class SG_TutorialController : MonoBehaviour
{
    [SerializeField]
    SG_Transporter transporter;

    [SerializeField]
    SG_TrashContainer foodContainer;

    Transform tutorTrash;

    [SerializeField]
    Transform tutFinger;

    [Inject] SG_MinigameSystem minigameSystem;

    [SerializeField] Button factoryMapBtn;
    [SerializeField] Button sgBtn;

    [SerializeField] GameObject[] lockedContainers;
    [SerializeField] GameObject tutorMask;

    bool tutStep1;

    private void Start()
    {
        this.Inject();

        minigameSystem.OnMiniGameEnd += MinigameSystem_OnMiniGameEnd;

        if (PlayerPrefs.GetInt("sortingTutorialStep1Completed") == 1)
            return;
        else
        {
            for(int i = 0; i < lockedContainers.Length; i++)
            {
                lockedContainers[i].GetComponent<BoxCollider2D>().enabled = false;
            }
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (PlayerPrefs.GetInt("sortingTutorialStep1Completed") == 1)
        {
            GetComponent<Collider2D>().enabled = false;
            return;
        } 

        if (collision.GetComponent<SG_TrashObject>() == null || tutStep1 == true) return;

        transporter.Stop();

        TutorialLoader.instance.Load("sg1");

        TutorialController.instance.HideNextButton();

        TutorialEvents.OnTutorialComplete.AddListener(Tut1Comp);


        if(PlayerPrefs.GetInt("ItemDragBeforeTut") == 1)
            tutorTrash = GameObject.Find("to1").transform;
        else tutorTrash = GameObject.Find("to1").transform;

        tutFinger.gameObject.SetActive(true);
        tutorMask.SetActive(false);
        


        for(int i = 0; i < lockedContainers.Length; i++)
        {
            lockedContainers[i].GetComponent<BoxCollider2D>().enabled = false;
        }

        tutFinger.position = tutorTrash.position;

        foodContainer.OnPickuped += OnPickuped;

        tutStep1 = true;

        MoveFingerToContainer();

    }

    void Tut1Comp()
    {
        transporter.Resume();
        TutorialEvents.OnTutorialComplete.RemoveListener(Tut1Comp);
        tutStep1 = false;
        tutFinger.gameObject.SetActive(false);
        tutorMask.SetActive(true);
        foodContainer.OnPickuped -= OnPickuped;
        PlayerPrefs.SetInt("sortingTutorialStep1Completed", 1);
    }


    void MinigameSystem_OnMiniGameEnd()
    {
        if (PlayerPrefs.GetInt("sortingTutorialStep2Completed") == 1)
        {
            return;
        }


        minigameSystem.OnMiniGameEnd -= MinigameSystem_OnMiniGameEnd;

        TutorialLoader.instance.Load("sg3");
        sgBtn.interactable = false;
        TutorialController.instance.HideNextButton();
        PlayerPrefs.SetInt("sortingTutorialStep2Completed", 1);

        PlayerPrefs.SetInt("factoryTutorialPart2Ready", 1);
        for(int i = 0; i < lockedContainers.Length; i++)
        {
            lockedContainers[i].GetComponent<BoxCollider2D>().enabled = true;
        }
    }

    void OnPickuped()
    {
        TutorialController.instance.Next();
    }

    void MoveFingerToContainer()
    {
        if (!tutStep1) return;

        tutFinger.DOMove(foodContainer.transform.position, 1.5f).OnComplete(() =>
        {
            tutFinger.position = tutorTrash.position;
            MoveFingerToContainer();

        });
    }


}
