﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BoxState
{
    HIDDEN,
    CLOSED,
    OPENED
}
public class SurBoxUI : MonoBehaviour
{
    [SerializeField]
    GameObject hiddenState;

    [SerializeField]
    GameObject openState;

    [SerializeField]
    GameObject closedState;

    [SerializeField]
    GameObject prizePlatform;

    [SerializeField]
    SurBoxPrizeUI boxPrize;

    [SerializeField]
    float openBoxTime = 0.8f;
    [SerializeField]
    float showPrizeDelay = 0.5f;

   public  BoxState currentState;

    private void Start()
    {
        SetState(BoxState.HIDDEN);
    }

    public void UnlockBox()
    {
        SetState(BoxState.CLOSED);
        Invoke("OpenBox", openBoxTime);
    }

    void OpenBox()
    {
        SetState(BoxState.OPENED);
    }

    void SetState( BoxState _boxState)
    {
        currentState = _boxState;
        openState.SetActive(false);
        closedState.SetActive(false);
        hiddenState.SetActive(false);
        prizePlatform.SetActive(false);

        switch (_boxState)
        {
            case BoxState.HIDDEN:
                hiddenState.SetActive(true);
                break;
            case BoxState.CLOSED:
                closedState.SetActive(true);
                break;
            case BoxState.OPENED:
                openState.SetActive(true);
                Invoke("ShowPrize", showPrizeDelay);
                break;
        }
    }

    public void ShowPrize()
    {
        prizePlatform.SetActive(true);
        boxPrize.ShowPrize();
    }
}
