﻿using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;
using Framework.Utility;
using Framework.Interfaces;

public enum PrizeType
{
    SOFT_VALUE,
    HARD_VALUE,
    ENERGY_VALUE,
    DETAILS_VALUE
}

[System.Serializable]
public struct PrizeFromBox:IRandom
{
    public PrizeType prizeType;
    public Sprite icon;
    public float chance;
   public int count;

    public float returnChance => chance;
}

public class SurBoxPrizeUI : MonoBehaviour
{
    [SerializeField] List<PrizeFromBox> prizeList = new List<PrizeFromBox>();

    [SerializeField] Text prizeCountText;
    [SerializeField] Image prizeIcon;

    [Inject] HardValueModel hardValueModel;
    [Inject] SoftValueModel softValueModel;
    [Inject] EnergyModel energyModel;
    [Inject] DetalsModel detalsModel;

    PrizeFromBox prizeFromBox;


     void Start()
    {
        this.Inject();
        GiveReward();
    }

    public void ShowPrize()
    {
        prizeFromBox = GetRandomPrize();

        prizeCountText.text = prizeFromBox.count.ToString();
        prizeIcon.sprite = prizeFromBox.icon;

       
    }

    public void GiveReward()
    {
        switch (prizeFromBox.prizeType)
        {
            case PrizeType.DETAILS_VALUE:
                detalsModel.AddValue(prizeFromBox.count);
                break;
            case PrizeType.ENERGY_VALUE:
                energyModel.AddEnergy(prizeFromBox.count);
                break;
            case PrizeType.HARD_VALUE:
                hardValueModel.AddValue(prizeFromBox.count);
                break;
            case PrizeType.SOFT_VALUE:
                softValueModel.AddValue(prizeFromBox.count);
                break;

        }
    }

    PrizeFromBox GetRandomPrize()
    {
        return prizeList.Select<PrizeFromBox>();
    }
}
