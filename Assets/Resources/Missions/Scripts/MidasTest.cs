﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MidasTest : MonoBehaviour, IMissionCheck
{
    bool IMissionCheck.MissionStatus()
    {
        return PlayerPrefs.GetInt("gold") >= 100;
    }
}
