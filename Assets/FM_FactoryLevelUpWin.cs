﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class FM_FactoryLevelUpWin : MonoBehaviour
{
    [SerializeField]
    Text currentLevelText;
    [SerializeField]
    Text nextLevelText;
    [SerializeField]
    Text upgCostText;

    [SerializeField]
    Button upgClickBtn;

    [SerializeField]
    Button closeClickBtn;

    FM_Factory factory;
    FactoryData factoryData;

    public UnityAction OnUpgradeBtnClicked;
    public UnityAction OnCloseBtnClicked;

    FM_FactoryLevelUpUpListBuilder FM_FactoryLevelUp;


    public void Snow(FM_Factory _factory)
    {

        gameObject.SetActive(true);

        factory = _factory;
        factoryData = _factory.GetFactoryData();

        upgClickBtn.onClick.AddListener(OnUpgradeBtnClickHandler);
        closeClickBtn.onClick.AddListener(OnCloseBtnClickHandler);

        currentLevelText.text = factoryData.level.ToString();
        nextLevelText.text = (factoryData.level + 1).ToString();

        upgCostText.text = factoryData.buyPrice.ToString();

        FM_FactoryLevelUp = GetComponent<FM_FactoryLevelUpUpListBuilder>();

        BuildUpgradesList();

    }

    void OnCloseBtnClickHandler()
    {
        OnCloseBtnClicked?.Invoke();
        Hide();
    }


    void BuildUpgradesList()
    {
        FM_FactoryLevelUp.BuildFactoryList(factory);
       // ClearUpgradeList();
    }

   

    void OnUpgradeBtnClickHandler()
    {
        OnUpgradeBtnClicked();

        Hide();
    }


    public void Hide()
    {
        gameObject.SetActive(false);


        upgClickBtn.onClick.RemoveListener(OnUpgradeBtnClickHandler);
        closeClickBtn.onClick.AddListener(OnCloseBtnClickHandler);
    }
}
