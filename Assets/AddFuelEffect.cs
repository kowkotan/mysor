﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework.Utility;

public class AddFuelEffect:MonoBehaviour
{


  [SerializeField] float activeTime = 4;

    [SerializeField]
    int fuelGrow = 5;


    public void LaunchEffect()
    {
        FindObjectOfType<Robot>().GetRobotData().currentFuelTank += fuelGrow;

    }



}
