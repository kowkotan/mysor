﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework;
using Framework.Managers.States;
using UnityEngine;
using UnityEngine.UI;

public class FM_GamePanelView : MonoBehaviour
{
    [SerializeField] LM_Location location;
    //[SerializeField] GM_Level[] levelGlobal;
    [SerializeField] LM_LocationMenu locationMenu;
    [SerializeField] GM_LevelMapMenu levelMapMenu;
    [SerializeField] GameObject missionsMenu;

    [SerializeField] Button OnSortingGameClicked;
    [SerializeField] Button OnGlobalGameClicked;
    [Inject] StateManager stateManager;
    [Inject] HardValueModel hardValueModel;
    void Start()
    {
        this.Inject();
        OnSortingGameClicked.onClick.AddListener(SortingGameClicked);
        OnGlobalGameClicked.onClick.AddListener(GlobalGameClicked);
    }

    void SortingGameClicked()
    {
        if (PlayerPrefs.GetInt("robotGameTutorialPart1Completed") == 1)  
        {
            locationMenu.Show(location);
        }
        else
        {
            //PlayerPrefs.SetString("PlayerLevelTest", "1");
            PlayerPrefs.SetString("PlayerLevelTutorial", "1");
            PlayerPrefs.SetString("PlayerLocationTutorial", "1");
            stateManager.ChangeScene("RobotGame");
        }
    }

    void GlobalGameClicked()
    {
        levelMapMenu.Show();
    }

    public void CallShopWindow()
    {
        Application.ExternalCall("ShowShop", 23, "TEST PAYMENT", 10);
    }

    public void AddFunds(int _id)
    {
        switch(_id)
        {
            case 20: hardValueModel.AddValue(20);
                break;
            case 55: hardValueModel.AddValue(55);
                break;
            case 240: hardValueModel.AddValue(240);
                break;
            case 750: hardValueModel.AddValue(750);
                break;
            case 2000: hardValueModel.AddValue(2000);
                break;
        }
    }

    public void AddFunds_OK(int amount)
    {
        switch(amount)
        {
            case 32: hardValueModel.AddValue(20);
                break;
            case 78: hardValueModel.AddValue(55);
                break;
            case 319: hardValueModel.AddValue(240);
                break;
            case 798: hardValueModel.AddValue(750);
                break;
            case 1595: hardValueModel.AddValue(2000);
                break;
        }
    }

    private void OnDestroy()
    {
        OnSortingGameClicked.onClick.RemoveListener(SortingGameClicked);
        OnGlobalGameClicked.onClick.RemoveListener(GlobalGameClicked);
    }
}
