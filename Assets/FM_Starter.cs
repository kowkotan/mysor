﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameAnalyticsSDK;

public class FM_Starter : MonoBehaviour
{
    [SerializeField] Button buildFoodFacButton;

    [SerializeField] GameObject speedUpButton;
    [SerializeField] GameObject priceTxt;
    [SerializeField] GameObject adBtn;

    [SerializeField] Button openFoodFacButton;

    [SerializeField] Button closeFacWindowButton;

    [SerializeField] Button sortingGameButton;

    private void Awake() {
        GameAnalytics.Initialize();
    }

    void Start()
    {
        if (PlayerPrefs.GetInt("factoryTutorialPart1Completed") == 1) return;

        TutorialLoader.instance.Load("fTut1");

        TutorialEvents.OnTutorialComplete.AddListener(Tut1Comp);

        Dictionary<String, System.Object> eventParameters = new Dictionary<String, System.Object>();
        eventParameters.Add("Прохождение", "Прошел 1-ый шаг туториала");
        AppMetrica.Instance.ReportEvent("Туториал", eventParameters);

        GameAnalytics.NewDesignEvent("Tutorial:Proshel 1 shag tutoriala");

        AppMetrica.Instance.SendEventsBuffer();
    }

    void Tut1Comp()
    {
        TutorialEvents.OnTutorialComplete.RemoveListener(Tut1Comp);

        TutorialLoader.instance.Load("fTut2", true, buildFoodFacButton);

        TutorialEvents.OnTutorialComplete.AddListener(Tut2Comp);
        speedUpButton.SetActive(false);
        priceTxt.SetActive(false);
        adBtn.SetActive(false);

        Dictionary<String, System.Object> eventParameters = new Dictionary<String, System.Object>();
        eventParameters.Add("Прохождение", "Прошел 2-ой шаг туториала");
        AppMetrica.Instance.ReportEvent("Туториал", eventParameters);

        GameAnalytics.NewDesignEvent("Tutorial:Proshel 2 shag tutoriala");

        AppMetrica.Instance.SendEventsBuffer();
    }

    void Tut2Comp()
    {
        TutorialEvents.OnTutorialComplete.RemoveAllListeners();
        Invoke("Tut3Comp", 5f);

        //TutorialEvents.OnTutorialComplete.AddListener(Tut3Comp);
    }

    void Tut3Comp()
    {
        TutorialLoader.instance.Load("fTut7", true, sortingGameButton);

        PlayerPrefs.SetInt("factoryTutorialPart1Completed", 1);

        Dictionary<String, System.Object> eventParameters = new Dictionary<String, System.Object>();
        eventParameters.Add("Прохождение", "Прошел 3-ий шаг туториала");
        AppMetrica.Instance.ReportEvent("Туториал", eventParameters);

        GameAnalytics.NewDesignEvent("Tutorial:Proshel 3 shag tutoriala");

        AppMetrica.Instance.SendEventsBuffer();
    }
}

