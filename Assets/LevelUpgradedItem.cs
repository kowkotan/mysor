﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelUpgradedItem : MonoBehaviour
{
    [SerializeField] Image levelUpItemIcon;

    [SerializeField] GameObject countPlaceholder;
    [SerializeField] Text countText;

    public void Fill(Sprite _icon, int _count)
    {
        levelUpItemIcon.sprite = _icon;
        countText.text = _count.ToString();
    }

    public void Fill(Sprite _icon)
    {
        levelUpItemIcon.sprite = _icon;
        countPlaceholder.SetActive(false);
    }

    public void Fill( int _count)
    {
        countText.text = _count.ToString();
    }
}
