﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class CountdownEffectUIIndicatorBase:MonoBehaviour
{

    protected CountdownEffect countdownEffect;
    float defTime;
  
    [SerializeField]  bool isTracking;

    public void StartTrack(CountdownEffect _countdownEffect)
    {
        isTracking = true;

        countdownEffect = _countdownEffect;
        defTime = countdownEffect.countdownTime;
        Show();
    }

    protected float GetFillingValueProgress()
    {
        return (defTime - countdownEffect.countdownTime) / defTime;
    }

    protected abstract void Show();


    private void LateUpdate()
    {
       
        if (!isTracking) return;

        UpdateTimeText();

    }

    protected abstract void UpdateTimeText();


    public void StopTrack()
    {
        isTracking = false;
        Hide();
    }

    protected abstract void Hide();

}
