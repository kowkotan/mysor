﻿using System;
using UnityEngine;

public static class BalancedValues
{
    public const float RobotPickupRadiusUpgGrow = 0.1f;
    public const int RobotTrashTankUpgGrow = 2;
    public const float RobotFuelTankUpgGrow = 3.5f;
    static public int[] MaxEnergyForLevel = { 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 10, 12, 12, 12, 14, 14, 14, 16, 16, 16, 18, 18, 18, 20 };

    public static int GetUserLevelUpSoftValueReward(int _newLevel)
    {
        return Mathf.CeilToInt(_newLevel * 100);
    }

    public static int GetUserLevelUpHardValueReward(int _newLevel)
    {
        int[] upPrices = { 50, 50, 10, 10, 10, 20, 20, 20, 20, 20, 30, 30, 30, 30, 30, 40, 40, 40, 40, 40, 50, 50, 50, 50, 50, 60 };
       
         if (_newLevel > 25)
            return (1551900 * _newLevel / 2);

        return upPrices[_newLevel];
    }


    public static int GetRobotUpradePrice(int defPrice,int _currentUpgradeLevel,float _growK )
    {
        return Mathf.CeilToInt( defPrice * _currentUpgradeLevel * _growK);
    }

    public static int GetSortingQuestValues(int _newLevel)
    {
        int[] upValues = { 5, 5, 5, 5, 5, 5, 7, 7, 7, 7, 7, 10, 10, 10, 10, 10, 13, 13, 13, 13, 13, 15, 15, 15, 15, 15 };

        if (_newLevel > 25)
            return (1551900 * _newLevel / 2);

        return upValues[_newLevel];
    }

    public static int GetCreatingQuestValues(int _newLevel)
    {
        int[] upValues = { 5, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 5, 5, 5, 5, 5, 7, 7, 7, 7, 7, 10, 10, 10, 10, 10 };

        if (_newLevel > 25)
            return (1551900 * _newLevel / 2);

        return upValues[_newLevel];
    }

    public static int GetCollectQuestValues(int _newLevel)
    {
        int[] upValues = { 5, 5, 5, 5, 5, 5, 7, 7, 7, 7, 7, 10, 10, 10, 10, 10, 13, 13, 13, 13, 13, 15, 15, 15, 15, 15 };

        if (_newLevel > 25)
            return (1551900 * _newLevel / 2);

        return upValues[_newLevel];
    }

    public static float GetFactoryCreatingNewProductTime( int _level, int _numberOfPruductionLine)
    {
        int[] Item1 = { 60, 60, 59, 57, 56, 54 };
        int[] Item2 = { 600, 600, 495, 408, 337, 337 };
        int[] Item3 = { 6000, 6000, 3000, 3000, 3000, 3000 };

        if (_numberOfPruductionLine == 1)
            return Item1[_level];
        if (_numberOfPruductionLine == 2)
            return Item2[_level];
        if (_numberOfPruductionLine == 3)
            return Item3[_level];

        return 0;
    }

    public static int GetProductCreatingCost(int _level, int _numberOfPruductionLine)
    {
        int[] Item1 = { 1, 1, 1, 1, 1, 1 };
        int[] Item2 = { 2, 2, 2, 2, 2, 2 };
        int[] Item3 = { 6, 6, 6, 6, 6, 6 };

        if (_numberOfPruductionLine == 1)
            return Item1[_level];
        if (_numberOfPruductionLine == 2)
            return Item2[_level];
        if (_numberOfPruductionLine == 3)
            return Item3[_level];

        return 0;
    }

    public static int GetProductSoldOneCost(int _level, int _numberOfPruductionLine)
    {
        int[] Item1 = { 45, 45, 45, 45, 45, 45 };
        int[] Item2 = { 300, 300, 300, 300, 300, 300 };
        int[] Item3 = { 2800, 2800, 2800, 2800, 2800, 2800 };

        if (_numberOfPruductionLine == 1)
            return Item1[_level];
        if (_numberOfPruductionLine == 2)
            return Item2[_level];
        if (_numberOfPruductionLine == 3)
            return Item3[_level];

        return 0;
    }


    public static int GetMaxExp(int _level)
    {
        int[] upPrices = { 0, 50, 200, 400, 700, 1150, 1800, 2750, 4150, 6250, 9400, 14100, 21150, 31700, 46700, 86900, 128300, 186600, 244900, 347800, 473200, 598500, 723900, 849300, 999800 };

        if (_level > 25)
            return (1551900 * _level / 2);

        return upPrices[_level];
    }


    public static int GetFactoryUpgradePrice( int _level)
    {
        int[] upPrices = { 200, 800, 11000, 110000, 110000 };
        return upPrices[_level];
    }

    public static int GetFactoryUnlockPrice(TrashMaterials trashMaterials)
    {
        switch (trashMaterials)
        {
            case TrashMaterials.FOOD:
                return 0;
                break;
            case TrashMaterials.GLASS:
                return 100000;
                break;
            case TrashMaterials.PAPER:
                return 1500;
                break;
            case TrashMaterials.PLASTIC:
                return 10000;
                break;
        }
        return 0;
    }

    public static int GetFactoryUpgradeTime(int _level)
    {
        int[] upTime = { 300, 1800, 10800, 43200, 86400 };
        return upTime[_level];
    }

    public static int GetFactoryUnlockTime(TrashMaterials trashMaterials)
    {
        switch (trashMaterials)
        {
            case TrashMaterials.FOOD:
                return 300;
                break;
            case TrashMaterials.GLASS:
                return 300;
                break;
            case TrashMaterials.PAPER:
                return 300;
                break;
            case TrashMaterials.PLASTIC:
                return 300;
                break;
        }
        return 0;
    }



    public static int GetFactoryMaxProductLimit(int _factoryLevel, int _numberOfPruductionLine)
    {
        int[] Item1 = { 15, 15, 20, 25, 30, 35 };
        int[] Item2 = { 20, 20, 25, 30, 35, 35 };
        int[] Item3 = { 30, 30, 30, 30, 35, 35 };

        if (_numberOfPruductionLine == 1)
            return Item1[_factoryLevel];
        if (_numberOfPruductionLine == 2)
            return Item2[_factoryLevel];
        if (_numberOfPruductionLine == 3)
            return Item3[_factoryLevel];

        return 0;
    }

    public static int GetFactoryTrashContainerLimit(int _factoryLevel)
    {
        return Mathf.CeilToInt(_factoryLevel * 10);
    }

    public static int GetFactoryBuildSpeedUpPrice(int _elapsedSecond)
    {
        return Math.Max(Mathf.CeilToInt(Mathf.Abs((float)_elapsedSecond / 45)), 1);
    }

    public static int GetSpeedUpProcessProductPrice(int _elapsedSecond)
    {
        int costPerHour = 45;
        return Math.Max(Mathf.CeilToInt(Mathf.Abs((float)_elapsedSecond / costPerHour)), 1);
    }

    public static int GetMaxEnergyForLevel(int level)
    {
        if(MaxEnergyForLevel.Length > level)
        {
            return MaxEnergyForLevel[level];
        }
        return -1;
    }
}

