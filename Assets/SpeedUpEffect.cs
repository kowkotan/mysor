﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework.Utility;

public class SpeedUpEffect : CountdownEffect
{
    [SerializeField]  List<ISpeedUpEffect> countdownEffects;

    [SerializeField] float activeTime = 12;


    public void LaunchEffect()
    {
        countdownEffects = Extensions.FindOnCurrentScene<ISpeedUpEffect>();

        StartEffect(activeTime);
    }

    void StartSpeedUpEffect()
    {

        for (int i = 0; i < countdownEffects.Count; i++)
        {
            countdownEffects[i].StartSpeedUpEffect();
        }
    }

     void StopSpeedUpEffect()
    {
        for (int i = 0; i < countdownEffects.Count; i++)
        {
            countdownEffects[i].StopSpeedUpEffect();
        }
    }

    public override void ApplyEffect()
    {
        StartSpeedUpEffect();
    }

    public override void ResetEffect()
    {
        StopSpeedUpEffect();
    }


}
