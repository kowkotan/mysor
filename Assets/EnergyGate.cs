﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyGate : MonoBehaviour
{
    [Inject] EnergyModel energyModel;

    [SerializeField] Text unlockPriceTxt;

    [SerializeField] int unlockpPrice = 1;

    void Start()
    {
        this.Inject();
        unlockPriceTxt.text = unlockpPrice.ToString();
    }

    public bool Unlock()
    {
        return energyModel.DecEnergy(unlockpPrice);
    }
}
