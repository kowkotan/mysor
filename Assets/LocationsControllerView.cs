﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework.Managers.States;
using Adic;
using UnityEngine.UI;

public class LocationsControllerView : MonoBehaviour
{
   [Inject] StateManager stateManager;
   //[Inject] LevelDBData levelDBData;
   [SerializeField] LocationLoaderController[] locationLoaderController;
   [SerializeField] LocationsBtnBarController[] locationsBtnBarController;

    private void Start() {
        this.Inject();
        LoadLocations();
    }

    public void Close()
    {
        Destroy(gameObject);
    }

    public void LoadLevel()
    {
        stateManager.ChangeScene("RobotGame");
    }

    void LoadLocations()
    {
        for(int i = 0; i < locationsBtnBarController.Length; i++)
        {
            locationsBtnBarController[i].FillValue(31, 100);
        }
    }
}
