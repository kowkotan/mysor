﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework.Interfaces;
using Framework.Managers.Event;
using UnityEngine;
using UnityEngine.UI;

public class LevelGate : MonoBehaviour, IEventListener, IGate
{
    [Inject] LevelModel levelModel;
    [Inject] EventManager eventManager;

    [SerializeField]
    Text reqLevelText;

   public bool isInited;

    [SerializeField]
    public int reqLevel;

    public int ReqLevel
    {
        get
        {
            return reqLevel;
        }
        set
        {
            reqLevel = value;
            reqLevelText.text = reqLevel.ToString();
        }
    }

    public Action OnUnlocked = delegate {};
    public Action OnInit = delegate { };


    void Start()
    {
        this.Inject();
        eventManager.AddListener(EVENT_TYPE.LEVEL_UP, this);
        reqLevelText.text = reqLevel.ToString();

        isInited = true;
        TryUnlock();

        OnInit();
    }

    public void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param = null)
    {
        TryUnlock();

    }

   public void TryUnlock()
    {

        if (gameObject.activeInHierarchy == false) return;

        if (IsUnlocked())
        {
            gameObject.SetActive(false);
            OnUnlocked();
        }
    }

    public bool IsUnlocked()
    {
        return reqLevel <= levelModel.CurrentLevel;
    }
}
