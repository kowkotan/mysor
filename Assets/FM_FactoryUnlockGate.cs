﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework.Interfaces;
using UnityEngine;
using UnityEngine.UI;

public class FM_FactoryUnlockGate : MonoBehaviour, IGate, ITickSec
{
 

    [SerializeField]
    FM_Factory factory;

    [SerializeField]
    FM_Factory trackedFactory;

    [SerializeField]
    Text factoryTitleText;

    [SerializeField]
    FactoryGate factoryGate;

    [SerializeField]
    LevelGate levelGate;

    [SerializeField]
    GameObject unlockState;

    public bool isInited;

    [Inject]
    IFactoryLocalDatabase factoryDB;
    

    void Awake()
    {
        factory.OnInitialized += Factory_OnInitialized;
        levelGate.OnInit += OnGatesInited;
        factoryGate.OnInit += OnGatesInited;
    }

    void Start()
    {
        Debug.Log(factoryTitleText.text + IsUnlocked());
        this.Inject();
        isInited = true;
        OnGatesInited();
        //TryUnlock();
    }

    void OnGatesInited()
    {

        if (factoryGate.isInited == false || levelGate.isInited == false || isInited == false)
            return;

        if (IsUnlocked() == false)
        {
            levelGate.OnUnlocked += OnUnlocked;
            levelGate.ReqLevel = factoryDB.GetUnlockLevel(factory.GetFactoryData().id);
            factoryGate.OnUnlocked += OnUnlocked;

            TryUnlock();
        }

    }

    void OnUnlocked()
    {
        TryUnlock();

        if(IsUnlocked())
        {
            levelGate.OnUnlocked -= OnUnlocked;
            factoryGate.OnUnlocked -= OnUnlocked;
        }
    }

    void Factory_OnInitialized()
    {
        factoryTitleText.text = "Завод " + factory.Title;

        if(trackedFactory != null)
        trackedFactory.BuildInFinished += Factory_BuildInFinished;
    }


    void Factory_BuildInFinished()
    {
        TryUnlock();
    }

    public bool IsUnlocked()
    {
        return factoryGate.IsUnlocked() == true && levelGate.IsUnlocked() == true;
    }

    public void TryUnlock()
    {
        factoryGate.TryUnlock();
        if (IsUnlocked())
        {
            gameObject.SetActive(false);
            unlockState.SetActive(true);
        }
    }

    private void OnDestroy()
    {

        factory.OnInitialized -= Factory_OnInitialized;

        levelGate.OnInit -= OnGatesInited;
        factoryGate.OnInit -= OnGatesInited;


        if (trackedFactory != null)
            trackedFactory.BuildInFinished -= Factory_BuildInFinished;
    }

    private void Update() {
        if (!IsUnlocked())
        {
            TryUnlock();
        }
    }

    public void TickSec()
    {
        if (!IsUnlocked())
        {
            TryUnlock();
        }
    }
}
