﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    [SerializeField] AudioSource audioSource;

    public void SetSound(AudioClip clip)
    {
        audioSource.clip = clip;
    }
}
