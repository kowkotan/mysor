﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework.Views;
using UnityEngine;

public class SG_TrashSortingProgressUI : MonoBehaviour
{
    [SerializeField] ProgressBarWithText CapicityProgressBar;
    [SerializeField] FillingImageView lost;


    RobotGameResult robotGameResult;
    [Inject] SG_TrashCounter trashCounter;

    int totalPickedTrashes;

    private void Start()
    {
        this.Inject();
        robotGameResult = FindObjectOfType<RobotGameResult>();
        totalPickedTrashes = trashCounter.TotalTrashes;//robotGameResult.GetPickedTrashDictionary().Count;
    }
    

    public void Update()
    {
        CapicityProgressBar.FillValue(trashCounter.SortingTrash, totalPickedTrashes);
        lost.SetNormalizedValue((float)trashCounter.LostTrash/totalPickedTrashes);
    }
}
