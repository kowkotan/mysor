﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Framework.Interfaces;
using Adic;

public class DailyQuests : MonoBehaviour
{
    [SerializeField] Text Timer;
    [Inject] LevelModel levelModel;
    [SerializeField] QuestsLocalStorage QuestDb;
    [SerializeField] GoalManager goalManager;
    private System.Random rnd;
    private int collectCount, sortCount, produceCount;
    private TrashMaterials collectType, sortType, produceType, upgradeType, buildType;
    
    public void Start(){
        int typesCount = Enum.GetNames(typeof(TrashMaterials)).Length;
        
        rnd = new System.Random(DateTime.Now.Year * 10000 + DateTime.Now.Month * 100 + DateTime.Now.Day);
        
        collectCount = rnd.Next();
        sortCount = rnd.Next();
        produceCount = rnd.Next();

        collectType = (TrashMaterials) rnd.Next(typesCount);
        sortType = (TrashMaterials) rnd.Next(typesCount);
        produceType = (TrashMaterials) rnd.Next(typesCount);
        upgradeType = (TrashMaterials) rnd.Next(typesCount);
        buildType = (TrashMaterials) rnd.Next(typesCount);

    }

    public void Update()
    {
        Timer.text = (23 - DateTime.Now.Hour).ToString("D2") + ":" + (59 - DateTime.Now.Minute).ToString("D2") + ":" + (59 - DateTime.Now.Second).ToString("D2");
        if(Timer.text == "00:00:00")
        {
            goalManager.ResetAllQuests();
        }
    }

}
