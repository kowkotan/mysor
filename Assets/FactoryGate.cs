﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using Framework.Interfaces;
using Framework.Managers.Event;
using UnityEngine.UI;

public class FactoryGate : MonoBehaviour, IEventListener ,IGate
{
    [SerializeField]
    string reqUnlockedFactoryId;

    [SerializeField]
    Image factoryImage;

    [Inject] IFactoryLocalDatabase factoryLocalDatabase;
    [Inject] ISaveLoadSystem saveLoadSystem;
    [Inject] EventManager eventManager;

    public bool isInited;

    FactoryData factoryData;

    public Action OnUnlocked = delegate { };
    public Action OnInit = delegate { };


    void Start()
    {
        this.Inject();

        eventManager.AddListener(EVENT_TYPE.LEVEL_LOADED, this);

        if (reqUnlockedFactoryId == string.Empty)
        {
            gameObject.SetActive(false);
            return;
        }

        factoryImage.sprite =  factoryLocalDatabase.GetIcon(reqUnlockedFactoryId);

        //TryUnlock();

        isInited = true;

        OnInit();
    }

    public void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param = null)
    {
        TryUnlock();

    }

    public void TryUnlock()
    {
        if (gameObject.activeInHierarchy == false) return;

        if (reqUnlockedFactoryId == string.Empty)
        {
            gameObject.SetActive(false);
            return;
        }

        factoryData = saveLoadSystem.LoadStruct<FactoryData>(reqUnlockedFactoryId);

        if (IsUnlocked())
        {
            gameObject.SetActive(false);
            OnUnlocked();
        }
    }

    public bool IsUnlocked()
    {
        if (reqUnlockedFactoryId == string.Empty) return true;

        return factoryData.level >= 1;
    }
}
