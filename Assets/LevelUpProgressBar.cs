﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework.Interfaces;
using Framework.Managers.Event;
using UnityEngine;
using UnityEngine.UI;

public class LevelUpProgressBar : MonoBehaviour, IEventListener
{
    [SerializeField] Text levelTxt;
    [SerializeField] ProgressBarWithTextFillingValue progressBarWithTextFilling;

    [SerializeField] Image currentProgressBar;
    [SerializeField] Image animatedProgressBar;
    [SerializeField] Text percentText;

    [Inject] EventManager eventManager;
    [Inject] LevelModel LevelModel;

    public float levelDifference;

    void Start()
    {
        this.Inject();
        eventManager.AddListener(EVENT_TYPE.LEVEL_UP, this);
        eventManager.AddListener(EVENT_TYPE.EXP_GROW, this);
        levelDifference = LevelModel.MaxExp - LevelModel.Exp;
        Invoke("UpdateProgressBarData", 0f);
    }

    public void OnEvent(EVENT_TYPE Event_Type, Component Sender, System.Object Param = null)
    {

        if (Event_Type == EVENT_TYPE.LEVEL_UP)
        {
            levelTxt.text = LevelModel.CurrentLevel.ToString();
        }
        //progressBarWithTextFilling.FillValue(LevelModel.Exp, LevelModel.MaxExp);
        //Debug.Log("CURRENT EXP: " + LevelModel.Exp/LevelModel.MaxExp);
        animatedProgressBar.gameObject.SetActive(true);
        animatedProgressBar.fillAmount = LevelModel.Exp/LevelModel.MaxExp;
        percentText.text = string.Format("{0:0}%", (LevelModel.Exp/LevelModel.MaxExp) * 100);
        levelDifference = LevelModel.MaxExp - LevelModel.Exp;
        StartCoroutine(UpdateAnimatedProgressBar());
    }

    IEnumerator UpdateAnimatedProgressBar()
    {
        yield return new WaitForSeconds(0.7f);
        do
        {
            yield return new WaitForSeconds(0.025f);
            currentProgressBar.fillAmount += 0.03f;
            //Debug.Log("CURRENT DATA: " + currentProgressBar.fillAmount);
        }
        while(currentProgressBar.fillAmount <= animatedProgressBar.fillAmount);

        if(currentProgressBar.fillAmount >= animatedProgressBar.fillAmount)
        {
            currentProgressBar.fillAmount = animatedProgressBar.fillAmount;
            StopCoroutine(UpdateAnimatedProgressBar());
            animatedProgressBar.gameObject.SetActive(false);
        }
    }

    void UpdateProgressBarData()
    {
        levelTxt.text = LevelModel.CurrentLevel.ToString();
        progressBarWithTextFilling.FillValue(LevelModel.Exp, LevelModel.MaxExp);
        levelDifference = LevelModel.MaxExp - LevelModel.Exp;
    }
}
