﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Framework.Common.Camera;
using UnityEngine;


public class CameraController : MonoBehaviour
{
    [SerializeField]
    Camera2DFollowTDS camera2DFollowTDS;

    Camera currentCamera;

    private void Start()
    {
        currentCamera = GetComponent<Camera>();
    }

    public void SetTarget(Transform _transform)
    {
        camera2DFollowTDS.SetTarget(_transform);
    }

    public void GoToPointAndZoom(Vector3 _pos, float _moveTimeSec, float _size, float _sizeChangeSpeed, Action OnCompletedAction  = null  )
    {
        int numbOfAction = 1;

        transform.DOMove(new Vector3(_pos.x,_pos.y,transform.position.z), _moveTimeSec).OnComplete(() =>
        {
            numbOfAction--;
            if (numbOfAction <= 0)
                OnCompletedAction?.Invoke();
        });

        //currentCamera.DOOrthoSize(_size, _sizeChangeSpeed).OnComplete(() =>
        //{
        //    numbOfAction--;
        //    if(numbOfAction <= 0)
        //    OnCompletedAction?.Invoke();
        //});
    }

}
