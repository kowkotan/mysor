﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework.Views;

public class LocationsBtnBarController : MonoBehaviour
{
    [SerializeField]
    PercentFillingValueView percentFillingValue;

    [SerializeField]
    FillingImageView fillingImageView;

    public void FillValue(float minValue, float maxValue )
    {
        percentFillingValue.SetNormalizedValue(minValue / maxValue);
        fillingImageView.SetNormalizedValue(minValue / maxValue);
    }
}
