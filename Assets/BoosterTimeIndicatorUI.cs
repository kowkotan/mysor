﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoosterTimeIndicatorUI : CountdownEffectUIIndicatorBase
{
    [SerializeField] Text timeTxt;

    protected override void Hide()
    {

        gameObject.SetActive(false);
    }

    protected override void Show()
    {
        gameObject.SetActive(true);
    }

    protected override void UpdateTimeText()
    {
        timeTxt.text = countdownEffect.countdownTime.ToString("00");
    }
}
