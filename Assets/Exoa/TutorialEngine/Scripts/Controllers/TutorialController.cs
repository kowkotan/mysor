﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour
{

	public static bool TUTORIAL_ACTIVE;
    public List<GameObject> TutorialUsedCanvases = new List<GameObject>();
	private TutorialSession session;
	private List<TutorialSession.TutorialStep> steps;
	public TutorialPopup popup;
	public RectTransform mask;
	private int currentStep = -1;
	public float scale = 1.2f;
	public float speed = .5f;
	private Color initBGColor;
	public Image bg;
	public static TutorialController instance;
	private bool retried;

	public float size1Duration = 0.5f;
	public float size2Duration = 2;
	public float size2Delay = 1;
	public float positionDuration = 1.5f;
	public float bgDuration = 1.5f;
	public Color tutoBGColor;
	public Ease size1Ease = Ease.InOutBack;
	public Ease size2Ease = Ease.OutElastic;
	public Ease positionEase = Ease.OutQuint;
	private GameObject currentTarget;
	private Vector2 currentEndPositionValue;
	private float currentEndSizeValue;
	private Tweener moveTween;
	private Tweener sizeTween1;
	private Tweener sizeTween2;
	private GameObject lastFocusedObject;

	void Awake()
	{
		if (instance != null) throw new Exception("TutorialController alraedy creaeted");

		instance = this;
		popup.gameObject.SetActive(false);
		popup.closeBtn.onClick.AddListener(OnClosePopup);
	}

	private void OnClosePopup()
	{
		HideTutorial();
	}

	void Start()
	{
		initBGColor = bg.color;
		bg.material.color = initBGColor;

        if (TutorialLoader.instance.tutorialLoaded)
        {
            onTutorialLoeaded(null);
        }

		TutorialLoader.instance.onTutorialLoaded +=  onTutorialLoeaded;

	}

	private void onTutorialLoeaded(Button nextButton )
	{
		steps = new List<TutorialSession.TutorialStep>();
		if (TutorialLoader.instance != null && TutorialLoader.instance.currentTutorial.tutorial_steps != null)
			steps.AddRange(TutorialLoader.instance.currentTutorial.tutorial_steps);

		TUTORIAL_ACTIVE = true;
		currentStep = -1;

		if (steps.Count > 0)
		{

            if (nextButton == null)
            {
                ShowNextButton();
            }
            else
            {
                ChangeNextButton(nextButton);
            }

            popup.gameObject.SetActive(true);
			popup.createBackground = false;
			popup.Init();
			popup.Center();
			popup.Open();

			bg.gameObject.SetActive(true);

			mask.gameObject.SetActive(true);
			mask.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 200);
			mask.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 200);
			mask.anchoredPosition = new Vector2(0, 1000);
			Next();
		}
		else
		{
			HideTutorial();
		}
	}

    public void ShowNextButton()
    {
        popup.ShowNextButton();
        popup.OnClickNext.RemoveAllListeners();
        popup.OnClickNext.AddListener(Next);
    }

    public void ChangeNextButton(Button newButton)
    {
        popup.HideNextButton();
        newButton.gameObject.layer = 3;
        currentButton = newButton;
        newButton.onClick.AddListener(Next);
    }

    public void HideNextButton()
    {
        popup.HideNextButton();
    }

    private void CheckRaycast(GraphicRaycaster raycaster)
    {
        PointerEventData pointer = new PointerEventData(EventSystem.current);
        pointer.position = Input.mousePosition;
        List<RaycastResult> results = new List<RaycastResult>();
        raycaster.Raycast(pointer, results);
        foreach (var result in results)
        {
            if (currentButton != null && result.gameObject.GetComponent<Button>() == currentButton)
            {
                currentButton.OnPointerClick(pointer);
            }
        }
    }

    Button currentButton = null;
    void Update()
	{
		if (!TUTORIAL_ACTIVE)
			return;


        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            GameObject canvas = GameObject.Find("[LEVELS]");
            if (canvas != null)
            {
                GraphicRaycaster raycaster = canvas.GetComponent<GraphicRaycaster>();
                if(raycaster != null)
                {
                    CheckRaycast(raycaster);
                }
            }

            foreach (var canv in TutorialUsedCanvases)
            {
                GraphicRaycaster raycaster = canv.GetComponent<GraphicRaycaster>();
                if (raycaster != null)
                {
                    CheckRaycast(raycaster);
                }
            }
        }

        if (currentTarget != null && moveTween != null)
		{
			Rect rect2D = GetObjectCanvasRect(currentTarget);
			float size = Mathf.Max(rect2D.width, rect2D.height);

			// Mask position live update
			if (currentEndSizeValue != size)
			{
				currentEndSizeValue = size;

				if (!sizeTween1.IsPlaying() && !sizeTween2.IsPlaying())
				{
					mask.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, currentEndSizeValue * scale);
					mask.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, currentEndSizeValue * scale);
				}
				else
				{
					if (sizeTween1.IsPlaying())
						sizeTween1.ChangeEndValue(new Vector2(.3f, 2f) * currentEndSizeValue * scale, true);
					if (sizeTween2.IsPlaying())
						sizeTween2.ChangeEndValue(Vector2.one * currentEndSizeValue * scale, true);

				}
			}
			if (currentEndPositionValue != rect2D.position)
			{
				currentEndPositionValue = rect2D.position;
				if (!moveTween.IsPlaying())
				{
					mask.anchoredPosition = rect2D.position;

				}
				else
				{
					moveTween.ChangeEndValue(currentEndPositionValue, true);
				}
				popup.SetPosition(rect2D);
			}

		}
	}

public	void Next()
	{
		print("Next Tutorial Step");
		currentStep++;
		if (steps == null || currentStep >= steps.Count)
		{
			HideTutorial();

			return;
		}
		if (sizeTween1 != null) sizeTween1.Kill();
		if (sizeTween2 != null) sizeTween2.Kill();
		if (moveTween != null) moveTween.Kill();

		TutorialSession.TutorialStep s = steps[currentStep];
		popup.SetStep(s);
		currentTarget = null;

        if (s.target_obj != "")
        {
            currentTarget = GameObject.Find(s.target_obj);
        }


  

        if (currentTarget != null && currentTarget != lastFocusedObject)
		{
			lastFocusedObject = currentTarget;
			Rect rect2D = GetObjectCanvasRect(currentTarget);

			float size = Mathf.Max(rect2D.width, rect2D.height);
			Vector3 dir = new Vector3(rect2D.position.x, rect2D.position.y, 0) - mask.anchoredPosition3D;

			Debug.DrawRay(mask.position, dir, Color.yellow, 10);
			mask.rotation = Quaternion.LookRotation(mask.forward, dir);

			currentEndPositionValue = rect2D.position;
			moveTween = mask.DOAnchorPos(rect2D.position, positionDuration).SetEase(positionEase);

			currentEndSizeValue = size;
			sizeTween1 = mask.DOSizeDelta(new Vector2(size * scale * 0.3f, size * scale * 2f), size1Duration).SetEase(size1Ease);
			sizeTween2 = mask.DOSizeDelta(new Vector2(size * scale, size * scale), size2Duration).SetDelay(size2Delay).SetEase(size2Ease);

			if (currentTarget.GetComponent<RectTransform>() == null)
			{
				Bounds rect3D = GameObjectUtils.GetObject3DRect(currentTarget);
				TutorialEvents.OnTutorialFocus.Invoke(s.target_obj, rect3D.center, 0);
			}

			popup.SetPosition(rect2D);
		}
		else if (!retried && s.target_obj != null && s.target_obj != "")
		{
			retried = true;
			Debug.Log("RETRYING CANNOT FIND " + s.target_obj);
			// Retry in .2f seconds
			currentStep--;
			Invoke("Next", .2f);

		}
		else
		{
			if (s.target_obj != null && s.target_obj != "")
				Debug.Log("CANNOT FIND " + s.target_obj);



            popup.SetPosition(new Rect(Vector2.zero, Vector2.zero));
			mask.sizeDelta = (Vector2.zero);
			mask.anchoredPosition = (Vector2.zero);
		}

		bg.DOColor(currentStep == 0 ? initBGColor : tutoBGColor, bgDuration);
		bg.material.DOColor(currentStep == 0 ? initBGColor : tutoBGColor, bgDuration);
	}

	private void HideTutorial()
	{
		popup.Hide();
		popup.OnClickNext.RemoveAllListeners();
		mask.DOSizeDelta(new Vector2(7000, 7000), 1).SetEase(Ease.InBack).OnComplete(() =>
		{
			bg.gameObject.SetActive(false);
			mask.gameObject.SetActive(false);
			TUTORIAL_ACTIVE = false;
			TutorialEvents.OnTutorialComplete.Invoke();
		});
	}


	/**
	* Get Object's bounds in the Canvas space
	**/
	private Rect GetObjectCanvasRect(GameObject obj)
	{
		RectTransform objRect = obj.GetComponent<RectTransform>();
		Renderer objRenderer = obj.GetComponent<Renderer>();
		Collider objCollider = obj.GetComponentInChildren<Collider>();

		Rect newRect = new Rect();
		if (objRect != null)
			newRect = objRect.GetRectFromOtherParent(mask.parent as RectTransform);
		else if (objRenderer != null)
		{
			newRect = objRenderer.GetScreenRect(Camera.main);
			newRect = (mask.parent as RectTransform).ScreenRectToRectTransform(newRect);
		}
		else if (objCollider != null)
		{
			newRect = objCollider.GetScreenRect(Camera.main);
			newRect = (mask.parent as RectTransform).ScreenRectToRectTransform(newRect);

		}
		return newRect;
	}





}
