﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TutorialLoader : MonoBehaviour
{

	public static TutorialLoader instance;
    public Action<Button> onTutorialLoaded;
    public UnityEvent onTutorialReady;

	[HideInInspector]
	public bool tutorialLoaded;

	[HideInInspector]
	public Tutorial currentTutorial;

	[HideInInspector]
	public string loadedTutorialName;

    Button nextButton;

	public virtual void Awake()
	{
		instance = this;
		tutorialLoaded = false;
	}

	public void Start()
	{

	}

#if UNITY_EDITOR
	void OnGUI()
	{

		if (Input.GetKeyDown(KeyCode.E) && Event.current != null && Event.current.control && Event.current.type == EventType.KeyDown)
			SceneManager.LoadScene("TutorialEditor");
	}
#endif


	virtual public void Load(string name, bool sendLoadedEvent = true, Button _nextButton = null, Action OnCompleted  = null)
	{
		Debug.Log("Load " + name);

       

        currentTutorial = LoadOffline(name);

        nextButton = _nextButton;

        loadedTutorialName = name;
		ProcessTutorial(sendLoadedEvent);
	}
	protected virtual void ProcessTutorial(bool sendLoadedEvent)
	{
		if (currentTutorial == null) return;


		tutorialLoaded = true;

        Debug.Log("NEXT BUTTON " + nextButton);

        if (sendLoadedEvent)
			onTutorialLoaded?.Invoke(nextButton);

		onTutorialReady?.Invoke();
	}


	public static Tutorial LoadOffline(string name)
	{
		TextAsset json = Resources.Load<TextAsset>("Tutorials/" + name);
		return JsonUtility.FromJson<Tutorial>(json.text);
	}

}
