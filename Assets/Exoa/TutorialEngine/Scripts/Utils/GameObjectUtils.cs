﻿using UnityEngine;

public class GameObjectUtils
{


	/**
	* Get Object's bounds in the 3D space
	**/
	public static Bounds GetObject3DRect(GameObject obj)
	{

		Renderer objRenderer = obj.GetComponent<Renderer>();
		Collider objCollider = obj.GetComponentInChildren<Collider>();

		Bounds newRect = new Bounds();
		if (objRenderer != null)
		{
			newRect = objRenderer.bounds;
		}
		else if (objCollider != null)
		{
			newRect = objCollider.bounds;
		}
		return newRect;
	}
}
