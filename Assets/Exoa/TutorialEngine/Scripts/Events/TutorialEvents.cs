﻿using UnityEngine;
using UnityEngine.Events;

public class TutorialEvents : MonoBehaviour
{

	public static UnityEvent OnTutorialComplete = new UnityEvent();
	[System.Serializable]
	public class ObjectEvent : UnityEvent<string, Vector3, float> { }
	public static ObjectEvent OnTutorialFocus = new ObjectEvent();
}
