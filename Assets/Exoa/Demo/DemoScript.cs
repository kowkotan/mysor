﻿using UnityEngine;
using UnityEngine.UI;

public class DemoScript : MonoBehaviour
{

	public Button playBtn;
	public Button startTutorialBtn;

	void Start()
	{
		playBtn.onClick.AddListener(OnClickPlay);
		startTutorialBtn.onClick.AddListener(OnClickStartTutorial);
	}

	void OnClickStartTutorial()
	{
		TutorialLoader.instance.Load("1.1");

		startTutorialBtn.gameObject.SetActive(false);
		playBtn.gameObject.SetActive(true);
	}

	void OnClickPlay()
	{
		TutorialLoader.instance.Load("1.2");
		TutorialEvents.OnTutorialComplete.AddListener(OnTutorialCompleted);

		playBtn.gameObject.SetActive(false);


	}

	private void OnTutorialCompleted()
	{
		startTutorialBtn.gameObject.SetActive(true);
	}
}
