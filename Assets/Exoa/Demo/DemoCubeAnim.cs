﻿using DG.Tweening;
using UnityEngine;

public class DemoCubeAnim : MonoBehaviour
{

	public bool rotate;
	public Vector3 rotationVector;

	public bool move;
	public Vector3 offset;
	public float duration = 1;

	void Start()
	{

		if (move)
			transform.DOMove(transform.position + offset, duration).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
	}
	void Update()
	{
		if (rotate) transform.Rotate(rotationVector * Time.deltaTime);
	}
}
