﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialStepEditorView : MonoBehaviour
{

	private Button delBtn;
	private Button addBtn;
	private Button downBtn;
	private Button upBtn;

	void Start()
	{
		delBtn = transform.FindChildRecursiveComp<Button>("DeleteBtn");
		upBtn = transform.FindChildRecursiveComp<Button>("UpBtn");
		downBtn = transform.FindChildRecursiveComp<Button>("DownBtn");

		delBtn.onClick.AddListener(OnClickDel);
		downBtn.onClick.AddListener(OnClickDown);
		upBtn.onClick.AddListener(OnClickUp);
	}

	private void OnClickDel()
	{
		DestroyImmediate(gameObject);
		ResizeContainer();
	}



	private void OnClickAdd()
	{
		GameObject prefab = Resources.Load<GameObject>("TutorialStepEditorView");
		Instantiate(prefab, transform.parent);
	}

	private void OnClickDown()
	{
		int index = transform.GetSiblingIndex();
		index++;
		index = Mathf.Clamp(index, 0, transform.parent.childCount);
		transform.SetSiblingIndex(index);
	}

	private void OnClickUp()
	{
		int index = transform.GetSiblingIndex();
		index--;
		index = Mathf.Clamp(index, 0, transform.parent.childCount);
		transform.SetSiblingIndex(index);
	}

	public TutorialSession.TutorialStep GetStep()
	{
		InputField target = transform.FindChildRecursiveComp<InputField>("TargetObjInput");
		InputField t = transform.FindChildRecursiveComp<InputField>("TextInput");

		TutorialSession.TutorialStep s = new TutorialSession.TutorialStep();
		s.target_obj = target.text;
		s.text = t.text;
		return s;
	}
	public static void AddTutorialStep(string targetObj = "", string text = "")
	{
		GameObject prefab = Resources.Load<GameObject>("TutorialStepEditorView");
		GameObject inst = Instantiate(prefab, GameObject.Find("TutorialSteps").transform);

		InputField target = inst.transform.FindChildRecursiveComp<InputField>("TargetObjInput");
		InputField textInput = inst.transform.FindChildRecursiveComp<InputField>("TextInput");

		target.text = targetObj;
		textInput.text = text;

		ResizeContainer();
	}

	public static void SetAllTutorialSteps(TutorialSession.TutorialStep[] steps)
	{

		GameObject.Find("TutorialSteps").transform.ClearChildrenImmediate();
		ResizeContainer();

		if (steps != null)
			foreach (TutorialSession.TutorialStep step in steps)
				AddTutorialStep(step.target_obj, step.text);
	}
	public static List<TutorialSession.TutorialStep> GetAllTutorialSteps()
	{
		List<TutorialSession.TutorialStep> steps = new List<TutorialSession.TutorialStep>();
		RectTransform container = GameObject.Find("TutorialSteps").GetComponent<RectTransform>();

		for (int i = 0; i < container.childCount; i++)
		{
			TutorialStepEditorView view = container.GetChild(i).GetComponent<TutorialStepEditorView>();
			steps.Add(view.GetStep());
		}
		return steps;
	}



	public static void ResizeContainer()
	{
		RectTransform container = GameObject.Find("TutorialSteps").GetComponent<RectTransform>();
		container.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, container.childCount * 732.47f);
	}
}
