﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class TutorialEditorGenericMenu : MonoBehaviour {

	public Button bgButton;
	public Button openButton;
	private bool isOpen;
	private RectTransform rt;
	public float closedX = -240f;
	public float openX = 240f;
	public float openSeed = 1f;
	public Ease ease;
	
	public virtual void Start()
	{
		rt = GetComponent<RectTransform>();
		Vector3 anchoredPosition = rt.anchoredPosition;
		anchoredPosition.x = closedX;
		rt.anchoredPosition = anchoredPosition;
		openButton.onClick.AddListener(OnClickOpen);
		bgButton.onClick.AddListener(OnClickOpen);
		bgButton.gameObject.SetActive(false);


		BuildMenu();
		TutorialLoader.instance.onTutorialLoaded += onTutorialLoeaded;

	}

	private void onTutorialLoeaded(Button button)
	{
		BuildMenu();
	}

	public virtual void BuildMenu()
	{
		
	}
	
	private void OnClickOpen()
	{
		isOpen = !isOpen;
		CloseMenu(!isOpen);


	}
	public void CloseMenu(bool close = true)
	{
		rt.DOAnchorPosX(close ? closedX : openX, openSeed).SetEase(ease);
		isOpen = !close;
		bgButton.gameObject.SetActive(true);
		bgButton.GetComponent<Image>().DOColor(new Color(0, 0, 0, close ? 0 : .4f), openSeed).OnComplete(() =>
		{
			bgButton.gameObject.SetActive(isOpen);
		});
	}
}
