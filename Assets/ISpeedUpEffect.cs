﻿public interface ISpeedUpEffect
{
    void StartSpeedUpEffect();
    void StopSpeedUpEffect();
}