﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework.Interfaces;
using UnityEngine;
using UnityEngine.UI;

public class EnergyIndictaorView : MonoBehaviour, ITickSec
{
    [SerializeField]
    Text energyTxt;

    [SerializeField]
    Text timeTxt;

    [SerializeField]
    Button addEnergyButton;

    [SerializeField] GameObject adBtn;

    [Inject] MagicShopController magicShop;
    [Inject] EnergyModel energyModel;
    [Inject] UpdateManager updateManager;

    void Start()
    {
        this.Inject();
        updateManager.AddTo(this);
        addEnergyButton.onClick.AddListener(AddEnergyBtnClicked);
        TickSec();
    }

    void AddEnergyBtnClicked()
    {
        magicShop.OpenMagicShop();
    }

    public void TickSec()
    {
        UpdateEnrgyTextIndicator();
        UpdateEnergyTimeTextIndicator();
    }

    void UpdateEnrgyTextIndicator()
    {
        energyTxt.text = energyModel.Energy.ToString() + " / " + energyModel.MaxEnergy.ToString();
    }

    void UpdateEnergyTimeTextIndicator()
    {
        if (energyModel.TimeToFullRechange > 0)
        {
            timeTxt.text = DateMaster.GetFormatedTimeSpan(TimeSpan.FromSeconds(energyModel.TimeToNextRechange));
            //adBtn.SetActive(true);
        }
        else
        {
            timeTxt.text = "";
            //adBtn.SetActive(false);
        }

        if(energyModel.Energy < 1)
            adBtn.SetActive(true);
        else adBtn.SetActive(false);
    }

    private void OnDestroy()
    {
        updateManager.RemoveFrom(this);
        addEnergyButton.onClick.RemoveListener(AddEnergyBtnClicked);
    }


}
