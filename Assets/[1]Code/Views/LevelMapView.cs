﻿using System;
using System.Collections;
using System.Collections.Generic;
using Framework.Interfaces;
using UnityEngine;
using UnityEngine.UI;

public class LevelMapView : MonoBehaviour,IState {

    [SerializeField] Button backBtn;
    [SerializeField] GameObject levelMapItemPrefab;

    [SerializeField] Transform levelMapItemContainer;
    [SerializeField] List<LevelMapItem> levelMapItems;


    int currentSpriteCount;
    int maxSpriteCount = 4;

    int maxLevelIsCompleted;

    public event Action backBtnClicked = delegate {};

    void Start()
    {

        backBtn.onClick.AddListener(() =>
        {
            backBtnClicked();
        });

    }
    public void Load()
    {
        gameObject.SetActive(true);
    }

    public void Unload()
    {
        gameObject.SetActive(false);
    }

    public void InitLevelMap(int maxLevel, int maxCompletedLevel,int _maxLevelIsCompleted)
    {
        maxLevelIsCompleted = _maxLevelIsCompleted;
        StartCoroutine(FillLevelMap(maxLevel,maxCompletedLevel));
    }

    IEnumerator FillLevelMap( int maxLevel, int maxCompletedLevel)
    {
        if(levelMapItems.Count == 0)
        {
            yield return StartCoroutine(BuildLevelMap(maxLevel));
        }
        SetupLevelMapItemsStates(maxLevel, maxCompletedLevel);
        yield return null;
    }

    void SetupLevelMapItemsStates(int maxLevel, int maxCompletedLevel)
    {
        for (int i = 0; i < maxLevel; i++)
        {
            levelMapItems[i].SetLevel(i + 1);
            if (i <= maxCompletedLevel - 1)
            {
                levelMapItems[i].SetUnlockedState();
            }
            else
            {
                levelMapItems[i].SetLockedState();
            }
        }

        if (maxCompletedLevel <= levelMapItems.Count && maxLevelIsCompleted == 0)
        {
            levelMapItems[maxCompletedLevel-1].SetNewLevelState();
        }

        if(maxLevelIsCompleted == 1)
        {
            levelMapItems[maxCompletedLevel - 1].SetUnlockedState();
        }
    }

    IEnumerator BuildLevelMap(int maxLevel)
    {
        for (int i = 0; i < maxLevel; i++)
        {

            LevelMapItem newLevelMapItem = Instantiate(levelMapItemPrefab, levelMapItemContainer).GetComponent<LevelMapItem>();
            newLevelMapItem.SetSprite(currentSpriteCount);
            levelMapItems.Add(newLevelMapItem);
            currentSpriteCount++;
            if (currentSpriteCount > maxSpriteCount)
                currentSpriteCount = 0;
        }
        yield return null;
    }


    void OnDestroy()
    {
        backBtn.onClick.RemoveAllListeners();
    }
}
