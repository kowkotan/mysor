﻿using System.Collections;
using System.Collections.Generic;
using Framework.Interfaces;
using UnityEngine;
using UnityEngine.UI;

public class SettingPanelView : MonoBehaviour,IState,ITickLate
{

    [SerializeField] Button settingsBtn;

    //[SerializeField] ButtonSwitcherView soundBtn;
    [SerializeField] ButtonSwitcherView vibroBtn;

    [SerializeField] SlideToPointUIPanelComponent slidePanel;

   public bool onCloseBtnPressed;

    void Start()
    {
        settingsBtn.onClick.AddListener(() =>
        {
            //onCloseBtnPressed = true;
            slidePanel.SwitchState();
        });
    }

    public void Load()
    {
        gameObject.SetActive(true);
    }

    public void Unload()
    {
        gameObject.SetActive(false);
    }

    //public ButtonSwitcherView GetSoundBtnView()
    //{
    //    return soundBtn;
    //}

    public ButtonSwitcherView GetVibroBtnView()
    {
        return vibroBtn;
    }

    public void TickLate()
    {
        onCloseBtnPressed = false;
    }

    private void OnDestroy()
    {
        settingsBtn.onClick.RemoveAllListeners();
    }

  
}
