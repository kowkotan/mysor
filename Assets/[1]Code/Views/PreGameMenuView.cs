﻿using System;
using Framework.Interfaces;
using UnityEngine;
using UnityEngine.UI;

public class PreGameMenuView : MonoBehaviour,IState {

    [SerializeField] Button playBtn;

    public event Action playBtnClicked = delegate {};

    public void Load()
    {
        gameObject.SetActive(true);
    }

    public void Unload()
    {
        gameObject.SetActive(false);
    }


     void Start  () {

        playBtn.onClick.AddListener(() =>
        {
            playBtnClicked();
        });

    }

    void OnDestroy()
    {
        playBtn.onClick.RemoveAllListeners();
    }
}
