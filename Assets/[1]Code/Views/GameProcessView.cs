﻿using System;
using Framework.Interfaces;
using UnityEngine;
using UnityEngine.UI;

public class GameProcessView : MonoBehaviour,IState {

    [SerializeField] Button restartBtn;
    [SerializeField] Button menuBtn;
    [SerializeField] Button hintsBtn;
  
    [SerializeField] GameObject addHintBtn;

    [SerializeField] Text levelTextField;

    [SerializeField] RewardedVideoButtonView hintRewardedBtnPressed;

    [SerializeField] Text hintsTextField;

    public event Action restartBtnClicked = delegate {};
    public event Action menuBtnClicked = delegate { };
    public event Action hintsBtnClicked = delegate { };

    public void Load()
    {
        gameObject.SetActive(true);
    }

    public void ActiveAddHintBtn()
    {
        addHintBtn.SetActive(true);
    }

    public void DeactiveAddHintBtn()
    {
        addHintBtn.SetActive(false);
    }

    public RewardedVideoButtonView GethintRewardedBtnView()
    {
        return hintRewardedBtnPressed;
    }


    public void Unload()
    {
        gameObject.SetActive(false);
    }

    public void SetHintsTextField(string _text)
    {
        hintsTextField.text = _text;
    }

    public void SetLevelTextFieldText( string _text)
    {
        levelTextField.text = "LEVEL " + _text;
     }


     void Start  () {

        restartBtn.onClick.AddListener(() =>
        {
            restartBtnClicked();
        });

        hintsBtn.onClick.AddListener(() =>
        {
            hintsBtnClicked();
        });

        menuBtn.onClick.AddListener(() =>
        {
            menuBtnClicked();
        });

    }

    void OnDestroy()
    {
        menuBtn.onClick.RemoveAllListeners();
        restartBtn.onClick.RemoveAllListeners();
    }
}
