﻿using System;
using Framework.Interfaces;
using UnityEngine;
using UnityEngine.UI;

public class GameWinPopupView : MonoBehaviour,IState
{

    [SerializeField] Button nextLevelBtn;

    public event Action nextLevelBtnClicked = delegate {};

    public void Load()
    {
        gameObject.SetActive(true);
    }

    public void Unload()
    {
        gameObject.SetActive(false);
    }

    void  Start  () {
        nextLevelBtn.onClick.AddListener(() =>
        {
            nextLevelBtnClicked();
        });

    }

    void OnDestroy()
    {
        nextLevelBtn.onClick.RemoveAllListeners();

    }



}
