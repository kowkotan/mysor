﻿using System.Collections;
using System.Collections.Generic;
using Adic;
//using AppodealAds.Unity.Api;
//using Facebook.Unity;
//using Framework.Interfaces;
using Framework.Managers.Ads;
using Framework.Managers.Statistics;
//using GameAnalyticsSDK;
using UnityEngine;

public class Starter : MonoBehaviour {

    [Inject] BaseAdSystem baseAdSystem;
    [Inject] PreGameMenuController preGameMenuController;
    [Inject] StatisticSystemController statisticSystemController;

    private void Awake()
    {
       // GameAnalytics.Initialize();

        //if (FB.IsInitialized)
        //{
        //    FB.ActivateApp();
        //}
        //else
        //{
        //    FB.Init(() => {
        //        FB.ActivateApp();
        //    });
        //}
    }

    void Start () {
        this.Inject();

        if (!GlobalVars.firstStartFlag)
        {
            GlobalVars.firstStartFlag = true;
            baseAdSystem.Init();
        }
        preGameMenuController.Load();
        baseAdSystem.ShowBanner();

        //statisticSystemController.AddStatisticSystem(new SS_FacebookAnalytics());
        //statisticSystemController.Init();

    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (!pauseStatus)
        {

        }

    }

    void OnApplicationFocus(bool hasFocus)
    {
        if (hasFocus)
        {
           // Appodeal.onResume();
        }
    }

    public void Init()
    {
      
    }
}
