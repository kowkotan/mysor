﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;

public class LevelMapItem : MonoBehaviour
{
    [SerializeField] Text levelTxt;

    [SerializeField] GameObject lockMask;

    [SerializeField] Animator animator;

    [SerializeField] Button chooseLevelBtn;

    [SerializeField] SpriteChooser spriteChooser;

    [SerializeField] GameObject levelCompletedIndi;

    [Inject] ICommandDispatcher commandDispatcher;

    int level;

    void Start()
    {
        this.Inject();
        chooseLevelBtn.onClick.AddListener(() =>
        {
//            commandDispatcher.Dispatch<LoadLevelCommand>(level);
        });

    }

    public void SetSprite(int spriteId)
    {
        spriteChooser.LoadSprite(spriteId);
    }

    public void SetLockedState()
    {
        levelCompletedIndi.gameObject.SetActive(false);
        chooseLevelBtn.enabled = false;
        lockMask.SetActive(true);
    }

    public void SetUnlockedState()
    {
        levelCompletedIndi.gameObject.SetActive(true);
        chooseLevelBtn.enabled = true;
        lockMask.SetActive(false);
    }

    public void SetNewLevelState()
    {
        levelCompletedIndi.gameObject.SetActive(false);
        chooseLevelBtn.enabled = true;
        animator.SetBool("NextLevelIsOn", true);
    }

    public void SetLevel(int _level)
    {
        level = _level;
        levelTxt.text = _level.ToString();
    }

    void OnDestroy()
    {
        chooseLevelBtn.onClick.RemoveAllListeners();
    }
}
