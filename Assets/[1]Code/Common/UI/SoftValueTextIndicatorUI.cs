﻿using Adic;

public class SoftValueTextIndicatorUI : ValueTextIndicatorUIWithButtonBase
{
    [Inject] SoftValueModel detalsModel;
    [Inject] MagicShopController magicShopController;

    void Start()
    {
        this.Inject();
        Init(detalsModel);
    }

    public override void BtnClicked()
    {
        magicShopController.OpenMagicShop();
    }

    public override void Dispose()
    {
        magicShopController = null;
        detalsModel = null;

    }
}
