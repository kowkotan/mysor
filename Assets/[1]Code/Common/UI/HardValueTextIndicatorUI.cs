﻿using Adic;
using UnityEngine;
using UnityEngine.UI;

public class HardValueTextIndicatorUI : ValueTextIndicatorUIWithButtonBase
{
    [Inject] HardValueModel detalsModel;
    [Inject] MagicShopController magicShopController;

    public override void BtnClicked()
    {
        magicShopController.OpenMagicShop();
    }

    void Start()
    {
        this.Inject();
        Init(detalsModel);
    }

    public override void Dispose()
    {
        magicShopController = null;
        detalsModel = null;

    }
}
