﻿using System;
using Adic;
using UnityEngine;
using UnityEngine.UI;

public class DetalsValueTextIndicatorUI : ValueTextIndicatorUIWithButtonBase, IDisposable
{
    [Inject] DetalsModel detalsModel;
    [Inject] MagicShopController magicShopController;

    void Start()
    {
        this.Inject();
        Init(detalsModel);

    }

    public override void BtnClicked()
    {
        Debug.Log("AAAAAA");
        magicShopController.OpenMagicShop();
    }

    public override void Dispose()
    {
        magicShopController = null;
        detalsModel = null;

    }

}
