﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausePanelView : MonoBehaviour
{
    public void Show()
    {
        gameObject.SetActive(true);

        Time.timeScale = 0;
    }

    public void Hide()
    {
        Time.timeScale = 1;

        gameObject.SetActive(false);
    }
}
