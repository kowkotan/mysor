﻿using System;
using System.Collections.Generic;

public static class Helper
{
   public  static Dictionary<int, int> GetNumberOfIdsInArrayAsDictionary(int[] _idArray)
   { 
        var result = new Dictionary<int, int>();
        foreach (var i in _idArray)
        {
            int res;
            if (result.TryGetValue(i, out res))
            {
                result[i] += 1;
            }
            else
            {
                result.Add(i, 1);
            }
        }
        return result;
    }

}

