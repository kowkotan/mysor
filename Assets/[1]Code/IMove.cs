﻿using UnityEngine;

public interface IMove
{
    void Move(Vector3 moveDir);
    bool CanMove();
}