﻿using Adic;
using Framework;
using Framework.Common;
using Framework.Controllers.Loader;
using Framework.Interfaces;
using Framework.Managers.Ads;
using Framework.Managers.Event;
using Framework.Managers.States;
using Framework.Managers.Statistics;
using Framework.Views;
using UnityEngine;

public class ContextMain : BaseMainContext
{
    [SerializeField]
    Starter starter;

    [SerializeField]
     GameWinPopupView gameWinPopupView;

    [SerializeField]
    GameLoosePopupView gameLoosePopupView;

    [SerializeField]
    PreGameMenuView preGameMenuView;

    [SerializeField]
    LevelMapView levelMapView;

    [SerializeField]
    SettingPanelView settingPanelView;

    [SerializeField]
    GameProcessView gameProcessPopupView;

    [SerializeField]
     LoaderView loaderView;

    [SerializeField]
    AudioSystem audioSystem;

    [SerializeField]
    private GameConfig gameConfig;

    public override void Init()
    {
        base.Init();
        starter.Init();
    }

    protected override void BindManagers()
    {
        this.containers[0]
              .Bind<EventManager>().ToGameObject()
            .Bind<UpdateManager>().ToGameObject()
          .Bind<BaseAdSystem>().ToSingleton<DummyAdSystem>()
            .Bind<ISceneLoader>().ToGameObject<DummyLoader>()
            .Bind<StatisticSystemController>().ToGameObject()
             .Bind<IVibrationSystem>().ToSingleton<TapticVibrationSystem>()
              .Bind<IAudioSystem>().To(audioSystem)
              .Bind<LevelLoader>().ToGameObject()

            .Bind<StateManager>().ToGameObject();
    }

    protected override void BindControllers()
    {
        this.containers[0]
          .Bind<GameWinPopupController>().ToSingleton()
            .Bind<RewardedVideoButtonController>().ToSelf()
          .Bind<SoundSwitcherController>().ToSingleton()
         .Bind<VibroSwitcherController>().ToSingleton()
                 .Bind<GameLoosePopupController>().ToSingleton()
                 .Bind<SettingPanelController>().ToSingleton()
                    .Bind<PreGameMenuController>().ToSingleton()
                       .Bind<LevelMapController>().ToSingleton()

           .Bind<GameProcessUIController>().ToSingleton(); /// TODO: сделать главный контроллер, слишком много синглтонов
//         .Bind<GameResultController>().ToSingleton();

    }

    protected override void BindModels()
    {
        this.containers[0].Bind<GameControlModel>().ToSelf()
             .Bind<RewardedVideoButtonModel>().ToSelf()
            .Bind<LevelModel>().ToSingleton()
            .Bind<SwitchModel>().ToSingleton<SwitchSoundModel>().As("soundSwitcherModel")
        .Bind<SwitchModel>().ToSingleton<SwitchVibroModel>().As("vibroSwitcherModel");
    }

    protected override void BindConfigs()
    {
        this.containers[0]
        .Bind<GameConfig>().To(gameConfig);
    }

    protected override void BindView()
    {
        this.containers[0]
         .Bind<LoaderView>().To(loaderView)
         .Bind<GameWinPopupView>().To(gameWinPopupView)
         .Bind<GameLoosePopupView>().To(gameLoosePopupView)
           .Bind<SettingPanelView>().To(settingPanelView)
           .Bind<PreGameMenuView>().To(preGameMenuView)
              .Bind<LevelMapView>().To(levelMapView)
         .Bind<GameProcessView>().To(gameProcessPopupView);

    }

    protected override void BindComponents()
    {
        //this.containers[0]
    }

    protected override void BindCommands()
    {
//        this.containers[0].RegisterCommand<LoadLevelCommand>();
    }
}
