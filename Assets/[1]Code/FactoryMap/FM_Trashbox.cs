﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FM_Trashbox : MonoBehaviour
{
    [Inject] FM_TrashWarehousePanelUI trashWarehousePanelUI;
    [Inject] TrashWarehouse trashWarehouse;

    [SerializeField] TrashMaterials trashMaterial;

    [SerializeField] Sprite trashBoxIcon;

    [SerializeField] Button openBtn;

    public Action OnOpened = delegate {};
    public Action OnClosed = delegate {};

    private void Start()
    {
        this.Inject();
        openBtn.onClick.AddListener(OpenBtnHandler);
    }

    void OpenBtnHandler()
    {

        OnOpened();
        trashWarehousePanelUI.OpenPanel(trashWarehouse.GetTrashCategoryTitle(trashMaterial), trashBoxIcon, trashWarehouse.GetTrashesByMaterial(trashMaterial));
        trashWarehousePanelUI.OnClose += FM_Trashbox_OnClosed;


    }

    void FM_Trashbox_OnClosed()
    {
        OnClosed();
    }


    private void OnDestroy()
    {
        trashWarehousePanelUI.OnClose -= FM_Trashbox_OnClosed;
        openBtn.onClick.RemoveListener(OpenBtnHandler);
    }
}
