﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class FM_WayPoint : MonoBehaviour
{
    [SerializeField]
    public List<GameObject> nextPoints = new List<GameObject>();
    [SerializeField]
    public bool endPoint = false;

#if UNITY_EDITOR
    void Update()
    {
        foreach (var point in nextPoints)
        {
            Debug.DrawLine(transform.position, point.transform.position + new Vector3(0.1f, 0.1f, 0), new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f)), 0.1f);
        }
    }
#endif

    public GameObject GetRandomNextPoint()
    {
        int index = Random.Range(0, nextPoints.Count);
        return nextPoints[index];
    }
}
