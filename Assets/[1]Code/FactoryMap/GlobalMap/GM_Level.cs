﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Adic;
using Framework.Interfaces;

public class GM_Level : MonoBehaviour
{
    [Inject] UpdateManager updateManager;
    [Inject] ISaveLoadSystem saveLoadSystem;
    [Inject] ILevelLocalDatabase levelLocalDatabase;

    LevelDBData levelDBata;

    public string Title
    {
        get
        {
            return levelDBata.title;
        }
    }

    public Sprite levelBGSprite
    {
        get
        {
            return levelDBata.levelSprite;
        }
    }

    [SerializeField] LevelData levelData;
    [SerializeField] GameObject timeState;

    public Action OnInitialized = delegate { };
    public Action ReloadInProgressAction = delegate {};
    public Action ReloadInFinished = delegate {};

    void Start()
    {
        this.Inject();

        Load();

        updateManager.AddTo(this);

        levelDBata = levelLocalDatabase.GetFullData(levelData.id);

        OnInitialized();
    }

    public LevelData GetLevelData()
    {
        return levelData;
    }

    void Load()
    {
       if( saveLoadSystem.IsSaved(levelData.id))
        {
           levelData = saveLoadSystem.LoadStruct<LevelData>(levelData.id);
        }
       else
        {
            Save();
        }
    }

    void Save()
    {
        saveLoadSystem.SaveStruct(levelData.id, levelData);
    }

    private void OnDestroy()
    {
        updateManager.RemoveFrom(this);
        //buildProcessPanelUI.speedUpBtnClicked -= BuildProcessPanelUI_SpeedUpBtnClicked;
        //buildPanelUI.upgradeBtnClicked -= BuildPanelUI_UpgradeBtnClicked;
    }
}
