﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework.Interfaces;
using UnityEngine;
using UnityEngine.UI;

public class GM_LEvelUnlockGate : MonoBehaviour, IGate
{
    [SerializeField] GM_Level level;
    [SerializeField] LevelGate levelGate;

    public bool isInited;
    [Inject] ILevelLocalDatabase levelDB;

    private void Awake() {
        //level.OnInitialized += Level_OnInitialized;
        levelGate.OnInit = OnGatesInited;
    }

    private void Start() {
        this.Inject();
        isInited = true;
        OnGatesInited();
    }

    void OnGatesInited()
    {
        if (levelGate.isInited == false || isInited == false)
            return;

        if (IsUnlocked() == false)
        {
            levelGate.OnUnlocked += OnUnlocked;
            levelGate.ReqLevel = levelDB.GetUnlockLevel(level.GetLevelData().id);

            TryUnlock();
        }
    }

    void OnUnlocked()
    {
        TryUnlock();

        if(IsUnlocked())
        {
            levelGate.OnUnlocked -= OnUnlocked;
        }
    }

    public bool IsUnlocked()
    {
        return levelGate.IsUnlocked() == true;
    }

    public void TryUnlock()
    {
        //factoryGate.TryUnlock();
        if (IsUnlocked())
        {
            gameObject.SetActive(false);
            //unlockState.SetActive(true);
        }
    }

    private void OnDestroy()
    {

        //level.OnInitialized -= Factory_OnInitialized;

        levelGate.OnInit -= OnGatesInited;
        //factoryGate.OnInit -= OnGatesInited;
    }

    public void TickSec()
    {
        if (!IsUnlocked())
        {
            TryUnlock();
        }
    }
}
