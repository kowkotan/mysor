﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Adic;
using Framework.Managers.States;

public class GM_LevelMapMenu : MonoBehaviour
{
    [SerializeField] GM_Level[] level;

    [SerializeField] Text[] titleText;
    [SerializeField] Image[] levelBGImage;

    [Inject] StateManager stateManager;

    public Action OnClosed = delegate {};
    public Action OnOpened = delegate { };

    private void Start() {
        this.Inject();
    }

    public void Show()
    {
        for(int i = 0; i < level.Length; i++)
        {
            levelBGImage[i].sprite = level[i].levelBGSprite;
        }


        this.gameObject.SetActive(true);

        FillTitleTextFields();

        Debug.Log("LEVELS MENU OPENED");
    }

    void FillTitleTextFields()
    {
        for(int i = 0; i < titleText.Length; i++)
            titleText[i].text = level[i].Title;
    }

    public void LoadLevel(string levelName)
    {
        if(levelName == "DesertMap")
            PlayerPrefs.SetString("PlayerLevelTest", "2");
        else PlayerPrefs.SetString("PlayerLevelTest", "1");
        stateManager.ChangeScene(levelName);
    }

    public void Hide()
    {
        OnClosed();
        this.gameObject.SetActive(false);
    }
}
