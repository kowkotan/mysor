﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FM_FactoryPanelController : MonoBehaviour
{
    [Inject] CameraController cameraController;

    [SerializeField] Button factoryOpenBtn;

    [SerializeField] FM_Factory factory;
    [SerializeField] FM_FactoryMenu factoryMenu;

    public FM_ProductionLine productionLine1;
    public FM_ProductionLine productionLine2;
    public FM_ProductionLine productionLine3;

    public FM_ProductionLineView productionLineView;
    public FM_ProductionLineView productionLineView2;
    public FM_ProductionLineView productionLineView3;







    private void Start()
    {
        this.Inject();
        factoryMenu.OnClosed += FactoryMenu_OnClosed;

        factoryOpenBtn.onClick.AddListener(OpenFactoryMenuBtnClicked);
    }

    void OpenFactoryMenuBtnClicked()
    {
        cameraController.GoToPointAndZoom(transform.position, 0.5f, 3.77f, 0.5f, OpenFactoryMenu);
    }


    void FactoryMenu_OnClosed()
    {
        SaveProductProgress();
    }

    void OpenFactoryMenu()
    {
       FactoryData factoryData = factory.GetFactoryData();

        if (factoryData.level == 0 || factoryData.buildInProgress == true)
        {
           
            return;
        }

        productionLine1.Launch();
        productionLineView.Init(productionLine1);

        productionLine2.Launch();
        productionLineView2.Init(productionLine2);

        productionLine3.Launch();
        productionLineView3.Init(productionLine3);

        factoryMenu.Show(factory);
    }



    private void OnDisable()
    {
        SaveProductProgress();
    }

    private void OnDestroy()
    {

        SaveProductProgress();

        factoryOpenBtn.onClick.RemoveListener(OpenFactoryMenuBtnClicked);

        factoryMenu.OnClosed -= FactoryMenu_OnClosed;
    }

    void SaveProductProgress()
    {
        productionLine1.SaveData();
        productionLine2.SaveData();
        productionLine3.SaveData();
    }

}
