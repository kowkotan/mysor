﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FM_FactoryMenu : MonoBehaviour
{
    FM_Factory factory;
    FactoryData factoryData;

    [SerializeField]
    Text titleTxt;

    [SerializeField] GameObject upgradeButtonObjects;
    [SerializeField] Text updPriceTxt;
    [SerializeField] Button upgradeBtnClicked;

    [SerializeField] Text levelTxt;

    [SerializeField] Image factoryLogoImg;
    [SerializeField] Image factoryContainerImg;

    [SerializeField]
    ProgressBarWithText numberOfMatsOnWarehouseProgressBar;

    public Action OnClosed = delegate {};
    public Action OnOpened = delegate { };



    private void Start()
    {
       
   
    }

    void UpgradeBtnClicked()
    {
        factory.UpgradeFactoryBtnClicked();
    }

    public void Show(FM_Factory _factory)
    {
        factory = _factory;
        factoryData = _factory.GetFactoryData();

        factoryLogoImg.sprite = factory.factoryLogoSprite;
        factoryContainerImg.sprite = factory.factoryContainerIconSprite;

        factory.BuildInProgressAction += Factory_BuildInProgressAction;

        gameObject.SetActive(true);

        levelTxt.text = factoryData.level.ToString();
        updPriceTxt.text = factoryData.buyPrice.ToString();

        if(factoryIsReachedMaxLevel())
        {
            upgradeButtonObjects.gameObject.SetActive(false);
        }
        else
        {
            upgradeButtonObjects.gameObject.SetActive(true);
            upgradeBtnClicked.onClick.AddListener(UpgradeBtnClicked);
        }
       
        FillTitleTextField();

        Debug.Log("FACTORY MENU OPENED");
    }

    private void Update()
    {
        numberOfMatsOnWarehouseProgressBar.FillValue((float)factory.numberOfMatsRow, factory.limitOfNumberOfMatsRow);
    }

    bool factoryIsReachedMaxLevel()
    {
        return factoryData.level == factoryData.maxLevel;
    }

    void Factory_BuildInProgressAction()
    {
        Hide();
    }

    void FillTitleTextField()
    {
        titleTxt.text = factory.Title;
    }

    public void Hide()
    {
        OnClosed();

        upgradeBtnClicked.onClick.RemoveListener(UpgradeBtnClicked);
        factory.BuildInProgressAction -= Factory_BuildInProgressAction;
        gameObject.SetActive(false);
    }
}
