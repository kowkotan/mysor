﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class FM_WorkProgressBarUI : MonoBehaviour
{
    [SerializeField]
    ProgressBarWithTextFillingValue progressBarWithTextFilling;

    [SerializeField]
    GameObject speedUpBtnPlaceholder;

    [SerializeField]
    Button speedUpBtn;

    [SerializeField]
    Text speedUpPriceText;

    [SerializeField]
    Image matIconImage;

    [SerializeField]
    Text timeTxt;

    public UnityAction OnSpeedUpBtnPressed;

    private void Start()
    {
        speedUpBtn.onClick.AddListener(OnSpeedUpBtnPressed);
    }

    public void SetMatIconSprite( Sprite _macIconSprite)
    {
        matIconImage.sprite = _macIconSprite;
    }

    public void SetSpeedUpPrice(int _speedUpPrice)
    {
        speedUpPriceText.text = _speedUpPrice.ToString();
    }

    public void ShowProgress(float _minValue, float _maxValue )
    {
        timeTxt.text =  DateMaster.GetFormatedTimeSpan( TimeSpan.FromSeconds(_maxValue - _minValue));
        progressBarWithTextFilling.FillValue(_minValue, _maxValue);
    }

    private void OnDestroy()
    {
        if (OnSpeedUpBtnPressed != null)
        {
            speedUpBtn.onClick.RemoveListener(OnSpeedUpBtnPressed);
        }
    }
}
