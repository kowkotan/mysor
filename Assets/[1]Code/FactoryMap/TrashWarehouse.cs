﻿using System;
using System.Collections.Generic;
using Adic;
using UnityEngine;

public class TrashWarehouse 
{
    List<WarehouseItem> warehouseItems;
    [Inject] ITrashDatabase trashDatabase;


    [Inject]
    public void Init()
    {
       LoadWarehouse();
    }

    public void AddTrashes(Dictionary<int,int> _newTrashesDic)
    {
        foreach (var kvp in _newTrashesDic)
        {
            warehouseItems.Add(new WarehouseItem() { id = kvp.Key, count = kvp.Value });
        }

        SaveWarehouse();
    }

    public void RemoveTrashesOfMat(TrashMaterials _trashMaterial, int _count)
    {
        int remCount = _count;
        for (int i = 0; i < warehouseItems.Count; i++)
        {
            if (trashDatabase.GetMaterial(warehouseItems[i].id) == _trashMaterial)
            {
                WarehouseItem sc = warehouseItems[i];
                sc.count -= 1;
                warehouseItems[i] = sc;
                if(sc.count <= 0)
                    warehouseItems.Remove(sc);
                //warehouseItems.Remove(warehouseItems[i]);
                i--;

                remCount--;
                if (remCount <= 0) return;
            }
        }
        SaveWarehouse();
    }

    public int GetTrashCategoryItemsCount(TrashMaterials _trashMaterial)
    {
        Dictionary<int, int> _trashes = GetTrashesByMaterial(_trashMaterial);
        int count = 0;
        foreach (var kvp in _trashes)
        {
            count += kvp.Value;
        }
        return count;
    }

    public  string GetTrashCategoryTitle(TrashMaterials _trashMaterials)
    {
        switch (_trashMaterials)
        {
            case TrashMaterials.FOOD:
                return "Еда";
                break;
            case TrashMaterials.GLASS:
                return "Стекло";
                break;
            case TrashMaterials.PAPER:
                return "Бумага";
                break;
            case TrashMaterials.PLASTIC:
                return "Пластик";
                break;
        }
        throw new ArgumentNullException();
    }





    public Dictionary<int, int> GetTrashesByMaterial(TrashMaterials _trashMaterial)
    {
        Dictionary<int, int> _trashes = new Dictionary<int, int>();
        foreach (var kvp in warehouseItems)
        {
            if (trashDatabase.GetMaterial(kvp.id) == _trashMaterial)
            {
                if (_trashes.ContainsKey(kvp.id))
                {
                    _trashes[kvp.id] += kvp.count;
                }
                else
                {
                    _trashes.Add((int)kvp.id, kvp.count);
                }
            }
        }
        return _trashes;
    }


    void LoadWarehouse()
    {
        if (!PlayerPrefs.HasKey("wh"))
        {
            warehouseItems = new List<WarehouseItem>();
            return;
        }

        warehouseItems = new List<WarehouseItem>( JsonUtilityHelper.FromJson<WarehouseItem>(PlayerPrefs.GetString("wh")));

    }

    public void SaveWarehouse()
    {
       PlayerPrefs.SetString("wh", JsonUtilityHelper.ToJson(warehouseItems.ToArray(),true));
    }
}
