﻿using System;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FM_FactoryProductSellCell : MonoBehaviour
{

    [SerializeField]
    Text numberOfMatsText;

    [SerializeField]
    Image matIconImage;

    [SerializeField] Text priceText;

    [SerializeField] Button sellBtn;

    [SerializeField]
    GameObject activeState;

    [SerializeField]
    Text reqLevelTxt;

    //[SerializeField] GameObject moneyDollars;


    [SerializeField]
    GameObject lockState;

    Vector3 defScale;

    public Action OnSoldBtnClicked =  delegate {};

    private void Start()
    {
        sellBtn.onClick.AddListener(SellBtnClicked);
        defScale = transform.localScale;
    }

    void SellBtnClicked()
    {
        OnSoldBtnClicked();
    }

    public void OnProductSolded()
    {
        transform.DOPunchScale(Vector3.one * 0.1f, 0.2f, 1, 1).OnComplete(() =>
        {
            transform.localScale = defScale;
        });
        //CreateAnimatedDollars();
    }

    // void CreateAnimatedDollars()
    // {
    //     Instantiate(moneyDollars, new Vector2(transform.position.x, transform.position.y+60), Quaternion.identity);
    // }

    public void OnProductCreated()
    {
        transform.DOPunchScale(Vector3.one * 0.2f, 0.5f, 1, 1).OnComplete(() =>
        {
            transform.localScale = defScale;
        });
    }

    public void Lock()
    {
        activeState.SetActive(false);
        lockState.SetActive(true);
    }

    public void Unlock()
    {
        activeState.SetActive(true);
        lockState.SetActive(false);
    }

    public void SetReqLevel(int _reqLevel)
    {
        reqLevelTxt.text = _reqLevel.ToString();
    }


    public void SetMatIcon( Sprite _newIcon)
    {
        matIconImage.sprite =  _newIcon;
    }

    public Sprite GetMatIcon()
    {
       return matIconImage.sprite;
    }

    public void SetMatNumber(int _matNumber,int _maxMatNumber)
    {
        numberOfMatsText.text = _matNumber.ToString()+"/"+_maxMatNumber.ToString();
    }

    public void SetMatPrice(int _matPrice)
    {
        priceText.text = _matPrice.ToString();
    }

    private void OnDestroy()
    {
        sellBtn.onClick.RemoveListener(SellBtnClicked);
    }

}
