﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FB_BuildProcessPanelUI : MonoBehaviour
{
    [SerializeField] Text priceTxt;
    [SerializeField] Text timeTxt;
    [SerializeField] Button speedUpBtn;

    public Action speedUpBtnClicked = delegate { };

    private void Start()
    {
        speedUpBtn.onClick.AddListener(SpeedUpBtnClicked);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void fillPriceText(int _price)
    {
        priceTxt.text = _price.ToString();
    }

    public void fillTimeElapsedText(TimeSpan _elapsedTime)
    {
        timeTxt.text = _elapsedTime.ToCustomString();
    }

    void SpeedUpBtnClicked()
    {
        speedUpBtnClicked();
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        speedUpBtn.onClick.RemoveListener(SpeedUpBtnClicked);
    }
}
