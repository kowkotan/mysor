﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FM_CarController : MonoBehaviour
{
    static public List<GameObject> AllCars { get; private set; } = new List<GameObject>();

    enum MovingSide
    {
        BottomRight,
        TopLeft,
        BottomLeft,
        TopRight
    }

    public GameObject CurrentRoadPoint { get; set; } = null;
    [Tooltip("First BottomRight direction, second TopLeft direction. Only two elements.")]
    public List<Sprite> SpriteForEachSide = new List<Sprite>();
    public float Speed = 1.5f;
    // Start is called before the first frame update
    void Start()
    {
        AllCars.Add(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (CurrentRoadPoint != null)
        {
            if (Vector2.Distance(CurrentRoadPoint.transform.position, transform.position) <= 0.1f)
            {
                FM_WayPoint oldWayPoint = CurrentRoadPoint.GetComponent<FM_WayPoint>();
                if (oldWayPoint != null)
                {
                    if(oldWayPoint.endPoint)
                    {
                        Destroy(gameObject);
                        return;
                    }
                    TargetNextPoint();
                }
            }

            Vector3 pos = transform.position;
            Vector2 targetPos = CurrentRoadPoint.transform.position;
            pos = (Vector2)pos + (targetPos - (Vector2)pos).normalized * Time.deltaTime * Speed;
            pos.z = -2;
            transform.position = pos;
        }
    }

    public void TargetNextPoint()
    {
        FM_WayPoint oldWayPoint = CurrentRoadPoint.GetComponent<FM_WayPoint>();
        CurrentRoadPoint = oldWayPoint.GetRandomNextPoint();
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        int currentSide = (int)GetCurrentMovingSide();
        spriteRenderer.sprite = SpriteForEachSide[currentSide % 2];
        spriteRenderer.flipX = currentSide >= 2;
    }

    private void OnDestroy()
    {
        AllCars.Remove(gameObject);
    }

    MovingSide GetCurrentMovingSide()
    {
        Vector2 thisPos = transform.position;
        Vector2 targetPos = CurrentRoadPoint.transform.position;
        
        if(thisPos.x <= targetPos.x && thisPos.y <= targetPos.y)
        {
            return MovingSide.TopRight;
        }
        else if(thisPos.x >= targetPos.x && thisPos.y <= targetPos.y)
        {
            return MovingSide.TopLeft;
        }
        else if(thisPos.x <= targetPos.x && thisPos.y >= targetPos.y)
        {
            return MovingSide.BottomRight;
        }
        else if(thisPos.x >= targetPos.x && thisPos.y >= targetPos.y)
        {
            return MovingSide.BottomLeft;
        }

        return MovingSide.BottomLeft;
    }
}
