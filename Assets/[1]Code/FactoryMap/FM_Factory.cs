﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using Framework.Interfaces;
using GoogleMobileAds.Api;
using GameAnalyticsSDK;

public class FM_Factory : MonoBehaviour, ITickSec
{
    [Inject] SoftValueModel softValueModel;
    [Inject] HardValueModel hardValueModel;
    [Inject] UpdateManager updateManager;
    [Inject] TrashWarehouse trashWarehouse;
    [Inject] ISaveLoadSystem saveLoadSystem;
    [Inject] IFactoryLocalDatabase factoryLocalDatabase;
    [Inject] UpgradeFactory upgradeFactory;
    [Inject] BuildFactory buildFactory;

    FactoryDBData factoryDBData;

    [SerializeField] AudioClip build_upgrade;
    [SerializeField] AudioClip speedUp;

    public string Title
    {
        get
        {
            return factoryDBData.title;
        }
    }

    public Sprite factoryLogoSprite
    {
        get
        {
            return factoryDBData.factoryLogo;
        }
    }

    public Sprite factoryContainerIconSprite
    {
        get
        {
            return factoryDBData.containerLogo;
        }
    }

    public TrashMaterials factoryTrashMaterials
    {
        get
        {
            return factoryDBData.trashType;
        }
    }




    public FactoryData factoryData;

    [SerializeField] FM_BuildPanelUI buildPanelUI;
    [SerializeField] FB_BuildProcessPanelUI buildProcessPanelUI;
    [SerializeField] FM_FactoryLevelUpWin factoryLevelUpWinUI;


    [SerializeField]
    Transform lvlVisualTransformRoot;

    [SerializeField]
    GameObject timeState;

    [SerializeField]
    GameObject buildState;

    public int numberOfMatsRow;
    public int limitOfNumberOfMatsRow;
    [SerializeField] GameObject btnToClick;
    [SerializeField] AdFactoryRewarded adFactoryRewarded;

    public int MaxNumberOfProduct
    {
        get
        {
           return BalancedValues.GetFactoryMaxProductLimit(factoryData.level, 1); // TEMP TODO: лимит у каждой линии пр-ства должен быть свой
        }
    }

    public Action OnInitialized = delegate { };
    public Action BuildInProgressAction = delegate {};
    public Action BuildInFinished = delegate {};

    int priceForSpeed;

    private void Awake() {
        GameAnalytics.Initialize();
    }

    void Start()
    {
        this.Inject();

        Load();

        Save();

        updateManager.AddTo(this);

        factoryDBData = factoryLocalDatabase.GetFullData(factoryData.id);

        buildPanelUI.upgradeBtnClicked += BuildPanelUI_UpgradeBtnClicked;
        buildProcessPanelUI.speedUpBtnClicked += BuildProcessPanelUI_SpeedUpBtnClicked;

        UpdateFactoryVisual();

        buildPanelUI.fillFactoryTitle(Title);

        if (factoryData.buildInProgress)
        {
            factoryData.buildStartDate = DateMaster.GetDate(factoryData.id + "StartBuildDate");

            if (BuildingIsFinished())
            {
                BuildFinished();
            }
            else
            {
                BuildInProgressAction();
                ShowUpgradeVisualState();
            }
        }


        numberOfMatsRow = GetNumberOfMatsRowValue(factoryData.level);


        OnInitialized();




    }

    int GetNumberOfMatsRowValue(int _factoryLevel)
    {
        int totalMatsOnThisCategory = trashWarehouse.GetTrashCategoryItemsCount(factoryTrashMaterials);
        limitOfNumberOfMatsRow = BalancedValues.GetFactoryTrashContainerLimit(_factoryLevel);

        return  Mathf.Min(totalMatsOnThisCategory, limitOfNumberOfMatsRow);
    }

    public FactoryData GetFactoryData()
    {
        return factoryData;
    }

    void Load()
    {
       if( saveLoadSystem.IsSaved(factoryData.id))
        {
           factoryData = saveLoadSystem.LoadStruct<FactoryData>(factoryData.id);
        }
       else
        {
            Save();
        }
    }

    public void Save()
    {
        saveLoadSystem.SaveStruct(factoryData.id, factoryData);
    }


    public void DellRawsTrahes(int _count)
    {
        numberOfMatsRow -= _count;
        trashWarehouse.RemoveTrashesOfMat(factoryTrashMaterials, _count);
    }

    void UpdateFactoryVisual()
    {
        ReplaceVisual();
        UpdateUpgradePrice();
        UpdateUpgradeTime();
    }

    void BuildPanelUI_UpgradeBtnClicked()
    {
        UpgradeButtonPressed();
    }

    public void UpgradeFactoryBtnClicked()
    {
        factoryLevelUpWinUI.Snow(this);
        factoryLevelUpWinUI.OnUpgradeBtnClicked += FactoryLevelUpWinUI_OnUpgradeBtnClicked;
        factoryLevelUpWinUI.OnCloseBtnClicked += FactoryLevelUpWinUI_OnCloseBtnClicked;
    }

    void FactoryLevelUpWinUI_OnCloseBtnClicked()
    {
        factoryLevelUpWinUI.OnUpgradeBtnClicked -= FactoryLevelUpWinUI_OnUpgradeBtnClicked;
        factoryLevelUpWinUI.OnCloseBtnClicked -= FactoryLevelUpWinUI_OnCloseBtnClicked;
    }


    void FactoryLevelUpWinUI_OnUpgradeBtnClicked()
    {
        UpgradeButtonPressed();
        factoryLevelUpWinUI.OnUpgradeBtnClicked -= FactoryLevelUpWinUI_OnUpgradeBtnClicked;
        factoryLevelUpWinUI.OnCloseBtnClicked -= FactoryLevelUpWinUI_OnCloseBtnClicked;
    }

     void UpgradeButtonPressed()
    {
        BuildInProgressAction();
        StartBuild();
    }

    void BuildProcessPanelUI_SpeedUpBtnClicked()
    {
        if(hardValueModel.isValueEnough(priceForSpeed))
        {
            BuildFinished();
            this.GetComponent<AudioSource>().clip = speedUp;
            this.GetComponent<AudioSource>().Play();
        }

        hardValueModel.DecValue(priceForSpeed);
    }

    bool BuildingIsFinished()
    {
        return GetSecToEndOfBuild() >= factoryData.upgradeTimeSec;
    }

    int GetSecToEndOfBuild()
    {
        return (int) DateMaster.GetDifference(factoryData.buildStartDate).TotalSeconds;
    }

    public  void StartBuild()
    {
        if (PlayerPrefs.GetInt("factoryTutorialPart1Completed") == 0)
        {
            factoryData.buildInProgress = true;

            Debug.Log("СТРОИТЕЛЬСТВО НАЧАЛОСЬ");
            this.GetComponent<AudioSource>().clip = build_upgrade;
            this.GetComponent<AudioSource>().Play();

            BuildInProgressAction();

            ShowUpgradeVisualState();

            factoryData.buildStartDate = DateTime.Now;

            DateMaster.SaveDate(factoryData.buildStartDate,factoryData.id+"StartBuildDate");

            Save();
        }
        else
        {
            if(softValueModel.isValueEnough(factoryData.buyPrice))
            {
                factoryData.buildInProgress = true;

                Debug.Log("СТРОИТЕЛЬСТВО НАЧАЛОСЬ");
                this.GetComponent<AudioSource>().clip = build_upgrade;
                this.GetComponent<AudioSource>().Play();

                BuildInProgressAction();

                ShowUpgradeVisualState();

                factoryData.buildStartDate = DateTime.Now;

                DateMaster.SaveDate(factoryData.buildStartDate,factoryData.id+"StartBuildDate");

                Save();

                //adFactoryRewarded.RequestRewardedAd();
                MobileAds.Initialize(initStatus => {});
            }

            softValueModel.DecValue(factoryData.buyPrice);
        }
    }

    void BuildFinished()
    {
        Debug.Log("СТРОИТЕЛЬСТВО ЗАВЕРШЕНО");

        if (factoryData.level == 0)
        {
            Dictionary<String, System.Object> eventParameters = new Dictionary<String, System.Object>();
            eventParameters.Add("Постройка", "Построил завод по " + factoryDBData.title + " на 1 уровне");

            AppMetrica.Instance.ReportEvent("Строительство", eventParameters);
        }
        else if (factoryData.level > 0)
        {
            Dictionary<String, System.Object> eventParameters = new Dictionary<String, System.Object>();
            eventParameters.Add("Повышение", "Повысил " + factoryData.level + " уровень " + " завода по " + factoryDBData.title + " на 1 уровне");
            AppMetrica.Instance.ReportEvent("Строительство", eventParameters);
        }
        AppMetrica.Instance.SendEventsBuffer();


        if (PlayerPrefs.GetString("BuildQuestFactoryNameForUpgrade") == "по производству еды" && factoryData.id == "foodFactory1")
        {
            buildFactory.Increment();
        }
        else if (PlayerPrefs.GetString("BuildQuestFactoryNameForUpgrade") == "по производству бумаги" && factoryData.id == "paperFactory1")
        {
            buildFactory.Increment();
        }
        else if (PlayerPrefs.GetString("BuildQuestFactoryNameForUpgrade") == "по производству пластика" && factoryData.id == "plasticFactory1")
        {
            buildFactory.Increment();
        }
        else if (PlayerPrefs.GetString("BuildQuestFactoryNameForUpgrade") == "по производству стекла" && factoryData.id == "glassFactory1")
        {
            buildFactory.Increment();
        }

        if (PlayerPrefs.GetString("CollectQuestFactoryNameForUpgrade") == "по производству еды" && factoryData.id == "foodFactory1")
        {
            upgradeFactory.Increment();
        }
        else if (PlayerPrefs.GetString("CollectQuestFactoryNameForUpgrade") == "по производству бумаги" && factoryData.id == "paperFactory1")
        {
            upgradeFactory.Increment();
        }
        else if (PlayerPrefs.GetString("CollectQuestFactoryNameForUpgrade") == "по производству пластика" && factoryData.id == "plasticFactory1")
        {
            upgradeFactory.Increment();
        }
        else if (PlayerPrefs.GetString("CollectQuestFactoryNameForUpgrade") == "по производству стекла" && factoryData.id == "glassFactory1")
        {
            upgradeFactory.Increment();
        }

        factoryData.buildInProgress = false;
        buildState.SetActive(false);
        factoryData.level++;
        factoryData.isAdSpeeded = false;
        btnToClick.SetActive(true);

        numberOfMatsRow = GetNumberOfMatsRowValue(factoryData.level);

        UpdateFactoryVisual();
        Save();

        BuildInFinished();
    }

    void ShowUpgradeVisualState()
    {
        buildState.SetActive(true);
        buildPanelUI.gameObject.SetActive(false);
    }


    private void UpdateUpgradePrice()
    {
        if(factoryData.level == 0)
        {
            factoryData.buyPrice = BalancedValues.GetFactoryUnlockPrice(factoryTrashMaterials);
        }
        else
        {
            factoryData.buyPrice = BalancedValues.GetFactoryUpgradePrice(factoryData.level - 1);
        }
  
        buildPanelUI.fillPriceText(factoryData.buyPrice);
    }

    void UpdateUpgradeTime()
    {
        if (factoryData.level == 0)
        {
            //factoryData.buyPrice = BalancedValues.GetFactoryUnlockTime(factoryTrashMaterials);
        }
        else
        {
            factoryData.upgradeTimeSec = BalancedValues.GetFactoryUpgradeTime(factoryData.level - 1);
        }
    }

    void ClearOtherVisuals()
    {
        foreach (Transform child in lvlVisualTransformRoot)
        {
            child.gameObject.SetActive(false);
        }
    }

    void ReplaceVisual()
    {
        ClearOtherVisuals();
        lvlVisualTransformRoot.GetChild(factoryData.level).gameObject.SetActive(true);
    }

    public void SetSpeed()
    {
        factoryData.isAdSpeeded = true;
        Save();
    }

    public void TickSec()
    {
        TimeSpan elapsedTime;
        if (factoryData.buildInProgress)
        {
            if(!factoryData.isAdSpeeded)
                elapsedTime = GetElapsedTimeSpan();
            else if(factoryData.isAdSpeeded && factoryData.upgradeTimeSec > 1800)
            {
                elapsedTime = GetElapsedADSpeedTimeSpan();
                btnToClick.SetActive(false);
            }
            else
                BuildFinished();

            Debug.Log("СТРОИТЕЛЬСТВО В ПРОЦЕССЕ:" + elapsedTime.ToString());

            priceForSpeed = BalancedValues.GetFactoryBuildSpeedUpPrice((int)elapsedTime.TotalSeconds);
            buildProcessPanelUI.fillPriceText(BalancedValues.GetFactoryBuildSpeedUpPrice((int)elapsedTime.TotalSeconds));
            buildProcessPanelUI.fillTimeElapsedText(elapsedTime);

            if (BuildingIsFinished()) BuildFinished();
        }
    }

    TimeSpan GetElapsedTimeSpan()
    {
        return DateMaster.GetDifference(factoryData.buildStartDate.AddSeconds(factoryData.upgradeTimeSec));
    }

    TimeSpan GetElapsedADSpeedTimeSpan()
    {
        return DateMaster.GetDifference(factoryData.buildStartDate.AddSeconds(factoryData.upgradeTimeSec - 1800));
    }

    private void OnDestroy()
    {
        updateManager.RemoveFrom(this);
        buildProcessPanelUI.speedUpBtnClicked -= BuildProcessPanelUI_SpeedUpBtnClicked;
        buildPanelUI.upgradeBtnClicked -= BuildPanelUI_UpgradeBtnClicked;
    }
}
