﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FM_ProductionLineView : MonoBehaviour
{
    [SerializeField]
    FM_FactoryRawMatCell factoryRawMatCell;

    [SerializeField]
    ProgressBarWithText numberOfMatsOnWarehouseProgressBar;

    [SerializeField]
    FM_FactoryProductSellCell factoryProductSellCell;

    [SerializeField] 
    FM_WorkProgressBarUI workProgressBarUI;

    public FM_ProductionLine productionLine;

    bool isWorked;

    private void Awake()
    {
       // 
    }

    private void OnEnable()
    {
        SubscribeToEvents();
    }

    private void OnDisable()
    {
        UnSubscribeToEvents();
    }

    public Sprite GetProductSprite()
    {
       return factoryProductSellCell.GetMatIcon();
    }

    void UpdateSoldMatNumberAndPrice()
    {

        factoryProductSellCell.SetMatNumber(productionLine.NumberOfProduct,productionLine.MaxNumberOfProduct);
        factoryProductSellCell.SetMatPrice(productionLine.GetSoldPrice());
    }

     void SubscribeToEvents()
    {

        workProgressBarUI.OnSpeedUpBtnPressed += WorkProgressBarUI_OnSpeedUpBtnPressed;

        factoryRawMatCell.OnSwitchWorkModeBtnClicked += FactoryRawMatCell_OnSwitchWorkModeBtnClicked;
        factoryProductSellCell.OnSoldBtnClicked += FactoryProductSellCell_OnSoldBtnClicked;

        productionLine.OnProductCreated += ProductionLine_OnProductCreated;
        productionLine.OnProductsSolded += ProductionLine_OnProductsSolded;
        productionLine.WorkIsStarted += ProductionLine_WorkIsStarted;
        productionLine.WorkIsFinished += ProductionLine_WorkIsFinished;
        productionLine.NotEnoughMaterials += ProductionLine_NotEnoughMaterials;

    }

    void WorkProgressBarUI_OnSpeedUpBtnPressed()
    {
        productionLine.SpeedUpProductCreationProcess();
    }


    void UnSubscribeToEvents()
    {
        workProgressBarUI.OnSpeedUpBtnPressed -= WorkProgressBarUI_OnSpeedUpBtnPressed;

        factoryRawMatCell.OnSwitchWorkModeBtnClicked -= FactoryRawMatCell_OnSwitchWorkModeBtnClicked;
        factoryProductSellCell.OnSoldBtnClicked -= FactoryProductSellCell_OnSoldBtnClicked;

        productionLine.OnProductCreated -= ProductionLine_OnProductCreated;
        productionLine.OnProductsSolded -= ProductionLine_OnProductsSolded;
        productionLine.WorkIsStarted -= ProductionLine_WorkIsStarted;
        productionLine.WorkIsFinished -= ProductionLine_WorkIsFinished;
        productionLine.NotEnoughMaterials -= ProductionLine_NotEnoughMaterials;
    }

    public void Init(FM_ProductionLine _productionLine)
    {
        isWorked = _productionLine.isAvailble();

        if (isWorked)
        {
            updateWorkIsStartedVisual();
        }
        else
        {
            ProductionLine_WorkIsFinished();
        }


        productionLine = _productionLine;

       
        factoryRawMatCell.SetFactoryItemIcon(_productionLine.GetFactoryIconSprite());

        UpdateSoldMatNumberAndPrice();

        factoryRawMatCell.SetMatIcon(_productionLine.GetRawMatSprite());
        factoryProductSellCell.SetMatIcon(_productionLine.GetProductMatSprite());
        workProgressBarUI.SetMatIconSprite(_productionLine.GetProductMatSprite());

        if (_productionLine.isLevelLocked() )
        {

            factoryRawMatCell.Lock();
            factoryProductSellCell.Lock();

            factoryProductSellCell.SetReqLevel(_productionLine.reqLevel);
            factoryRawMatCell.SetReqLevel(_productionLine.reqLevel);
        }
        else
        {
            factoryRawMatCell.Unlock();
            factoryProductSellCell.Unlock();

            factoryRawMatCell.SetCountOfrawMatsForOneProduct(_productionLine.CountOfRawMatsForOneProduct);
  
        }
    }

    void FactoryRawMatCell_OnSwitchWorkModeBtnClicked()
    {
        productionLine.SwitchWorkStatus();
    }


    void ProductionLine_NotEnoughMaterials()
    {
        factoryRawMatCell.NotEnoughMaterials();
    }


    void ProductionLine_WorkIsFinished()
    {
        workProgressBarUI.gameObject.SetActive(false);
        factoryRawMatCell.WorkFinished();
    }


    void ProductionLine_WorkIsStarted()
    {

        updateWorkIsStartedVisual();
        isWorked = productionLine.isAvailble();
    }

    void updateWorkIsStartedVisual()
    {
        workProgressBarUI.gameObject.SetActive(true);
        factoryRawMatCell.WorkStarted();
    }

    public void WorkProgressBarStatus( bool _workProgressBar)
    {
        workProgressBarUI.gameObject.SetActive(_workProgressBar);
    }

    void ProductionLine_OnProductsSolded()
    {
        factoryProductSellCell.OnProductSolded();
        UpdateSoldMatNumberAndPrice();
    }


    void ProductionLine_OnProductCreated()
    {
        factoryProductSellCell.SetMatNumber(productionLine.NumberOfProduct,productionLine.MaxNumberOfProduct);
        factoryProductSellCell.SetMatPrice(productionLine.GetSoldPrice());

        factoryRawMatCell.OnProductCreated();
        factoryProductSellCell.OnProductCreated();

    }

    void FactoryProductSellCell_OnSoldBtnClicked()
    {
        productionLine.SoldProducts();
    }

    public void SetWorkStatus( bool workStatus )
    {
        if (workStatus)
        {
            workProgressBarUI.SetMatIconSprite(productionLine.GetProductMatSprite());
        }
    }

    private void Update()
    {
        if (!isWorked) return;

        if(productionLine.isAvailble()  )
        {

            workProgressBarUI.ShowProgress(productionLine.currentTime, productionLine.timeToNextProduct);

            workProgressBarUI.SetSpeedUpPrice(productionLine.GetSpeedUpPrice());

            factoryRawMatCell.SetCountOfrawMatsForOneProduct(productionLine.CountOfRawMatsForOneProduct);

            numberOfMatsOnWarehouseProgressBar.FillValue( (float) productionLine.NumberOfMatsOnContainer , 99f);
        }

        CheckForMats();
    }

    public void CheckForMats()
    {
        if(factoryRawMatCell.testr > productionLine.NumberOfMatsOnContainer)
                factoryRawMatCell.SetRed();
            else factoryRawMatCell.SetGreen();
    }
}
