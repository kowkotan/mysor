﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FM_TrashWarehousePanelUI : MonoBehaviour
{
    [SerializeField] TrashResultPanel trashResultPanel;
    [SerializeField] Text titile;
    [SerializeField] Image categoryImg;

    public Action OnClose = delegate {};

    public void OpenPanel(string _trashCategoryName, Sprite _categoryContSprite, Dictionary<int,int> _trashes)
    {
        gameObject.SetActive(true);
        titile.text = _trashCategoryName;
        categoryImg.sprite = _categoryContSprite;
        trashResultPanel.FillPanel(_trashes);
    }


    public void ClosePanel()
    {
        OnClose();
        gameObject.SetActive(false);
    }

}
