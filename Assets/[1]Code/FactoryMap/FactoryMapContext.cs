﻿using Adic;
using Framework;
using Framework.Common;
using Framework.Controllers.Loader;
using Framework.Managers.Event;
using Framework.Managers.States;
using Framework.Views;
using UnityEngine;

public class FactoryMapContext : BaseMainContext
{
    [SerializeField] LoaderView loaderView;
    [SerializeField] CameraController cameraController;

    [SerializeField] TrashLocalDatabase trashDatabase;
    [SerializeField] FactoryLocalDatabase factoryDatabase;
    [SerializeField] LocationLocalDatabase locationLocalDatabase; 
    [SerializeField] LevelLocalDatabase levelLocalDatabase;

    [SerializeField] FM_TrashWarehousePanelUI trashWarehousePanelUI;


    public override void Init()
    {
    

        base.Init();
       
    }

    protected override void BindCommands()
    {
      ///  throw new System.NotImplementedException();
    }

    protected override void BindComponents()
    {
    //    throw new System.NotImplementedException();
    }

    protected override void BindConfigs()
    {
      ///  throw new System.NotImplementedException();
    }

    protected override void BindControllers()
    {
        this.containers[0].Bind<CameraController>().To(cameraController);
            //.Bind<RobotController>().ToSelf();
    }

    protected override void BindManagers()
    {
        this.containers[0]
        .Bind<ITrashDatabase>().To(trashDatabase)
          .Bind<IFactoryLocalDatabase>().To(factoryDatabase)
            .Bind<ILocationLocalDatabase>().To(locationLocalDatabase)
              .Bind<ILevelLocalDatabase>().To(levelLocalDatabase)
        .Bind<ISceneLoader>().ToGameObject<ControllerLoader>()
          .Bind<ISaveLoadSystem>().ToSingleton<SaveLoadSystem>()
                 .Bind<EventManager>().ToSingleton<EventManager>()
                  .Bind<LevelModel>().ToGameObject()
            .Bind<LevelUpPanelController>().ToGameObject()
        .Bind<StateManager>().ToGameObject()
        .Bind<LevelLoader>().ToGameObject()
        .Bind<UpdateManager>().ToGameObject()
                .Bind<TrashWarehouse>().ToSingleton();

    }

    protected override void BindModels()
    {

        this.containers[0].Bind<SoftValueModel>().ToSingleton()
        .Bind<HardValueModel>().ToSingleton()
        .Bind<EnergyModel>().ToGameObject();
    }

    protected override void BindView()
    {
        this.containers[0]
         .Bind<LoaderView>().To(loaderView)
        .Bind<FM_TrashWarehousePanelUI>().To(trashWarehousePanelUI);

    }
}
