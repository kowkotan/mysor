﻿using UnityEngine;
using System;
using System.Collections;

[System.Serializable]
public struct WarehouseItem 
{
    public int id;
    public  int count;
}
