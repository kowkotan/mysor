﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct FM_Product 
{
    public Sprite rawMaterialsSprite;
    public Sprite productMaterialsSprite;

    public int processingTimeInSec;
    public int countOfrawMatsForOneProduct;
    public int priceForOne;
}
