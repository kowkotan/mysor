﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;

public class FM_ProductionLine : MonoBehaviour
{
    [SerializeField] FM_Product product;
    [SerializeField] FM_Factory factory;
    [SerializeField] string productLineId;
    [Inject] SoftValueModel softValueModel;
    [Inject] HardValueModel hardValueModel;
    [Inject] LevelModel levelModel;
    [Inject] TrashWarehouse trashWarehouse;
    [Inject] CreateTrash createTrash;

    public int numberOfPruductionLine;

    public int reqLevel;
    private bool isAvaible;
     bool infinityRaws = false; //



   public bool workInProgressStatus;

    [SerializeField]
    bool isInitialized;


    public int NumberOfProduct => numberOfProduct;
    public int MaxNumberOfProduct
    {
        get
        {
            return  factory.MaxNumberOfProduct;
        }
    }



    public Action OnProductCreated = delegate {};
    public Action OnProductsSolded = delegate { };

    public Action WorkIsStarted = delegate { };
    public Action WorkIsFinished = delegate { };
    public Action NotEnoughMaterials = delegate { };


    [SerializeField]
    int numberOfProduct;
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip speedup;
    [SerializeField] AudioClip start;

    public int NumberOfMatsOnContainer 
    {
        get
        {
       
            return factory.numberOfMatsRow;
        }

    }

    public int CountOfRawMatsForOneProduct
    {
        get
        {
            return BalancedValues.GetProductCreatingCost(factory.GetFactoryData().level, numberOfPruductionLine);
        }

    }

    public int DefTimeToNextProduct => product.processingTimeInSec;


    DateTime lastLaunchDate;

    TimeSpan lastSellDateElapsTimeSpan;

   public   float currentTime;
   
    [HideInInspector]
    public float timeToNextProduct;
   

    private void Start()
    {
        this.Inject();
    }

    void Factory_OnInitialized()
    {
         //  ActivateProductionLine();
            Launch();

    }


    public void Launch()
    {
        LoadWorkStatus();

        isAvaible = isAvailble();

        if (isAvailble() == false || isInitialized == true) return;

        Init();
    }

    public void SwitchWorkStatus()
    {
        if(workInProgressStatus)
        {
           // TurnOffWorkStatus();
        }
        else
        {
            TurnOnWorkStatus();
        }
    }

    public void TurnOnWorkStatus()
    {
        if (CountOfRawMatsForOneProduct <= NumberOfMatsOnContainer)
        {
            if (workInProgressStatus == false)
            {
                currentTime = 0;
                workInProgressStatus = true;


                Delete();
                trashWarehouse.SaveWarehouse();
                audioSource.clip = start;
                audioSource.Play();

                SaveData();
                SaveWorkStatus();
                if (isInitialized == false)
                {
                    Launch();
                }
                WorkIsStarted();
            }
        }
        else
        {
            NotEnoughMaterials();
        }
    }

    public void TurnOffWorkStatus()
    {
        if (workInProgressStatus)
        {
            workInProgressStatus = false;
            currentTime = 0;
            isInitialized = false;
            WorkIsFinished();
            SaveWorkStatus();
        }
    }

     void LoadWorkStatus()
    {
        if(infinityRaws)
        {
            workInProgressStatus = true;
            return;
        }

        workInProgressStatus = PlayerPrefs.GetInt(productLineId + "WorkStatus") == 1;
    }

    void SaveWorkStatus()
    {
        int workStatusBinary = 0;

        if (workInProgressStatus == true)
            workStatusBinary = 1;
        else
            workStatusBinary = 0;

        PlayerPrefs.SetInt(productLineId + "WorkStatus", workStatusBinary);
    }

    void Init()
    {
        lastLaunchDate = DateMaster.GetDate(productLineId + "LaunchDate");
        numberOfProduct = PlayerPrefs.GetInt(productLineId + "NumberOfProduct");

        lastSellDateElapsTimeSpan = DateTime.Now.Subtract(lastLaunchDate);

        timeToNextProduct = BalancedValues.GetFactoryCreatingNewProductTime(factory.GetFactoryData().level,numberOfPruductionLine);

        double totalOfflineProduct = lastSellDateElapsTimeSpan.TotalSeconds / timeToNextProduct;

        int newProducts = 0;
        int avaibleProducts;


        newProducts = Convert.ToInt32(Math.Min(Math.Truncate(totalOfflineProduct), MaxNumberOfProduct));
        avaibleProducts = newProducts;

        if (!infinityRaws)
        {
            avaibleProducts = Convert.ToInt32(Math.Truncate((double)(NumberOfMatsOnContainer / CountOfRawMatsForOneProduct)));
            newProducts =  Math.Min(newProducts, avaibleProducts);


            if (newProducts >= 1)
            {
                newProducts = 1;
                TurnOffWorkStatus();
            }
            //else
            //{
                //if (NumberOfMatsOnContainer < CountOfRawMatsForOneProduct) TurnOffWorkStatus();
            //}
        }

        numberOfProduct += newProducts;
        currentTime = (float)(lastSellDateElapsTimeSpan.TotalSeconds - (newProducts * timeToNextProduct));

        if (avaibleProducts == 0 || currentTime > timeToNextProduct) currentTime = 0;

        //if (!infinityRaws)
        //{ 
            //factory.DellRawsTrahes(newProducts);
        //}

        PlayerPrefs.SetInt(productLineId + "NumberOfProduct", numberOfProduct);


        Debug.Log(string.Format("Вас небыло {0} секунд, произведено {1} следующий товар через {2} секунд ЛИНИЯ {3}", lastSellDateElapsTimeSpan.TotalSeconds,
        Mathf.CeilToInt(Mathf.Round((float)newProducts) ),
           currentTime, productLineId));

        factory.BuildInFinished += Factory_BuildInFinished;

        isInitialized = true;


    }


    public void SaveData()
    {
        DateMaster.SaveDate(DateTime.Now, productLineId + "LaunchDate");
    }


    public Sprite GetFactoryIconSprite()
    {
        return factory.factoryLogoSprite;
    }

    public int GetSoldPrice()
    {
        return BalancedValues.GetProductSoldOneCost(factory.GetFactoryData().level, numberOfPruductionLine);
    }

    public void SoldProducts()
    {
        if (numberOfProduct <= 0) return;

        softValueModel.AddValue( GetSoldPrice());
        levelModel.AddExp(GetSoldPrice());

        numberOfProduct --;
        OnProductsSolded();
        PlayerPrefs.SetInt(productLineId + "NumberOfProduct", numberOfProduct);
    }


    public Sprite GetRawMatSprite()
    {
        return product.rawMaterialsSprite;
    }

    public Sprite GetProductMatSprite()
    {
        return product.productMaterialsSprite;
    }

    public float GetProcessFinishRation()
    {
        return currentTime / timeToNextProduct;
    }

    void Factory_BuildInFinished()
    {
        isAvaible = isAvailble();

        timeToNextProduct = BalancedValues.GetFactoryCreatingNewProductTime( factory.GetFactoryData().level, numberOfPruductionLine);

    }

    public bool isAvailble()
    {
        return (isLevelLocked() == false && workInProgressStatus == true);
          
    }

    public bool isLevelLocked()
    {
        return factory.GetFactoryData().level < reqLevel;
    }

    public int GetSpeedUpPrice()
    {
        return BalancedValues.GetSpeedUpProcessProductPrice(Mathf.CeilToInt(currentTime-timeToNextProduct));
    }

    public void SpeedUpProductCreationProcess()
    {
        if (hardValueModel.DecValue(GetSpeedUpPrice()))
        {
            currentTime = timeToNextProduct;
            audioSource.clip = speedup;
            audioSource.Play();
        }
    }

    void Update()
    {
        if (isInitialized == false) return;

        if (isAvailble() && numberOfProduct < MaxNumberOfProduct)
        {
            currentTime += Time.deltaTime;
            if(currentTime >= timeToNextProduct )
            {
                Debug.Log("НОВЫЙ ПРОДУКТ");
                numberOfProduct++;
                OnProductCreated();

                if (PlayerPrefs.GetString("CreatingQuestTrashType") == "FOOD" && factory.factoryData.id == "foodFactory1")
                    createTrash.Increment();
                else if (PlayerPrefs.GetString("CreatingQuestTrashType") == "PAPER" && factory.factoryData.id == "paperFactory1")
                    createTrash.Increment();
                else if (PlayerPrefs.GetString("CreatingQuestTrashType") == "PLASTIC" && factory.factoryData.id == "plasticFactory1")
                    createTrash.Increment();
                else if (PlayerPrefs.GetString("CreatingQuestTrashType") == "GLASS" && factory.factoryData.id == "glassFactory1")
                    createTrash.Increment();

                //factory.DellRawsTrahes(product.countOfrawMatsForOneProduct);
                //PlayerPrefs.SetInt(productLineId + "NumberOfProduct", numberOfProduct);
                currentTime = 0;

                if (!infinityRaws) TurnOffWorkStatus();
            }
        }
    }

    public void Delete()
    {
        if(isAvailble())
        {
            factory.DellRawsTrahes(product.countOfrawMatsForOneProduct);
            PlayerPrefs.SetInt(productLineId + "NumberOfProduct", numberOfProduct);
        }
    }

    public void Stop()
    {

        //PlayerPrefs.SetInt(productLineId + "LastNumberOfMats", factory.numberOfMatsRow);

        if (factory.BuildInFinished != null)
        factory.BuildInFinished -= Factory_BuildInFinished;
        SaveData();
    }

    private void OnDisable()
    {
        Stop();
        SaveData();
    }

}
