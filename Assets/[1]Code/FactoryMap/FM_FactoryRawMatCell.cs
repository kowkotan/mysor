﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class FM_FactoryRawMatCell : MonoBehaviour
{
    [SerializeField] FM_ProductionLine productionLine;
    [SerializeField] FM_ProductionLineView productionLineView;
    [SerializeField] Text countOfrawMatsForOneProductText;

    [SerializeField]
    Text reqLevelTxt;

    [SerializeField]
    GameObject lockState;

    [SerializeField]
    GameObject numberOfMatsHolder;

    [SerializeField]
    GameObject workInProgressState;

    [SerializeField] Button switchWorkStatusBtn;

    [SerializeField]
    Image matIconImage;

    [SerializeField] GameObject unlockedState;

    [SerializeField]
    Image factoryLogoIcon;

    Vector3 defPos;
    Vector3 defScale;

    public Action OnSwitchWorkModeBtnClicked = delegate { };

    public int testr;

    private void Start()
    {
        defPos = transform.position;
        defScale = transform.localScale;

        switchWorkStatusBtn.onClick.AddListener(SwitchWorkStatusBtnClicked);
    }

    void SwitchWorkStatusBtnClicked()
    {
        OnSwitchWorkModeBtnClicked();
    }


    public void SetCountOfrawMatsForOneProduct(int _countOfrawMatsForOneProduct)
    {
        if (_countOfrawMatsForOneProduct == 0)
        {
            numberOfMatsHolder.SetActive(false);
        }

        testr = _countOfrawMatsForOneProduct;

        countOfrawMatsForOneProductText.text = "x" + _countOfrawMatsForOneProduct.ToString();

        productionLineView.CheckForMats();
    }

    public void SetGreen()
    {
        countOfrawMatsForOneProductText.color = Color.white;
    }

    public void SetRed()
    {
        countOfrawMatsForOneProductText.color = Color.red;
    }

    public void WorkStarted()
    {
        //productionLineView.CheckForMats();
        //productionLine.Delete();
        workInProgressState.SetActive(true);
        transform.DOPunchScale(Vector3.one * 0.1f, 0.2f, 1, 1).OnComplete(() =>
        {
            transform.localScale = defScale;
        });
    }


    public void WorkFinished()
    {
        workInProgressState.SetActive(false);
    }

    public void NotEnoughMaterials()
    {
        transform.DOPunchPosition(new Vector3(5, 0, 0), 0.2f, 2, 1).OnComplete(() =>
        {
            transform.position = defPos;
        });
    }


    public void Lock()
    {
        numberOfMatsHolder.SetActive(false);
        lockState.SetActive(true);
        unlockedState.SetActive(false);
    }

    public void SetFactoryItemIcon(Sprite _newIcon)
    {
        factoryLogoIcon.sprite = _newIcon;
    }

    public void OnProductCreated()
    {
        //if(testr < productionLine.NumberOfMatsOnContainer)
        // transform.DOPunchScale(Vector3.one * 0.2f, 0.5f, 1, 1).OnComplete(() =>
        //{
        //    transform.localScale = defScale;
        //});

    }

    //public void SetMatNumber(int _matNumber)
    //{
    //    numberOfMatsText.text = _matNumber.ToString();
    //}

    public void Unlock()
    {
        numberOfMatsHolder.SetActive(true);
        lockState.SetActive(false);
        unlockedState.SetActive(true);
    }


    public void SetReqLevel(int _reqLevel)
    {
        reqLevelTxt.text = _reqLevel.ToString();
    }

    public void SetMatIcon(Sprite _newIcon)
    {
        matIconImage.sprite = _newIcon;
    }

    private void OnDestroy()
    {
        switchWorkStatusBtn.onClick.RemoveListener(SwitchWorkStatusBtnClicked);
    }
}
