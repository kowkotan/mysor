﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FM_BuildPanelUI : MonoBehaviour
{
    [SerializeField] Text priceTxt;
    [SerializeField] Text factoryTitleTxt;
    [SerializeField] Button buildButton;



    public  Action upgradeBtnClicked = delegate {};

    private void Start()
    {
        buildButton.onClick.AddListener(BuildButtonClicked);

    }

    void Factory_BuildInProgressAction()
    {
        gameObject.SetActive(false);
    }


    public void fillFactoryTitle( string _title)
    {
        factoryTitleTxt.text = "Завод " + _title;
    }

    public void fillPriceText( int _price )
    {
        priceTxt.text = _price.ToString();
    }

    void BuildButtonClicked()
    {
        upgradeBtnClicked();
    }

    private void OnDestroy()
    {

        buildButton.onClick.RemoveListener(BuildButtonClicked);
    }

}
