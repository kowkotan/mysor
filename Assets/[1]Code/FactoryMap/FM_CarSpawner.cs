﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FM_CarSpawner : MonoBehaviour
{
    public int CarCount = 10;
    public List<GameObject> CarTemplate = new List<GameObject>();

    private List<GameObject> Cars = new List<GameObject>();
    private List<GameObject> SpawnPoints = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            FM_WayPoint point = transform.GetChild(i).GetComponent<FM_WayPoint>();
            if (point != null && point.endPoint)
            {
                SpawnPoints.Add(transform.GetChild(i).gameObject);
            }
        }

        if (CarTemplate != null && CarCount > 0)
        {
            for (int i = FM_CarController.AllCars.Count; i < CarCount; i++)
            {
                SpawnCar();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(FM_CarController.AllCars.Count < CarCount)
        {
            SpawnCar();
        }
    }

    void SpawnCar()
    {
        int roadRandomIndex = Random.Range(0, SpawnPoints.Count);
        int carRandomIndex = Random.Range(0, CarTemplate.Count);
        GameObject currentRoadPoint = SpawnPoints[roadRandomIndex];
        GameObject template = CarTemplate[carRandomIndex];
        Vector3 newPosition = currentRoadPoint.transform.position;
        GameObject newOne = Instantiate(template, newPosition, currentRoadPoint.transform.rotation);
        var controller = newOne.GetComponent<FM_CarController>();
        if (controller != null)
        {
            controller.CurrentRoadPoint = currentRoadPoint;
            controller.TargetNextPoint();
        }
    }
}
