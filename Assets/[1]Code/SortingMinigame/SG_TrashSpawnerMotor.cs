﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SG_TrashSpawnerMotor : MonoBehaviour
{
    [SerializeField]
    float fireRate = 1f;
    float nextFireTime;

    public Action OnTrashSpawn;

    void Update()
    {
        if (CanFire())
            SpawnTrash();
    }

    public  void SpawnTrash()
    {
        nextFireTime = Time.time + fireRate;
        OnTrashSpawn();
    }

    protected  bool CanFire()
    {
        return Time.time >= nextFireTime;

    }
}


