﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;

public class SG_GameEndPanel : MonoBehaviour
{

    [SerializeField]
    TrashResultPanel sortingTrashPanel;

    [SerializeField]
    TrashResultPanel lostedTrashPanel;

    [SerializeField]
    Button sortingButton;

    [SerializeField]
    Button factoryButton;

    [SerializeField] GameObject btnAd;

    SG_TrashCounter trashCounter;

    public Action OnSortingBtnClicked;
    public Action OnFactoryBtnClicked;

    private void Start()
    {
        this.Inject();
        sortingButton.onClick.AddListener(SortingBtnClicked);
        factoryButton.onClick.AddListener(FactoryBtnClicked);

        trashCounter = FindObjectOfType<SG_TrashCounter>();
    }

    void FactoryBtnClicked()
    {
        OnFactoryBtnClicked();
    }



    void SortingBtnClicked()
    {
        OnSortingBtnClicked();
    }


    public void Show(Dictionary<int,int> _sortingTrashDic, Dictionary<int, int> _lostTrashDic)
    {
        gameObject.SetActive(true);

        sortingTrashPanel.FillPanel(_sortingTrashDic);
        lostedTrashPanel.FillPanel(_lostTrashDic);

        if(_lostTrashDic.Count >= 3)
            btnAd.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
