﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;


public class SG_TrashObject : MonoBehaviour,IMove
{
    [SerializeField]
    SpriteRenderer spriteRenderer;

    public int TrashId;


   public TrashMaterials trashMaterial;

    float deltaX;
    float deltaY;

    Vector3 mousePos;
    Vector3 initPos;

    [SerializeField]
    bool canMove = true;

    private void Start()
    {
       

    }

    private void OnMouseDown()
    {
        if (!enabled) return;

        initPos = transform.position;
        deltaX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;
         deltaY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y - transform.position.y;
        canMove = false;

    }

    private void OnMouseDrag()
    {

        if (!enabled) return;
        
        if(PlayerPrefs.GetInt("factoryTutorialPart2Ready") == 0)
            PlayerPrefs.SetInt("ItemDragBeforeTut", 1); 

        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector2(mousePos.x - deltaX, mousePos.y - deltaY);
    }

    private void OnMouseUp()
    {
        if (!enabled) return;

        transform.DOMove(initPos, 0.5f).OnComplete(() =>
        {
            canMove = true;
        });
       
    }



    public void SetTrashImageAndMaterial(int _trashId,Sprite _trashImg, TrashMaterials _trashMaterials)
    {
        TrashId = _trashId;
        trashMaterial = _trashMaterials;
        spriteRenderer.sprite = _trashImg;

    }

    public void Move(Vector3 moveDir)
    {
        if (!enabled) return;

        transform.position += moveDir;
    }

    public bool CanMove()
    {
        return canMove;
    }
}
