﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;

public class SG_TrashCounter : MonoBehaviour
{
    [Inject] SG_MinigameSystem trashMinigameSys;

    public int TotalTrashes;

    public int LostTrash;
    public int SortingTrash;


     List<int> sortingTrashIdList = new List<int>();
     List<int> lostTrashIdList = new List<int>();

    private void Start()
    {
        this.Inject();
    }

    public Dictionary<int,int> GetSortingTrashDic()
    {
        return Helper.GetNumberOfIdsInArrayAsDictionary(sortingTrashIdList.ToArray());
    }

    public Dictionary<int, int> GetLostTrashDic()
    {
        return Helper.GetNumberOfIdsInArrayAsDictionary(lostTrashIdList.ToArray());
    }

    public void TrashSorting( int _trashId)
    {      
        SortingTrash++;

        sortingTrashIdList.Add(_trashId);
        DecTotalTrash();
    }

    public void TrashLost( int _trashId)
    {
       
        LostTrash++;

        lostTrashIdList.Add(_trashId);
        DecTotalTrash();
    }

    public void TrashReplace()
    {
        sortingTrashIdList.AddRange(lostTrashIdList.ToArray());
        lostTrashIdList.Clear();
        LostTrash = 0;
        trashMinigameSys.GameEnd(this);
    }

    void DecTotalTrash()
    {
        TotalTrashes--;
        if (TotalTrashes <= 0)
        {
            trashMinigameSys.GameEnd(this);
        }
    }
}
