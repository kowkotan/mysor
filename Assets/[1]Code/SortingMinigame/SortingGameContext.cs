﻿using Adic;
using Framework;
using Framework.Common;
using Framework.Controllers.Loader;
using Framework.Managers.States;
using Framework.Views;
using UnityEngine;

public class SortingGameContext : BaseMainContext
{

    [SerializeField] SG_GameEndPanel GameEndPanel;
    [SerializeField] LoaderView loaderView;

    [SerializeField] TrashLocalDatabase trashDatabase;
    [SerializeField] FactoryLocalDatabase factoryDatabase;


    public override void Init()
    {
        base.Init();
    }

    protected override void BindCommands()
    {
      ///  throw new System.NotImplementedException();
    }

    protected override void BindComponents()
    {
    //    throw new System.NotImplementedException();
    }

    protected override void BindConfigs()
    {
      ///  throw new System.NotImplementedException();
    }

    protected override void BindControllers()
    {
        //this.containers[0].Bind<IRobotInput>().To<KeyboardController>()
            //.Bind<RobotController>().ToSelf();
    }

    protected override void BindManagers()
    {
        this.containers[0]
        .Bind<ITrashDatabase>().To(trashDatabase)
                       .Bind<IFactoryLocalDatabase>().To(factoryDatabase)
        .Bind<ISceneLoader>().ToGameObject<ControllerLoader>()
        .Bind<StateManager>().ToGameObject()
        .Bind<TrashWarehouse>().ToSingleton()
        .Bind<LevelLoader>().ToGameObject()
        .Bind<SG_MinigameSystem>().ToGameObject()
        .Bind<SG_TrashCounter>().ToGameObject();
    }

    protected override void BindModels()
    {

        //this.containers[0].Bind<RobotModel>().ToSelf();
    }

    protected override void BindView()
    {
        this.containers[0]
        .Bind<LoaderView>().To(loaderView)
        .Bind<SG_GameEndPanel>().To(GameEndPanel);
    }
}
