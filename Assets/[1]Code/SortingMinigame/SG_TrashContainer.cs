﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using DG.Tweening;
using UnityEngine;

public class SG_TrashContainer : MonoBehaviour
{
    public TrashMaterials trashMaterials;
    [Inject] SG_TrashCounter trashCounter;
    [Inject] SortTrash sortTrashQuest;
    [SerializeField] AudioSource audioSource;

    [SerializeField] AudioClip wrong;
    [SerializeField] AudioClip success;

    public  Action OnPickuped = delegate {};

    SG_TrashObject trashObject;

    private void Start()
    {
        this.Inject();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        SG_TrashObject _trashObject = collision.GetComponent<SG_TrashObject>();
        if (_trashObject != null && _trashObject.enabled == true)
        {
            trashObject = _trashObject;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        trashObject = null;
    }

    void TryPickUp()
    {
        if (trashObject == null) return;

        if (trashObject.trashMaterial == trashMaterials)
        {
            PickUp(trashObject);
            audioSource.clip = success;
            audioSource.Play();
            if (PlayerPrefs.GetString("SortingQuestTrashType") == "FOOD" && trashObject.trashMaterial == TrashMaterials.FOOD)
                sortTrashQuest.Increment();
            if (PlayerPrefs.GetString("SortingQuestTrashType") == "GLASS" && trashObject.trashMaterial == TrashMaterials.GLASS)
                sortTrashQuest.Increment();
            if (PlayerPrefs.GetString("SortingQuestTrashType") == "PAPER" && trashObject.trashMaterial == TrashMaterials.PAPER)
                sortTrashQuest.Increment();
            if (PlayerPrefs.GetString("SortingQuestTrashType") == "PLASTIC" && trashObject.trashMaterial == TrashMaterials.PLASTIC)
                sortTrashQuest.Increment();

        }
        else
        {
            WrongPickUp(trashObject);
            audioSource.clip = wrong;
            audioSource.Play();
        }
        trashObject = null;
    }

    private void Update()
    {
        if(Input.GetMouseButtonUp(0))
        {
            TryPickUp();
        }
    }

    void WrongPickUp(SG_TrashObject _trashObject)
    {
        _trashObject.enabled = false;

        _trashObject.transform.position = transform.position + new Vector3(0, 1.5f, 0);
        _trashObject.transform.DOShakePosition(0.5f).SetEase(Ease.InOutExpo).OnComplete(() =>
        {
            _trashObject.gameObject.SetActive(false);
            trashCounter.TrashLost(_trashObject.TrashId);
        });
    }

    void PickUp(SG_TrashObject _trashObject)
    {


        _trashObject.enabled = false;

        _trashObject.transform.position = transform.position + new Vector3(0, 1.5f, 0);
        _trashObject.transform.DOMove(transform.position, 0.5f).SetEase(Ease.InOutExpo).OnComplete(()=>
        {
            PickUpEnd(_trashObject);
        });
    }

    void PickUpEnd(SG_TrashObject trashObject)
    {
        trashObject.gameObject.SetActive(false);
        trashCounter.TrashSorting(trashObject.TrashId);
        OnPickuped();
    }

}
