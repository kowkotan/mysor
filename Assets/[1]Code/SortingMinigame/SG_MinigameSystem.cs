﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework.Managers.States;
using UnityEngine;

public class SG_MinigameSystem : MonoBehaviour
{
    [Inject] StateManager stateManager;
    [Inject] SG_GameEndPanel GameEndPanel;
    [Inject] TrashWarehouse trashWarehouse;

    public Action OnMiniGameEnd = delegate {};

    private void Start()
    {
        this.Inject();

        GameEndPanel.OnSortingBtnClicked += GameEndPanel_OnSortingBtnClicked;
        GameEndPanel.OnFactoryBtnClicked += GameEndPanel_OnFactoryBtnClicked;

    }

    void GameEndPanel_OnFactoryBtnClicked()
    {
        FactoryMap();
    }


    void GameEndPanel_OnSortingBtnClicked()
    {
        RobotGame();
    }


    public void GameEnd(SG_TrashCounter trashCounter)
    {
        GameEndPanel.Show(trashCounter.GetSortingTrashDic(), trashCounter.GetLostTrashDic());
        trashWarehouse.AddTrashes(trashCounter.GetSortingTrashDic());
        OnMiniGameEnd();
    }

    public void RobotGame()
    {
        stateManager.ChangeScene("RobotGame");
    }

    public void FactoryMap()
    {
        stateManager.ChangeScene("FactoryMap");
    }

    private void OnDestroy()
    {
        GameEndPanel.OnSortingBtnClicked -= GameEndPanel_OnSortingBtnClicked;
        GameEndPanel.OnFactoryBtnClicked -= GameEndPanel_OnFactoryBtnClicked;
    }
}
