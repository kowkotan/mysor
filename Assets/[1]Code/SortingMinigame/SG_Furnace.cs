﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;

public class SG_Furnace : MonoBehaviour
{
    [SerializeField] GameObject passvieState;
    [SerializeField] GameObject activeState;

    [Inject] SG_TrashCounter trashCounter;

    private void Start()
    {
        this.Inject();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        SG_TrashObject trashObject = collision.GetComponent<SG_TrashObject>();
        if (trashObject  != null)
        {
           collision.gameObject.SetActive(false);
            ShowBurnAnim();
            trashCounter.TrashLost(trashObject.TrashId);
            this.GetComponent<AudioSource>().Play();
        }
    }

    void ShowBurnAnim()
    {
        activeState.SetActive(true);

        Invoke("HideButnAnim",0.5f);
    }

    void HideButnAnim()
    {
       
        activeState.SetActive(false);
    }
}
