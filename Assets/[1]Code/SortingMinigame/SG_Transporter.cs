﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SG_Transporter : MonoBehaviour
{
    List<IMove> movedObjects = new List<IMove>();

    [SerializeField] float speed;

    public bool isStopped;

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.GetComponent<IMove>() != null)
        {
            movedObjects.Add(collision.GetComponent<IMove>());
        }
    }

    public void Stop()
    {
        isStopped = true;
    }

    public void Resume()
    {
        isStopped = false;
    }

    private void Update()
    {
        if (movedObjects.Count == 0 || isStopped == true) return;


        for (int i = 0; i < movedObjects.Count; i++)
        {
            if (movedObjects[i].CanMove())
            {
                movedObjects[i].Move(new Vector3(1, 0, 0) * Time.deltaTime * speed);
            }
        }
    }

    public void SpeedUp()
    {
        if (movedObjects.Count == 0) return;

        for (int i = 0; i < movedObjects.Count; i++)
        {
            if (movedObjects[i].CanMove())
            {
                movedObjects[i].Move(new Vector3(1, 0, 0) * speed);
            }
        }
    }
}
