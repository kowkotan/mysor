﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;

public class SG_ContainerController : MonoBehaviour
{
    IRobotGameResult robotGameResult;

    [Inject] ITrashDatabase trashDatabase;

    [SerializeField] SG_TrashContainer[] trashContainers;

    void Start()
    {
        this.Inject();
        robotGameResult = FindObjectOfType<RobotGameResult>();
        //HideAllContainers();
        //ShowActualContainer();

    }

    public void HideAllContainers()
    {
        for(int i = 0; i  < trashContainers.Length; i++)
        {
            trashContainers[i].gameObject.SetActive(false);
        }
    }

    void ShowActualContainer()
    {
        foreach (var dic in robotGameResult.GetPickedTrashDictionary())
        {
            for (int i = 0; i < trashContainers.Length; i++)
            {
                if(trashDatabase.GetMaterial(dic.Key) == trashContainers[i].trashMaterials )
                {
                    trashContainers[i].gameObject.SetActive(true);
                }
            }
        }
    }
}
