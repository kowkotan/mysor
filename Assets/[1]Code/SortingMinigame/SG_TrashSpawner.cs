﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Adic;
using UnityEngine;

public class SG_TrashSpawner : MonoBehaviour
{
    [SerializeField] GameObject sortingTrashContainerPrefab;

    [SerializeField] GameObject spawnPoint;

    [Inject] ITrashDatabase trashDatabase;

     [Inject] SG_TrashCounter trashCounter;

    SG_TrashSpawnerMotor spawnerMotor;

    IRobotGameResult robotGameResult;

    Dictionary<int, int> trashItems;

    int trashSpawnCounter;

    private void Start()
    {
        this.Inject();
        robotGameResult = FindObjectOfType<RobotGameResult>();
        spawnerMotor = GetComponent<SG_TrashSpawnerMotor>();
        spawnerMotor.OnTrashSpawn += SpawnerMotor_OnTrashSpawn;
        trashItems = robotGameResult.GetPickedTrashDictionary();

        trashCounter.TotalTrashes = GetTotalTrash();
    }

    void SpawnerMotor_OnTrashSpawn()
    {
        CreateTrashObject();
    }

    int GetTotalTrash()
    {
        int totalTrash = 0;
        foreach ( var k in trashItems )
        {
            totalTrash += k.Value;
        }
        return totalTrash;
    }

    int CutTrashId()
    {

       

        int currentKey = trashItems.First().Key;
        int currentValue = trashItems.First().Value;

        if(currentValue <= 0)
        {
            trashItems.Remove(currentKey);
        }

        if (trashItems.Count == 0) return -1;

        if (trashItems.ContainsKey(trashItems.First().Key))
        {
            trashItems[trashItems.First().Key]--;
            return trashItems.First().Key;
        }
        else
        {
            return -1;
        }


    }

     void CreateTrashObject()
    {
        if (trashItems.Count == 0) return;

        int trashId = CutTrashId();

        if (trashId == -1) return;

        trashSpawnCounter++;

        Sprite trashImg = trashDatabase.GetSortingItem(trashId);
        TrashMaterials trashMaterial = trashDatabase.GetMaterial(trashId);

      GameObject newTrashObject =  Instantiate(sortingTrashContainerPrefab, spawnPoint.transform.position, Quaternion.identity);
        newTrashObject.name = "to" + trashSpawnCounter;
        newTrashObject.GetComponent<SG_TrashObject>().SetTrashImageAndMaterial(trashId, trashImg, trashMaterial);

    }

    private void OnDestroy()
    {
        spawnerMotor.OnTrashSpawn -= SpawnerMotor_OnTrashSpawn;
    }

}
