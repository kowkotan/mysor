﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LM_LocationButton : MonoBehaviour
{
    [SerializeField] LM_Location location;
    [SerializeField] LM_Location previousLocation;
    [SerializeField] bool firstButton;
    [SerializeField] bool lastButton;
    [SerializeField] GameObject previousButton;
    [SerializeField] GameObject nextButton;

    [SerializeField] GameObject openBtnState;
    [SerializeField] GameObject textBtn;
    [SerializeField] GameObject fillingBtn;
    [SerializeField] GameObject percentageText;
    [SerializeField] GameObject checkState;
    [SerializeField] GameObject blockBtnState;
    [SerializeField] GameObject closeBtnState;
    [SerializeField] GameObject timeTxt;
    [SerializeField] Button closeBtnWhenBLock;

    public int levelLevel;
    public int level;

    private void Start() {
        Test1();
    }

    void Test1()
    {
        //Debug.Log(PlayerPrefs.GetInt("Level1-3Percentage"));
        if(PlayerPrefs.GetInt("RobotLocation" + levelLevel + "-" + level + "Progress") == 100)
        {
            BlockState();
            //nextButton.GetComponent<LM_LocationButton>().ShowState();
            if(!location.locationData.reloadInProgress)
            {
                location.StartReload();
            }
        }
        else if(PlayerPrefs.GetInt("RobotLocation" + levelLevel + "-" + (level-1) + "Progress") == 100)
        {
            this.ShowState();
        }
        else if(PlayerPrefs.GetInt("RobotLocation" + levelLevel + "-" + (level-1) + "Progress") < 100 && previousLocation.locationData.isComplited)
        {
            this.ShowState();
        }
    }

    public void ReloadedFinished()
    {
        PlayerPrefs.SetInt("Level" + level + "Percentage", 0);
    }

    public void ShowState()
    {
        fillingBtn.SetActive(true);
        openBtnState.SetActive(true);
        textBtn.SetActive(true);
        closeBtnState.SetActive(false);
        timeTxt.SetActive(false);
        percentageText.SetActive(true);
        checkState.SetActive(false);
        blockBtnState.SetActive(false);
        closeBtnWhenBLock.enabled = true;
    }

    public void CloseState()
    {
        //fillingBtn.SetActive(false);
        openBtnState.SetActive(false);
        //textBtn.SetActive(false);
        //closeBtnState.SetActive(true);
        timeTxt.SetActive(true);
        percentageText.SetActive(false);
        checkState.SetActive(true);
        blockBtnState.SetActive(true);
    }

    public void BlockState()
    {
        fillingBtn.SetActive(true);
        openBtnState.SetActive(false);
        textBtn.SetActive(true);
        closeBtnState.SetActive(false);
        timeTxt.SetActive(true);
        percentageText.SetActive(false);
        checkState.SetActive(true);
        blockBtnState.SetActive(true);
        closeBtnWhenBLock.enabled = false;
    }

    public void fillTimeElapsedText(TimeSpan _elapsedTime)
    {
        timeTxt.GetComponent<Text>().text = _elapsedTime.ToCustomString();
    }  
}
