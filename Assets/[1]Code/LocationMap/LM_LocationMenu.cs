﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Adic;
using Framework.Managers.States;

public class LM_LocationMenu : MonoBehaviour
{
    LM_Location location;
    LocationData locationData;

    [Inject] StateManager stateManager;

    [SerializeField] Text titleText;
    [SerializeField] ProgressBarWithTextFillingValue[] progressBarWithTextFillingValue;
    [SerializeField] LM_LocationButton[] locationButton; 

    [SerializeField] string levelToLoad;

    public Action OnClosed = delegate {};
    public Action OnOpened = delegate { };

    private void Start() {
        this.Inject();
    }

    public void Show(LM_Location _location)
    {
        location = _location;
        locationData = _location.GetLocationData();

        gameObject.SetActive(true);

        Debug.Log("LOCATION MENU OPENED");

        for(int i = 0; i < 5; i++)
        {
            //Debug.Log(PlayerPrefs.GetInt("Level1-" + locationButton[i].level + "Percentage"));
            int LevelLoadPerc = PlayerPrefs.GetInt("RobotLocation" + locationButton[i].levelLevel + "-" + locationButton[i].level + "Progress");
            progressBarWithTextFillingValue[i].FillValue(LevelLoadPerc, 100);   
        }
    }

    public void LoadLevel(string locationLevel)
    {
        PlayerPrefs.SetString("PlayerLevelTest", levelToLoad);
        PlayerPrefs.SetString("PlayerLocationTest", locationLevel);
        stateManager.ChangeScene("RobotGame");
    }

    public void Hide()
    {
        OnClosed();
        gameObject.SetActive(false);
    }
}
