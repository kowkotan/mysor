﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Adic;

public class LM_LocationButtonOpener : MonoBehaviour
{
    [SerializeField] LM_Location[] locationData;

    [SerializeField] GameObject[] Buttons;

    [SerializeField] GameObject btnOpenState;
    [SerializeField] GameObject btnCloseState;

    private void Awake() {
        this.Inject();
        //CheckState();
    }

    void CheckState()
    {
        for(int i = 0; i < locationData.Length; i++)
        {
            if(locationData[i].locationData.reloadInProgress)
            {
                Buttons[i].GetComponent<LM_LocationButton>().CloseState();
            }
        }
    }
}
