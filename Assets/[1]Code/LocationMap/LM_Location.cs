﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using Framework.Interfaces;

public class LM_Location : MonoBehaviour, ITickSec
{
    [Inject] UpdateManager updateManager;
    [Inject] ISaveLoadSystem saveLoadSystem;
    [Inject] ILocationLocalDatabase locationLocalDatabase;

    LocationDBData locationDBData;

    public string Title
    {
        get
        {
            return locationDBData.title;
        }
    }

    public string LevelToLoad
    {
        get
        {
            return locationDBData.levelToUnlock;
        }
    }

    public LocationData locationData;
    [SerializeField] LM_LocationButton locationButton;

    public Action OnInitialized = delegate { };
    public Action ReloadInProgressAction = delegate {};
    public Action ReloadInFinished = delegate {};

    void Start() {
        
        this.Inject();

        Load();

        updateManager.AddTo(this);

        locationDBData = locationLocalDatabase.GetFullData(locationData.id);

        if(locationData.reloadInProgress)
        {
            locationData.reloadStartDate = DateMaster.GetDate(locationData.id + "StartReloadDate");

            if(ReloadIsFinished())
            {
                ReloadFinished();
            } 
            else
            {
                ReloadInProgressAction();
            }
        }

        OnInitialized();
    }

    public LocationData GetLocationData()
    {
        return locationData;
    }

    void Load()
    {
        if( saveLoadSystem.IsSaved(locationData.id))
        {
           locationData = saveLoadSystem.LoadStruct<LocationData>(locationData.id);
        }
       else
        {
            Save();
        }
    }

    void Save()
    {
        saveLoadSystem.SaveStruct(locationData.id, locationData);
    }

    public bool ReloadIsFinished()
    {
        return GetSecToEndOfReload() >= locationData.upgradeTimeSec;
    }

    int GetSecToEndOfReload()
    {
        return (int) DateMaster.GetDifference(locationData.reloadStartDate).TotalSeconds;
    }

    public void StartReload()
    {
        locationData.reloadInProgress = true;

        Debug.Log("ПЕРЕЗАГРУЗКА УРВОНЯ НАЧАЛАСЬ");

        locationData.upgradeTimeSec = 86400;
        locationButton.BlockState();
        locationData.isComplited = true;

        ReloadInProgressAction();

        //ShowUpgradeVisualState();

        string timeForStart = PlayerPrefs.GetString("RobotLocation" + locationButton.levelLevel + "-" + locationButton.level + "StartDateTime");
        DateTime reloadDate = Convert.ToDateTime(timeForStart);
        locationData.reloadStartDate = reloadDate;

        DateMaster.SaveDate(locationData.reloadStartDate,locationData.id+"StartReloadDate");

        Save();
    }

    void ReloadFinished()
    {
        Debug.Log("ПЕРЕЗАГРУЗКА УРОВНЯ ЗАВЕРШЕНА");
        locationData.reloadInProgress = false;
        locationButton.ShowState();
        locationButton.GetComponentInChildren<ProgressBarWithTextFillingValue>().FillValue(0, 100);
        PlayerPrefs.SetInt("RobotLocation" + locationButton.levelLevel + "-" + locationButton.level + "Progress", 0);
        //locationButton.ReloadedFinished();
        //buildState.SetActive(false);
        //factoryData.level++;


        //numberOfMatsRow = GetNumberOfMatsRowValue(factoryData.level);


        //UpdateFactoryVisual();
        Save();

        ReloadInFinished();
    }

    public void TickSec()
    {
        TimeSpan elapsedTime;
        if (locationData.reloadInProgress)
        {
            elapsedTime = GetElapsedTimeSpan();
            Debug.Log("ПЕРЕЗАГРУЗКА УРОВНЯ В ПРОЦЕССЕ:" + elapsedTime.ToString());

            //buildProcessPanelUI.fillPriceText(BalancedValues.GetFactoryBuildSpeedUpPrice((int)elapsedTime.TotalSeconds));
            //buildProcessPanelUI.fillTimeElapsedText(elapsedTime);
            locationButton.fillTimeElapsedText(elapsedTime);

            if (ReloadIsFinished()) ReloadFinished();
        }
    }

    TimeSpan GetElapsedTimeSpan()
    {
        return DateMaster.GetDifference(locationData.reloadStartDate.AddSeconds(locationData.upgradeTimeSec));
    }

    private void OnDestroy() {
        updateManager.RemoveFrom(this);
    }
}
