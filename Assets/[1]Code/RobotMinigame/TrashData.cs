﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "TrashData", menuName = "Create trash data")]
public class TrashData : ScriptableObject
{
    public TrashItems trashType;
    public string title;
    public Sprite icon;
    public Sprite sortingItem;
    public TrashMaterials materialType;

}
