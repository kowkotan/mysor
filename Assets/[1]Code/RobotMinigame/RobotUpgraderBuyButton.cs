﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using GameAnalyticsSDK;

public class RobotUpgraderBuyButton : MonoBehaviour
{
    [Inject] DetalsModel detalsModel;

    [SerializeField] string robotUpCharId;

    [SerializeField]
    int startPrice;

    public int Price => price;

    int price;

    [SerializeField]
    float growCoeff;

    [SerializeField]
    int currentLevel = 1;
    [SerializeField]
    int maxLevel = 10;

    public Action OnSuccesBuyed;

    private void Awake()
    {
        GameAnalytics.Initialize();
        currentLevel = PlayerPrefs.GetInt("robotUp" + robotUpCharId,1);
        price = BalancedValues.GetRobotUpradePrice(startPrice, currentLevel,growCoeff);
    }

    void Start()
    {
        this.Inject();
    }

    public float GetLvlRatio()
    {
        return (float)currentLevel / maxLevel;
    }

    public void Buy()
    {
        if (currentLevel >= maxLevel) return;

        bool transactionResult = detalsModel.DecValue(price);
        if (!transactionResult) return;

        currentLevel++;

        price = BalancedValues.GetRobotUpradePrice(startPrice, currentLevel,growCoeff);


        PlayerPrefs.SetInt("robotUp" + robotUpCharId, currentLevel);

        this.GetComponent<AudioSource>().Play();

        if(robotUpCharId == "fuelup")
        {
            Dictionary<String, System.Object> eventParameters = new Dictionary<String, System.Object>();
            eventParameters.Add("Улучшение навыков", "Прокачал навык Топливный бак до " + currentLevel);
            AppMetrica.Instance.ReportEvent("Прокачка робота", eventParameters);
            GameAnalytics.NewDesignEvent("RobotUpgrade:"+"Prokachal navik Toplivniu bak do " + currentLevel);
        }
        else if(robotUpCharId == "pickupup")
        {
            Dictionary<String, System.Object> eventParameters = new Dictionary<String, System.Object>();
            eventParameters.Add("Улучшение навыков", "Прокачал навык Радиус сбора до " + currentLevel);
            AppMetrica.Instance.ReportEvent("Прокачка робота", eventParameters);
            GameAnalytics.NewDesignEvent("RobotUpgrade:"+"Prokachal navik Radius sbora do " + currentLevel);
        }
        else if(robotUpCharId == "trashback")
        {
            Dictionary<String, System.Object> eventParameters = new Dictionary<String, System.Object>();
            eventParameters.Add("Улучшение навыков", "Прокачал навык Вместительность бака до " + currentLevel);
            AppMetrica.Instance.ReportEvent("Прокачка робота", eventParameters);
            GameAnalytics.NewDesignEvent("RobotUpgrade:"+"Prokachal navik Vmestitelnost baka do " + currentLevel);
        }

        AppMetrica.Instance.SendEventsBuffer();

        OnSuccesBuyed();
    }
}
