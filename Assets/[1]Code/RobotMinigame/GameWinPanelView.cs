﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameWinPanelView : MonoBehaviour
{
    [SerializeField]
    TrashResultPanel trashResultPanel;

    public void Show()
    {
        gameObject.SetActive(true);

 
    }

    public void ShowResult(Dictionary<int, int> resultData)
    {
        trashResultPanel.FillPanel(resultData);
    }
}
