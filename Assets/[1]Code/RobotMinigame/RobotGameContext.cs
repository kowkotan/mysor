﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework;
using Framework.Common;
using Framework.Controllers.Loader;
using Framework.Managers.States;
using Framework.Views;
using UnityEngine;

public class RobotGameContext : BaseMainContext
{
    [SerializeField]
    RobotMinigameSystem robotMinigameSystem;

    [SerializeField] LoaderView loaderView;

    [SerializeField] TrashLocalDatabase trashDatabase;
    [SerializeField] FactoryLocalDatabase factoryDatabase;


    public override void Init()
    {
        base.Init();
    }

    protected override void BindCommands()
    {
      ///  throw new System.NotImplementedException();
    }

    protected override void BindComponents()
    {
    //    throw new System.NotImplementedException();
    }

    protected override void BindConfigs()
    {
      ///  throw new System.NotImplementedException();
    }

    protected override void BindControllers()
    {
        //this.containers[0].Bind<IRobotInput>().To<KeyboardController>()
            //.Bind<RobotController>().ToSelf();
    }

    protected override void BindManagers()
    {
        this.containers[0]
        .Bind<ISceneLoader>().ToGameObject<ControllerLoader>()
         .Bind<TrashSaveLoadSystem>().ToSingleton()
        .Bind<StateManager>().ToGameObject()
        .Bind<UpdateManager>().ToGameObject()
        .Bind<ISaveLoadSystem>().To<SaveLoadSystem>()
        .Bind<LevelLoader>().ToGameObject()
         .Bind<ITrashDatabase>().To(trashDatabase)
                 .Bind<IFactoryLocalDatabase>().To(factoryDatabase)
        .Bind<RobotMinigameSystem>().To(robotMinigameSystem);
    }

    protected override void BindModels()
    {
        this.containers[0].Bind<DetalsModel>().ToSingleton()
        .Bind<EnergyModel>().ToSingleton()
         .Bind<GameProgressModel>().ToSingleton()
            .Bind<SoftValueModel>().ToSingleton()
                .Bind<HardValueModel>().ToSingleton();
    }

    protected override void BindView()
    {
        this.containers[0].Bind<LoaderView>().To(loaderView);
    }
}
