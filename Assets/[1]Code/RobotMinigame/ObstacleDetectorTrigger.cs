﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ObstacleDetectorTrigger : MonoBehaviour
{


    RobotMotor robotMotor;



    public void Init(RobotMotor _robotMotor)
    {
        robotMotor = _robotMotor;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "obstacle")
        {
            robotMotor.Position(false);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "obstacle")
        {
            robotMotor.Position(true);
        }
    }
}
