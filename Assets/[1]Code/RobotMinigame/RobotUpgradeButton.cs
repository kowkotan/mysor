﻿using System;
using System.Collections;
using System.Collections.Generic;
using Framework.Views;
using UnityEngine;
using UnityEngine.UI;

public class RobotUpgradeButton : MonoBehaviour
{
    [SerializeField]
    Button buyButton;

    [SerializeField]
    FillingImageView fillingImageView;

    [SerializeField]
    Text priceTxt;

    RobotUpgraderBuyButton robotUpgrader;

    private void Start()
    {
        robotUpgrader = GetComponent<RobotUpgraderBuyButton>();
        robotUpgrader.OnSuccesBuyed += RobotUpgrader_OnSuccesBuyed;

        buyButton.onClick.AddListener(BuyButtonClickHandler);

        UpdateCoinsIndicator();
        UpdateLevelBar();
    }

    void BuyButtonClickHandler()
    {
        robotUpgrader.Buy();
    }

    void UpdateLevelBar()
    {
        fillingImageView.SetNormalizedValue(robotUpgrader.GetLvlRatio());
    }

    void UpdateCoinsIndicator()
    {
        priceTxt.text = robotUpgrader.Price.ToString();
    }

    void RobotUpgrader_OnSuccesBuyed()
    {
        UpdateLevelBar();
        UpdateCoinsIndicator();
    }

    private void OnDestroy()
    {
        buyButton.onClick.RemoveListener(BuyButtonClickHandler);
        robotUpgrader.OnSuccesBuyed -= RobotUpgrader_OnSuccesBuyed;
    }
}
