﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework.Interfaces;
using Framework.Managers.Ads;
using UnityEngine;
using UnityEngine.UI;

public class CountdownEffectBroadcaster : CountdownEffect
{
    [SerializeField] CountdownEffect[] countdownEffects;

    public override void ApplyEffect()
    {
        for (int i = 0; i < countdownEffects.Length; i++)
        {
            countdownEffects[i].StartEffect(countdownTime);
        }
    }

    public override void ResetEffect()
    {
        for (int i = 0; i < countdownEffects.Length; i++)
        {
            countdownEffects[i].ResetEffect();
        }
    }
}

