﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CountdownEffect : MonoBehaviour
{
   protected bool countdownIsStarted;
   public float countdownTime;
    public CountdownEffectUIIndicatorBase countdownEffectUIIndicator;

    protected float effectTime = 0;
    public void StartEffect(float _time)
    {
        effectTime = _time;
        if (_time > countdownTime)
        countdownTime = _time;

        if (!countdownIsStarted)
        {
            ApplyEffect();
            countdownIsStarted = true;

            countdownEffectUIIndicator?.StopTrack();

            countdownEffectUIIndicator?.StartTrack(this);
        }
    }

    public abstract void ApplyEffect();
    public abstract void ResetEffect();


    public void CountDownEnd()
    {
        countdownIsStarted = false;
        ResetEffect();
        countdownEffectUIIndicator?.StopTrack();
    }


    private void Update()
    {
        if(countdownIsStarted )
        {
            countdownTime -= Time.deltaTime;
            if(countdownTime <= 0)
            {
                CountDownEnd();
            }
        }
    }
}
