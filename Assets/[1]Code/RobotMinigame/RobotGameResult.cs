﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotGameResult : MonoBehaviour, IRobotGameResult
{
    Dictionary<int, int> trashDatabase;

    public int boxFounded = 2;

    public void Awake()
    {
        DontDestroyOnLoad(this);
    }

    int[] GetPickedTrashIds(Trash[] _trashList)
    {
        List<int> pickedItemsIdList = new List<int>();
        for (int i = 0; i < _trashList.Length; i++)
        {
            if (_trashList[i].isPickuped == true)
            {
                pickedItemsIdList.Add((int)_trashList[i].trashItems);
            }
        }
        return pickedItemsIdList.ToArray();
    }

    public void CountIdsAndCountsOfTrashItems(Trash[] _trashList)
    {
        trashDatabase = Helper.GetNumberOfIdsInArrayAsDictionary(GetPickedTrashIds(_trashList));
    }

    public Dictionary<int, int> GetPickedTrashDictionary()
    {
        return trashDatabase;
    }
}
