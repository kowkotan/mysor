﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework.Managers.Event;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(CircleCollider2D))]
public class RobotPickupTrigger : MonoBehaviour
{

    CircleCollider2D circleCollider;
    RobotData robotData;

    public bool noclip;

    [Inject] RobotMinigameSystem robotMinigameSystem;

    [SerializeField]
    List<Transform> pickupsItems = new List<Transform>();

    public Action<Transform> PickupAction;

    private void Awake()
    {
        circleCollider = GetComponent<CircleCollider2D>();
    }

    private void Start()
    {
        this.Inject();
    }

    public void Init(float _radius, RobotData _robotData)
    {
        SetPickupRadius(_radius);
        robotData = _robotData;
    }

   public void SetPickupRadius(float _radius)
    {
        circleCollider.radius = _radius;
    }

    private void OnTriggerEnter2D(Collider2D _collision)
    {
        IPickup pickupableItem = _collision.GetComponent<IPickup>();

        if (pickupableItem == null) return;

        pickupsItems.Add(_collision.transform);

    }

    private void Update()
    {
        if (robotMinigameSystem.gameoverflag) return;

        if(pickupsItems.Count > 0)
        {
            for(int i = 0; i < pickupsItems.Count; i++)
            {

                RaycastHit2D raycastHit = Physics2D.Raycast(transform.position, pickupsItems[i].transform.position - transform.position, Mathf.Infinity, 1 << 8);

                if (raycastHit.collider != null && raycastHit.collider.tag != "obstacle" || noclip == true)
                {

                    if (pickupsItems[i].GetComponent<Trash>() != null )
                        robotData.currentTrashTank++;

               
                    if (robotData.currentTrashTank <= robotData.maxTrashTank)
                    {
                    
                        PickupAction(pickupsItems[i]);
                        pickupsItems[i].GetComponent<IPickup>().Pickup();

                  
                    }

                    if (robotData.currentTrashTank >= robotData.maxTrashTank)
                    {
                        robotMinigameSystem.GameLoose(LOOSE_REASON.NO_CAPACITY);
                    }


                }
            }
        }

        SetPickupRadius(robotData.pickupZoneRadius);
    }

    private void OnTriggerExit2D(Collider2D _collision)
    {
        IPickup pickupableItem = _collision.GetComponent<IPickup>();

        if (pickupableItem == null) return;

        pickupsItems.Remove(_collision.transform);

    }
}
