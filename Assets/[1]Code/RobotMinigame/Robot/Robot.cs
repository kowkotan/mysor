﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework.Common.Camera;
using UnityEngine;

public class Robot : MonoBehaviour, ISpeedUpEffect,IMagnetEffect
{
    IRobotInput robotInput;
    RobotMotor robotMotor;
    RobotPickupTrigger robotPickupTrigger;

    [SerializeField]
    Transform pickupRadiusTransform;

    [Inject] RobotMinigameSystem robotMinigameSystem;

    [Inject] ISaveLoadSystem saveLoadSystem;

    [SerializeField] RobotData robotData;

    [SerializeField] ObstacleDetectorTrigger obstacleDetectorTrigger;

    [SerializeField] Sprite StandartRadius = null;
    [SerializeField] Sprite MagnetRadius = null;

    PrizeInGameController prizeInGameController;

    [SerializeField]
    Animator animator;

    [SerializeField] AudioClip mopving;
    [SerializeField] AudioClip idle;

    float PickZoneRadius
    {
        get
        {
            return robotData.pickupZoneRadius;
        }
        set
        {
            robotData.pickupZoneRadius = value;
            pickupRadiusTransform.localScale = new Vector3(1, 1, 1) * robotData.pickupZoneRadius;
            robotPickupTrigger.SetPickupRadius(robotData.pickupZoneRadius);
        }


    }

    void Start()
    {
        this.Inject();

      
        Load();

        robotData.currentFuelTank = robotData.maxFuelTank;
        prizeInGameController = FindObjectOfType<PrizeInGameController>();

        robotPickupTrigger = GetComponent<RobotPickupTrigger>();
        robotPickupTrigger.Init(robotData.pickupZoneRadius,robotData);

        robotPickupTrigger.PickupAction += RobotPickupTrigger_PickupAction;;

        animator.SetInteger("idleAnimId", Random.Range(0,1));

        FindObjectOfType<Camera2DFollowTDS>().SetTarget(transform);

        robotInput = new JoysticInput(FindObjectOfType<FloatingJoystick>());
        robotMotor = new RobotMotor(robotInput, transform, robotMinigameSystem, robotData);

        obstacleDetectorTrigger.Init(robotMotor);

    }

    void Save()
    {
        saveLoadSystem.SaveClass(robotData.robotId,robotData);
    }

    void Load()
    {
        if (saveLoadSystem.IsSaved(robotData.robotId))
        {
            robotData = saveLoadSystem.LoadClass<RobotData>(robotData.robotId);

            Debug.Log("Загруженный класс " + robotData.maxTrashTank);
        }
    }


    void RobotPickupTrigger_PickupAction(Transform obj)
    {
        Debug.Log("PICK UP");
        animator.SetBool("isPickup", true);
        this.GetComponent<AudioSource>().Play();
        Invoke("PickupOff", 0.5f);

        if(obj.GetComponent<PrizePickup>() != null)
        {
            prizeInGameController.PickupPrize();
        }
    }

    public void PickRadiusUpgrade()
    {
        PickZoneRadius += BalancedValues.RobotPickupRadiusUpgGrow;
        Save();
    }

    public void TrashTankUpgrade()
    {
        robotData.maxTrashTank += BalancedValues.RobotTrashTankUpgGrow;
        Save();
    }

    public void FuelTankUpgrade()
    {
        robotData.maxFuelTank += BalancedValues.RobotFuelTankUpgGrow;
        robotData.currentFuelTank = robotData.maxFuelTank;
        Save();
    }

    public void StartSpeedUpEffect()
    {
        robotData.moveSpeed += 2;
    }

    public void StopSpeedUpEffect()
    {
        robotData.moveSpeed -= 2;
    }


    public void PickupOff()
    {
        animator.SetBool("isPickup", false);
    }

    public RobotData GetRobotData()
    {
        return robotData;
    }

    void Update()
    {
        robotInput.Tick();
    }

    private void FixedUpdate()
    {
        robotMotor.TickFixed();
    }
    private void OnDestroy()
    {
        robotPickupTrigger.PickupAction -= RobotPickupTrigger_PickupAction;
    }

    public void StartMagnetEffect()
    {
        robotPickupTrigger.noclip = true;
        pickupRadiusTransform.gameObject.GetComponent<SpriteRenderer>().sprite = MagnetRadius;
        PickZoneRadius *= 1.5f;
    }

    public void StopMagnetEffect()
    {
        robotPickupTrigger.noclip = false;
        pickupRadiusTransform.gameObject.GetComponent<SpriteRenderer>().sprite = StandartRadius;
        PickZoneRadius /= 1.5f;
    }
}
