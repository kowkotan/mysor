﻿using System;
using Adic;
using Framework.Interfaces;
using UnityEngine;

public class RobotMotor:ITickFixed
{

    RobotMinigameSystem robotMinigameSystem;

    Transform robotTransform;
    IRobotInput robotInput;
    RobotData robotData;

    bool RotationIsOn = true;
    bool PositionIsOn = true;

    bool isMoving;


    public RobotMotor(IRobotInput inputBase,Transform _transformToMove, RobotMinigameSystem _robotMinigameSystem, RobotData _robotData)
    {
        robotInput = inputBase;
        robotTransform = _transformToMove;
        robotData = _robotData;
        robotMinigameSystem = _robotMinigameSystem;

        robotData.currentFuelTank = robotData.maxFuelTank;

        robotInput.SetMoveDirection( _transformToMove.position);
    }

    public void Rotation(bool _isAvaible)
    {
        RotationIsOn = _isAvaible;
    }

    public void Position(bool _isAvaible)
    {
        PositionIsOn = _isAvaible;
    }

    public bool FuelTankIsEmpty()
    {
        return (robotData.currentFuelTank <= 0);
    }

    public void TickFixed()
    {
        if (FuelTankIsEmpty())
        {
            robotMinigameSystem.GameLoose(LOOSE_REASON.NO_FUEL);
            return;
        }

        Vector3 moveVector = Vector3.zero;
        Vector3 nextMoveVector = Vector3.zero;
        float quotient = 0;

        moveVector = robotInput.GetMoveDirection();// - robotTransform.position).normalized;


        if (PositionIsOn)
        {
            // quotient = Mathf.Sqrt(moveVector.x * moveVector.x * moveVector.y * moveVector.y) / robotData.moveSpeed;
            nextMoveVector = new Vector3(Math.Min(moveVector.x, robotData.moveSpeed), Math.Min(moveVector.y, robotData.moveSpeed), 0) * Time.deltaTime * robotData.moveSpeed;
            //  nextMoveVector /= quotient;
            robotTransform.position += nextMoveVector;

            if (moveVector != Vector3.zero)
            {
                if(moveVector.magnitude > 0)
                robotData.currentFuelTank -= Time.deltaTime;
            }
        }

        if (RotationIsOn && moveVector != Vector3.zero)
        {
            float angle = Mathf.Atan2(moveVector.y, moveVector.x) * Mathf.Rad2Deg;
            if (angle < 0f)
            {
                angle += 360f;
            }
            robotTransform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        }
    }

   
}

