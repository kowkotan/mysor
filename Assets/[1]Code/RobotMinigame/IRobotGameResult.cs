﻿using System.Collections.Generic;

public interface IRobotGameResult
{
    void CountIdsAndCountsOfTrashItems(Trash[] _trashList);
    Dictionary<int, int> GetPickedTrashDictionary();
}