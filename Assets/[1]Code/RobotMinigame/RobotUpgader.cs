﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;

public class RobotUpgader : MonoBehaviour
{
    [SerializeField] RobotUpgraderBuyButton fuelTankUpgade;
    [SerializeField] RobotUpgraderBuyButton trashTankUpgade;
    [SerializeField] RobotUpgraderBuyButton pickradiusUpgade;

    [Inject] RobotMinigameSystem robotMinigameSystem;

    Robot currentRobot;

    void Start()
    {
        fuelTankUpgade.OnSuccesBuyed += FuelTankUpgade_OnSuccesBuyed;
        trashTankUpgade.OnSuccesBuyed += TrashTankUpgade_OnSuccesBuyed;
        pickradiusUpgade.OnSuccesBuyed += PickradiusUpgade_OnSuccesBuyed;

        this.Inject();

        robotMinigameSystem.OnMinigameLoaded += RobotMinigameSystem_OnMinigameLoaded;

       
    }

    void RobotMinigameSystem_OnMinigameLoaded()
    {
        currentRobot = FindObjectOfType<Robot>();
    }


    void PickradiusUpgade_OnSuccesBuyed()
    {
        currentRobot.PickRadiusUpgrade();
    }


    void TrashTankUpgade_OnSuccesBuyed()
    {
        currentRobot.TrashTankUpgrade();
    }


    void FuelTankUpgade_OnSuccesBuyed()
    {
        currentRobot.FuelTankUpgrade();
    }

    private void OnDestroy()
    {
        fuelTankUpgade.OnSuccesBuyed -= FuelTankUpgade_OnSuccesBuyed;
        trashTankUpgade.OnSuccesBuyed -= TrashTankUpgade_OnSuccesBuyed;
        pickradiusUpgade.OnSuccesBuyed -= PickradiusUpgade_OnSuccesBuyed;
        robotMinigameSystem.OnMinigameLoaded -= RobotMinigameSystem_OnMinigameLoaded;
    }
}
