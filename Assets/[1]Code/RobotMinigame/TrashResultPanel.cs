﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using Framework.Utility;

public class TrashResultPanel : MonoBehaviour
{
    [Inject] ITrashDatabase trashDatabase;

    [SerializeField]
    GameObject trashResultItemPrefab;

    [SerializeField] Transform trashContainer;


    Dictionary<int, int> results;

    private void Start()
    {
        this.Inject();

        Debug.Log("START");

      

    }

    public void FillPanel(Dictionary<int, int> _results)
    {
        Debug.Log("FILL");

        foreach (Transform child in trashContainer)
        {
            Destroy(child.gameObject);
        }

        results = _results;
        Invoke("ShowResults", 0.2f);
        //        System.Diagnostics.Trace.WriteLine(kv.Key + " (" + kv.Value + ")");
    }
    

    void ShowResults()
    {
        foreach (var kv in results)
        {
            Debug.Log(trashDatabase);
            Sprite trashIcon = trashDatabase.GetTrashIcon(kv.Key);
            int trashCount = kv.Value;
            TrashResultCell trashResultCell = Instantiate(trashResultItemPrefab, trashContainer).GetComponent<TrashResultCell>();
            trashResultCell.FillCell(trashIcon, trashCount);
        }
    }
}
