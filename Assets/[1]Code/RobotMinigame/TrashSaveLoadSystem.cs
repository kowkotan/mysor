﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TrashSaveLoadSystem
{
    List<string> deactiveTrashesId = new List<string>();

    public List<string> DeactiveTrashesId => deactiveTrashesId;

    public void SaveAllPickedTrashes(int _factoryZoneNumber, int _minigameLevel,Trash[] pickedTrashes)
    {
        for(int i = 0; i < pickedTrashes.Length; i++)
        {
            if(pickedTrashes[i].isPickuped == true && deactiveTrashesId.Contains(pickedTrashes[i].GetName()) == false)
            {
                deactiveTrashesId.Add(pickedTrashes[i].GetName());
            }
        }
        PlayerPrefs.SetString( string.Format( "sapt {0}{1}",_factoryZoneNumber,_minigameLevel), JsonUtilityHelper.ToJson(deactiveTrashesId.ToArray(), true));
    }

    public void LoadAllPickedTrashes(int _factoryZoneNumber, int _minigameLevel)
    {
        if (!PlayerPrefs.HasKey(string.Format("sapt {0}{1}", _factoryZoneNumber, _minigameLevel)))
        {
            deactiveTrashesId = new List<string>();
            return;
        }

        deactiveTrashesId = new List<string>(JsonUtilityHelper.FromJson<string>(PlayerPrefs.GetString(string.Format("sapt {0}{1}", _factoryZoneNumber, _minigameLevel))));

        Debug.Log(deactiveTrashesId.Count + " dasdasdsadasdas ");
    }

    public void ReloadAllTrashesh(int _factoryZoneNumber, int _minigameLevel, Trash[] pickedTrashes)
    {
        PlayerPrefs.DeleteKey(string.Format("sapt {0}{1}", _factoryZoneNumber, _minigameLevel));
    }
}
