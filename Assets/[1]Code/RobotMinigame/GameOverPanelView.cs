﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverPanelView : MonoBehaviour
{

    [SerializeField]
    GameObject noFuelReasonState;
    [SerializeField]
    GameObject noTrashTankReasonState;

    [SerializeField]
    TrashResultPanel trashResultPanel;

    public void Show(LOOSE_REASON looseReason)
    {
        gameObject.SetActive(true);

        noTrashTankReasonState.SetActive(false);
        noFuelReasonState.SetActive(false);

        if (looseReason == LOOSE_REASON.NO_CAPACITY)
        {
            noTrashTankReasonState.SetActive(true);
        }

        if (looseReason == LOOSE_REASON.NO_FUEL)
        {
            noFuelReasonState.SetActive(true);
        }
    }

    public void ShowResult(Dictionary<int, int> resultData)
    {
        trashResultPanel.FillPanel(resultData);
    }
}
