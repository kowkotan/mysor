﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrashResultCell : MonoBehaviour
{
    [SerializeField] Image trashIcon;
    [SerializeField] Text trashCountText;

    public void FillCell(Sprite _trashIcon, int _trashCount)
    {
        trashIcon.sprite = _trashIcon;
        trashCountText.text = _trashCount.ToString();
    }
}
