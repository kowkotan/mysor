﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Adic;

public class Trash : MonoBehaviour, IPickup
{ 
    public bool isHided;
    public bool isPickuped;
    [Inject] CollectTrash collectTrash;

    public TrashItems trashItems;

    string trashName;

    Collider2D collider;

    void Awake()
    {
        trashName = gameObject.name;
    }

    void Start()
    {
        this.Inject();
        collider = GetComponent<Collider2D>(); 
    }

    public string GetName()
    {
        return trashName;
    }

    public void Pickup()
    {
        isPickuped = true;
        collider.enabled = false;

        if(PlayerPrefs.GetInt("robotGameTutorialPart1Completed") == 1)
        {
            if (PlayerPrefs.GetString("CollectQuestTrashType") == "FOOD" && (trashItems == TrashItems.Food1 || trashItems == TrashItems.Food2 || trashItems == TrashItems.Food3))
                collectTrash.Increment();
            else if (PlayerPrefs.GetString("CollectQuestTrashType") == "PLASTIC" && (trashItems == TrashItems.BigPlasticBottle1 || trashItems == TrashItems.PlasticBag1 || trashItems == TrashItems.PlasticBottle1))
                collectTrash.Increment();
            else if (PlayerPrefs.GetString("CollectQuestTrashType") == "GLASS" && (trashItems == TrashItems.GlassBottle1 || trashItems == TrashItems.GlassJar1 || trashItems == TrashItems.GlassJar2))
                collectTrash.Increment();
            else if (PlayerPrefs.GetString("CollectQuestTrashType") == "PAPER" && (trashItems == TrashItems.Paper1 || trashItems == TrashItems.Paper2 || trashItems == TrashItems.Paper3))
                collectTrash.Increment();
        }
        //-gameObject.SetActive(false);
    }

    public void Hide()
    {
        isHided = true;
        gameObject.SetActive(false);
    }

    public void Show()
    {
        isHided = false;
        gameObject.SetActive(true);
    }
}
