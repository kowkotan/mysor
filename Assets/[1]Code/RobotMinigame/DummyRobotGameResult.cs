﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DummyRobotGameResult : MonoBehaviour,IRobotGameResult
{
    Dictionary<int, int> trashDic = new Dictionary<int, int>();

    void Awake()
    {
        trashDic.Add(1, 3);
        trashDic.Add(11, 4);
    }

    public void CountIdsAndCountsOfTrashItems(Trash[] _trashList)
    {
       //;
    }

    public Dictionary<int, int> GetPickedTrashDictionary()
    {
        return trashDic;
    }


}
