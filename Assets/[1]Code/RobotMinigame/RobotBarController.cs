﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;

public class RobotBarController : MonoBehaviour
{

    RobotData robotData;

    [Inject] RobotMinigameSystem robotMinigameSystem;

    [SerializeField]
    ProgressBarWithTextFillingValue fuelTank;

    [SerializeField]
    ProgressBarWithText progressTank;

    [SerializeField] Image fuelBarFOrTutorial;

    Robot robot;

    private void Awake()
    {
       
    }

    private void Start()
    {
        this.Inject();
        robotMinigameSystem.OnMinigameLoaded += RobotMinigameSystem_OnMinigameLoaded;
    }

    void RobotMinigameSystem_OnMinigameLoaded()
    {

        robot = FindObjectOfType<Robot>();

        robotData = robot.GetRobotData();


    }

    private void OnDestroy()
    {
        robotMinigameSystem.OnMinigameLoaded -= RobotMinigameSystem_OnMinigameLoaded;
    }

    private void Update()
    {
        if (robotData == null) return;

        robotData = robot.GetRobotData();
        fuelTank.FillValue(robotData.currentFuelTank, robotData.maxFuelTank);
        progressTank.FillValue(robotData.currentTrashTank, robotData.maxTrashTank);
    }
}
