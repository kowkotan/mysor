﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework;
using Framework.Managers.States;
using UnityEngine;
using UnityEngine.SceneManagement;
using GameAnalyticsSDK;

public enum LOOSE_REASON { NO_FUEL, NO_CAPACITY }

public class RobotMinigameSystem : MonoBehaviour
{

    [Inject] LevelLoader levelLoader;
    [Inject] StateManager stateManager;
    ProgressBarWithText robotBarController;

  public  bool gameoverflag;

    public Action OnMinigameLoaded;
    public Action OnMinigameWin = delegate { };
    public Action OnMinigameLoose = delegate {};

    [SerializeField] TrashLevelTrackSystem trashLevelTrackSystem;
    [SerializeField] GameObject upgPanel;
    [SerializeField] GameOverPanelView gameOverPanel;
    [SerializeField] GameWinPanelView gameWinPanel;
    [SerializeField] AudioSource audioSource;

 
    [Inject] TrashSaveLoadSystem trashSaveLoadSystem;
    [Inject] GameProgressModel gameProgressModel;

    RobotGameResult robotGameResult;

    int factoryZoneNumber;
    int sortingLevelNumber;

    private void Awake() {
        GameAnalytics.Initialize();
    }

    private void Start()
    {
        this.Inject();

        robotGameResult = FindObjectOfType<RobotGameResult>();
        Invoke("LoadMiniGame", 0.01f);
    }

    public void LoadMinigame(int _factoryZoneNumber, int _minigameLevel)
    {

        if (PlayerPrefs.GetInt("robotGameTutorialPart1Completed") == 1)  
        {
            levelLoader.LoadLevel(new RobotAndSortingPathProvider(PlayerPrefs.GetString("PlayerLevelTest"), PlayerPrefs.GetString("PlayerLocationTest")));
        }
        else
        {
            levelLoader.LoadLevel(new RobotAndSortingPathProvider(PlayerPrefs.GetString("PlayerLevelTutorial"), PlayerPrefs.GetString("PlayerLocationTutorial")));
        }

        OnMinigameLoaded();
        gameoverflag = false;
    }

    public void Restart()
    {
        Scene scene = SceneManager.GetActiveScene(); SceneManager.LoadScene(scene.name);
    }

    public void LoadSortnigGame()
    {
        if (trashLevelTrackSystem.pickedTrashIsNotNull())
        {
            stateManager.ChangeScene("SortingGame");
        }
        else
        {
            stateManager.ChangeScene("FactoryMap");
        }
    }

    public void LoadMiniGame()
    {
         //factoryZoneNumber = gameProgressModel.GetCurrentFactoryLevel();
         //sortingLevelNumber = gameProgressModel.GetCurrentSortingLevel();

        if (PlayerPrefs.GetInt("robotGameTutorialPart1Completed") == 1)  
        {
            factoryZoneNumber = Convert.ToInt32(PlayerPrefs.GetString("PlayerLevelTest"));
            sortingLevelNumber = Convert.ToInt32(PlayerPrefs.GetString("PlayerLocationTest"));   
        }
        else
        {
            factoryZoneNumber = Convert.ToInt32(PlayerPrefs.GetString("PlayerLevelTutorial"));
            sortingLevelNumber = Convert.ToInt32(PlayerPrefs.GetString("PlayerLocationTutorial"));
        }

        trashSaveLoadSystem.LoadAllPickedTrashes(factoryZoneNumber, sortingLevelNumber);
        LoadMinigame(factoryZoneNumber, sortingLevelNumber);

        if (PlayerPrefs.GetInt("robotGameTutorialPart1Completed") == 1) upgPanel.SetActive(true);
            else return;
        Debug.Log(trashLevelTrackSystem.GetTrashRatio());
        Debug.Log("factoryZoneNumber:" + factoryZoneNumber + " sortingLevelNumber:" + sortingLevelNumber);
    }

    public void GameWin()
    {
        if (gameoverflag) return;
        gameoverflag = true;

        trashSaveLoadSystem.ReloadAllTrashesh(factoryZoneNumber, sortingLevelNumber, trashLevelTrackSystem.GetTrashes());
        //trashSaveLoadSystem.SaveAllPickedTrashes(factoryZoneNumber, sortingLevelNumber, trashLevelTrackSystem.GetTrashes());

        Debug.Log("GAME WIN");

        MiniGameEnd();

        gameWinPanel.Show();
        gameWinPanel.ShowResult(robotGameResult.GetPickedTrashDictionary());
        audioSource.Play();

        PlayerPrefs.SetInt("RobotLocation" + PlayerPrefs.GetString("PlayerLevelTest") + "-" + sortingLevelNumber + "Progress", 100);

        Dictionary<String, System.Object> eventParameters = new Dictionary<String, System.Object>();
        eventParameters.Add("Очистка локации", "Полностью очистил локацию " + PlayerPrefs.GetString("PlayerLevelTest") + "-" + sortingLevelNumber);
        AppMetrica.Instance.ReportEvent("Сбор", eventParameters);
        GameAnalytics.NewDesignEvent("Collect:"+"Polnostu ochistil lokaciu " + PlayerPrefs.GetString("PlayerLevelTest") + "-" + sortingLevelNumber);

        //GameWinPrefsForLevels
        PlayerPrefs.SetString("RobotLocation" + PlayerPrefs.GetString("PlayerLevelTest") + "-" + sortingLevelNumber + "StartDateTime", DateTime.Now.ToString());

        AppMetrica.Instance.SendEventsBuffer();

        gameProgressModel.SetCurrentSortingLevel(sortingLevelNumber + 1);

        OnMinigameWin();
    }

    public void GameLoose(LOOSE_REASON _looseReason)
    {
        if (gameoverflag) return;
        gameoverflag = true;

        trashSaveLoadSystem.SaveAllPickedTrashes(factoryZoneNumber, sortingLevelNumber, trashLevelTrackSystem.GetTrashes());

        //Debug.Log("TOTALTRASH:" + (trashLevelTrackSystem.currentTrashOnLevel + trashLevelTrackSystem.currentHidedTrashOnLevel));
        //Debug.Log("SORTOMNG"+sortingLevelNumber);

        if (PlayerPrefs.GetInt("robotGameTutorialPart1Completed") == 1)  
        {
            
        }
        else
        {
            PlayerPrefs.SetInt("RobotLocation" + PlayerPrefs.GetString("PlayerLevelTest") + "-" + (sortingLevelNumber + 1) + "Progress", 0);
            //gameProgressModel.SetCurrentSortingLevel(sortingLevelNumber + 1);
        }

        Debug.Log("GAME LOOSE " + _looseReason.ToString());

        StartCoroutine(Loose(_looseReason));


    }

    /// <summary>
    /// /////////// TrashSave
    /// </summary>




    IEnumerator Loose(LOOSE_REASON _looseReason)
    {
        yield return new WaitForSeconds(0.8f);
         MiniGameEnd();
        gameOverPanel.Show(_looseReason);
        gameOverPanel.ShowResult(robotGameResult.GetPickedTrashDictionary());
        OnMinigameLoose();
        yield return null;
    }

    void MiniGameEnd()
    {
        robotGameResult.CountIdsAndCountsOfTrashItems(trashLevelTrackSystem.GetTrashes());
        gameoverflag = true;
    }

}
