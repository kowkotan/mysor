﻿using UnityEngine;

public interface IRobotInput
{
    Vector3 GetMoveDirection();
    void SetMoveDirection(Vector3 moveDir);
    void Tick();
}