﻿using System;
using Framework.Interfaces;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseInput:ITick,IRobotInput
{
     Vector3 moveVector;

    Camera mainCamera;

    public MouseInput(Camera _mainCamera)
    {
        mainCamera = _mainCamera;
    }

    public Vector3 GetMoveDirection()
    {
        return moveVector;
    }

    public void SetMoveDirection(Vector3 moveDir)
    {
        moveVector = moveDir;
    }

    public void Tick()
    {
        if(Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            moveVector = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        }
    }
}

