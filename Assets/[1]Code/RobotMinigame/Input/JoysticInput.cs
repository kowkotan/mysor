﻿using System;
using Framework.Interfaces;
using UnityEngine;
using UnityEngine.EventSystems;

public class JoysticInput : ITick,IRobotInput
{

    Joystick joystick;

    public JoysticInput(Joystick _joystic)
    {
        joystick = _joystic;
    }

    public Vector3 GetMoveDirection()
    {
        return joystick.Direction; 
    }

    public void SetMoveDirection(Vector3 moveDir)
    {
        //moveVector = moveDir;
    }

    public void Tick()
    {
        //
    }
}

