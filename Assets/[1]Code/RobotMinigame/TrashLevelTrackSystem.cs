﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;

public class TrashLevelTrackSystem : MonoBehaviour
{
    [SerializeField]
    Trash[] trashArray;

    int totalTrashOnLevel;
    public int currentTrashOnLevel;
    int currentHidedTrashOnLevel = 0;
    public double trashLocationProgress; 

    [Inject] RobotMinigameSystem robotMinigameSystem;
    [Inject] TrashSaveLoadSystem trashSaveLoadSystem;
    [Inject] CollectTrash collectTrash;

    [SerializeField]
    ProgressBarWithTextFillingValue progressBarWithText;


    private void Start()
    {
        this.Inject();
        robotMinigameSystem.OnMinigameLoaded += RobotMinigameSystem_OnMinigameLoaded;
    }

    public bool pickedTrashIsNotNull()
    {
        return currentTrashOnLevel > 0;
    }


    public Trash[] GetTrashes()
    {
        return trashArray;
    }

    void RobotMinigameSystem_OnMinigameLoaded()
    {
        GettAllActiveTrashOnLevel();
    }

    void ResetAllTrashOnLevel()
    {
        trashArray =  FindObjectsOfType<Trash>();



        totalTrashOnLevel = trashArray.Length;

        for (int i = 0; i < trashArray.Length; i++)
        {
            Debug.Log("SHOW  " + trashArray[i].GetName());
            trashArray[i].Show();
            // _trashList.RemoveAt(i);
        }
    }

    void GettAllActiveTrashOnLevel()
    {

        List<Trash> _trashList = new List<Trash>();

        trashArray =  FindObjectsOfType<Trash>();

        _trashList.AddRange(trashArray);


        totalTrashOnLevel = trashArray.Length;

        for (int i = 0; i < trashArray.Length; i++)
        {
            for (int k = 0; k < trashSaveLoadSystem.DeactiveTrashesId.Count; k++)
            {
                if (trashArray[i].gameObject.name == trashSaveLoadSystem.DeactiveTrashesId[k])
                {
                    Debug.Log("HIDE  " + trashArray[i].GetName());
                    _trashList[i].Hide();
                   // _trashList.RemoveAt(i);
                }
            }
        }

        trashArray = _trashList.ToArray();

    }

    public float GetTrashRatio()
    {
        return currentTrashOnLevel / totalTrashOnLevel;
    }

    void Update()
    {
        currentTrashOnLevel = 0;
        currentHidedTrashOnLevel = 0;

        for (int i = 0; i < trashArray.Length; i++)
        {
            if (trashArray[i].isHided == true)
            {
                currentHidedTrashOnLevel++;
            }

            if (trashArray[i].isPickuped == true)
            {
                currentTrashOnLevel++;
            }
        }
        progressBarWithText.FillValue(currentTrashOnLevel + currentHidedTrashOnLevel, totalTrashOnLevel);
        trashLocationProgress = totalTrashOnLevel/100.0;
        if (PlayerPrefs.GetInt("robotGameTutorialPart1Completed") == 1)  
        {
            PlayerPrefs.SetInt("RobotLocation" + PlayerPrefs.GetString("PlayerLevelTest") + "-" + PlayerPrefs.GetString("PlayerLocationTest") + "Progress", (int)((currentTrashOnLevel + currentHidedTrashOnLevel)/trashLocationProgress));
        }
        else
        {

        }
        Debug.Log(PlayerPrefs.GetInt("RobotLocation" + PlayerPrefs.GetString("PlayerLevelTest") + "-" + PlayerPrefs.GetString("PlayerLocationTest") + "Progress"));

        if (trashArray.Length > 0)
        {
            if (currentTrashOnLevel + currentHidedTrashOnLevel == totalTrashOnLevel)
                robotMinigameSystem.GameWin();
        }
    }

    private void OnDestroy()
    {
        robotMinigameSystem.OnMinigameLoaded -= RobotMinigameSystem_OnMinigameLoaded;
    }
}
