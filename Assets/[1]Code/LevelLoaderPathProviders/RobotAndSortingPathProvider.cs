﻿using UnityEngine;

public class RobotAndSortingPathProvider : IPathProvider
{
    string Level;
    string FactoryZoneLevel;

    public RobotAndSortingPathProvider(string _factoryZoneLevel , string _level)
    {
        Level = _level;
        FactoryZoneLevel = _factoryZoneLevel;
    }

    public string GetLevelPath()
    {
        return string.Format("Levels/Robot/{0}-{1}", FactoryZoneLevel, Level);
        //return string.Format("Levels/Robot/1-{0}", PlayerPrefs.GetString("PlayerLocationTest"));
    }
}

