﻿public interface IPathProvider
{
     string GetLevelPath();
}