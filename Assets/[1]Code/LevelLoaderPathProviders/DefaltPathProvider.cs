﻿using System;

public class DefaltPathProvider : IPathProvider
{
    int Level;
    public void Init(int _level)
    {
        Level = _level;
    }

    public string GetLevelPath()
    {
        return string.Format("Levels/{0}", Level);
    }
}

