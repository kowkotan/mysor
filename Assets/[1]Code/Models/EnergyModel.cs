﻿using System;
using Adic;
using Framework.Interfaces;
using Framework.Managers.Event;
using UnityEngine;

public class EnergyModel : MonoBehaviour,IFixedUpdatable,IDisposable,IEventListener
{
    [Inject] MagicShopController magicShop;
    [Inject] LevelModel levelModel;
    [Inject] EventManager eventManager;

    int maxEnergy;
    public const int energyRefuelTime = 300;

    public Action<int> OnEnergyDecrased = delegate {};

    [Inject]
    public void Init()
    {
        energy = PlayerPrefs.GetInt("currentenergy", maxEnergy);
        refullEnergyStart = PlayerPrefs.GetInt("refullEnergyStart", 0);
        maxEnergy = BalancedValues.GetMaxEnergyForLevel(levelModel.CurrentLevel);
    }

    void Start()
    {
        this.Inject();
        eventManager.AddListener(EVENT_TYPE.LEVEL_UP, this);
    }

    public void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param = null)
    {
        NewLevelUpdate();
    }

    public void NewLevelUpdate()
    {
        maxEnergy = BalancedValues.GetMaxEnergyForLevel(levelModel.CurrentLevel);
    }

    int refullEnergyStart;
    public int RefullEnergyStartTimeInSec
    {
        get
        {
            return refullEnergyStart;
        }
        protected set
        {
            refullEnergyStart = value;
            PlayerPrefs.SetInt("refullEnergyStart", refullEnergyStart);
        }
    }

    int energy;
    public int Energy
    {
        get
        {
            return energy;
        }
        protected set
        {
            energy = value;
            PlayerPrefs.SetInt("currentenergy", energy);
        }
    }

    public int MaxEnergy
    {
        get
        {
            return maxEnergy;
        }
        set
        {
            maxEnergy = value;
        }
    }

    int timeToFullRechange;
    public int TimeToFullRechange
    {
        get
        {
            return timeToFullRechange;
        }
        protected set
        {
            timeToFullRechange = value;
        }
    }

    int timeToNextRechange;
    public int TimeToNextRechange
    {
        get
        {
            return timeToNextRechange;
        }
        protected set
        {
            timeToNextRechange = value;
        }
    }

    public void AddEnergy(int count)
    {
        Energy += Math.Abs( count );

        if (Energy >= MaxEnergy)
        {
            RefullEnergyStartTimeInSec = 0;
        }
       
        UpdateTimeToFullRechange();
    }

    public bool DecEnergy(int count)
    {
        if(Energy < count)
        {
            OnNotEnoughEnergy();
            return false;
        }

        Energy -= Math.Abs( count );

        if (Energy < MaxEnergy)
        {
            RefullEnergyStartTimeInSec = DateMaster.NowSeconds();
        }

        UpdateTimeToFullRechange();
        OnEnergyDecrased(count);

        return true;
    }

    void OnNotEnoughEnergy()
    {
        magicShop.OpenMagicShop();
    }

    void UpdateTimeToFullRechange()
    {
        if (RefullEnergyStartTimeInSec > 0)
        {
            int curSeconds = DateMaster.NowSeconds() - RefullEnergyStartTimeInSec;
             timeToFullRechange = energyRefuelTime * (MaxEnergy - Energy) - curSeconds;
        }
    }

    void UpdateTimeToNextRechange()
    {
        int curSeconds = DateMaster.NowSeconds() - RefullEnergyStartTimeInSec;
        timeToNextRechange = energyRefuelTime - curSeconds;
    }

    private void RefullEnergyUpdate()
    {
        if (energy < MaxEnergy)
        {
            int curSeconds = DateMaster.NowSeconds() - RefullEnergyStartTimeInSec;
            if (curSeconds > energyRefuelTime)
            {
                int incEnergy = Mathf.Min(MaxEnergy - Energy, curSeconds / energyRefuelTime);
                RefullEnergyStartTimeInSec += incEnergy * energyRefuelTime;
                AddEnergy(incEnergy);
            }
        }
    }

    public void FixedUpdate()
    {
        UpdateTimeToFullRechange();
        UpdateTimeToNextRechange();

        RefullEnergyUpdate();
    }

    public void Dispose()
    {
        magicShop = null;
    }
}
