﻿using UnityEngine;
using System.Collections;
using Adic;
using System;

public class EnergyValueModel : ValueModelBase, IDisposable
{
    [Inject] MagicShopController magicShopController;
    [Inject] EnergyModel energyModel;


    [Inject]
    public  void Init()
    {
        base.Init("energy", 200, 999999);

        OnNotEnoughValues += SoftValueModel_OnNotEnoughValues;
    }


    void SoftValueModel_OnNotEnoughValues()
    {
        magicShopController.OpenMagicShop();
    }


    public void Dispose()
    {
        OnNotEnoughValues -= SoftValueModel_OnNotEnoughValues;
        magicShopController = null;
    }

    public override void AddValue(int _value)
    {
        energyModel.AddEnergy(_value);
    }

}
