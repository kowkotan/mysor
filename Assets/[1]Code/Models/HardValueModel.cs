﻿using UnityEngine;
using System.Collections;
using Adic;
using System;

public class HardValueModel : ValueModelBase, IDisposable
{
    [Inject] MagicShopController magicShopController;

  
    [Inject]
    public  void Init()
    {
        base.Init("hard", 50, 999999);

        //AddValue(200);

        OnNotEnoughValues += HardValueModel_OnNotEnoughValues;
    }

    void HardValueModel_OnNotEnoughValues()
    {
        //magicShopController.OpenMagicShop();
    }

    public void Dispose()
    {
        OnNotEnoughValues -= HardValueModel_OnNotEnoughValues;
        magicShopController = null;
    }

}
