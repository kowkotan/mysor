﻿using UnityEngine;
using System.Collections;
using Adic;
using System;

public class DetalsModel : ValueModelBase, IDisposable
{
    [Inject] MagicShopController magicShopController;

    [Inject]
    public  void Init()
    {
        base.Init("detals", 10, 999999);

        OnNotEnoughValues += DetalsModel_OnNotEnoughValues;
    }

    void DetalsModel_OnNotEnoughValues()
    {
        magicShopController.OpenMagicShop();
    }

    public void Dispose()
    {
        OnNotEnoughValues -= DetalsModel_OnNotEnoughValues;
        magicShopController = null;
    }

}
