﻿using System;
using Adic;
//using Facebook.Unity;
using Framework;
using Framework.Interfaces;
using Framework.Managers.Ads;
using Framework.Managers.States;
//using GameAnalyticsSDK;
using UnityEngine;

public class GameControlModel:IDisposable
{
    [Inject] LevelLoader levelLoader;
    [Inject] LevelModel levelModel;
    [Inject] BaseAdSystem baseAdSystem;
    [Inject] IVibrationSystem vibrationSystem;
    //public void ResetGame()
    //{
    //    PlayerPrefs.DeleteKey("cucurrentLevel");
    //    LoadLevel();
    //}

    int restartCounter;

    public void Restart()
    {

//        levelLoader.LoadLevel(levelModel.CurrentLevel);
        GlobalVars.gameOverFlag = false;
        restartCounter++;
        vibrationSystem.LightVibrate();
   

        if (restartCounter % 3 == 0)
        {
            baseAdSystem.ShowInterstetial((bool isLoaded) =>
            {
                if (isLoaded)
                {
                    //FB.LogAppEvent("InterstetialAdShown");
                    //GameAnalytics.NewDesignEvent("InterstetialAdShown");

                }
            });
        }
    }


    public void LoadLevel( int levelNumb )
    {
        levelModel.CurrentLevel = levelNumb;
        baseAdSystem.ShowBanner();
//        levelLoader.LoadLevel(levelNumb);
        GlobalVars.gameOverFlag = false;
    }

    public void Dispose()
    {
        levelLoader = null;
        baseAdSystem = null;
        levelModel = null;
        vibrationSystem = null;
    }
}

