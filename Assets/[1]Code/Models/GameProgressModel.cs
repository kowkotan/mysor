﻿using UnityEngine;
using System.Collections;
using Adic;
using System;

public class GameProgressModel: IDisposable
{
    [Inject] ISaveLoadSystem saveLoadSystem;

    int currentFactoryLevel;

    int currentSortingLevel;
    int maxSortingLevel = 6;


    [Inject]
    void Load()
    {
        currentFactoryLevel = saveLoadSystem.GetInt("currentFactoryLevel", 1);
        currentSortingLevel = saveLoadSystem.GetInt("PlayerLocationTest", 1);
    }


    public void SetCurrentFactoryLevel( int _value )
    {
        currentFactoryLevel = _value;
        saveLoadSystem.SetInt("currentFactoryLevel", currentFactoryLevel);
    }

    public int GetCurrentFactoryLevel()
    {
        return currentFactoryLevel;
    }

    public void SetCurrentSortingLevel(int _value)
    {
    
        currentSortingLevel =  Math.Min( _value,maxSortingLevel);
        saveLoadSystem.SetInt("PlayerLocationTest", currentSortingLevel);
    }

    public int GetCurrentSortingLevel()
    {
        return currentSortingLevel;
    }

    public int GetCurrentMaxSortingLevel()
    {
        return maxSortingLevel;
    }

    public void Dispose()
    {
        saveLoadSystem = null;
    }
}
