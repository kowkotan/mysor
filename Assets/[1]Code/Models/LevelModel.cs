﻿using System;
using System.Collections;
using Adic;
using Framework.Interfaces;
using Framework.Managers.Event;
using UnityEngine;

public class LevelModel : MonoBehaviour, IDisposable
{
    [Inject] EventManager eventManager;


    bool LevelisReadyToUp;

    [Inject]
    public void Init()
    {
        Load();
      //  eventManager.AddListener(EVENT_TYPE.CHARACTERS_CHANGED, this);
    }

 

   public float exp;
    public float Exp
    {
        get
        {
            return exp;
        }
          private set
        {

            exp = value;
        }
    }

  public  float maxExp;
    public float MaxExp
    {
        get
        {
            return maxExp;
        }
        private set
        {
            maxExp = value;
        }
    }

    int currentLevel;
    public int CurrentLevel
    {
        get
        {
            return currentLevel;
        }
        set
        {
            currentLevel = value;
        }
    }

    public void Save()
    {
        PlayerPrefs.SetInt("currentLevel", CurrentLevel);
        PlayerPrefs.SetFloat("exp", Exp);
    }

    public void Load()
    {
        Exp = PlayerPrefs.GetFloat("exp");
        CurrentLevel = PlayerPrefs.GetInt("currentLevel", 1);
        maxExp = GetMaxExpValue();
    }


    public float getLevelProgressRatio()
    {
        return (float)Exp / MaxExp;
    }

    public void AddExp(long _exp)
    {

        if (Exp+_exp >= MaxExp)
        {
            StartCoroutine( CountAndUpLevels(_exp) );
        }

        if(Exp < MaxExp)
        {
            Exp += _exp;
            eventManager.PostNotification(EVENT_TYPE.EXP_GROW, null);
        }
        Save();
    }

    IEnumerator CountAndUpLevels(long _exp)
    {
        int newLevelCount = 1;
        while ( ((Exp + _exp) / MaxExp) >= 2 )
        {
            Exp -= MaxExp;
            MaxExp = GetMaxExpValue();
            newLevelCount++;
        }
        LevelUp(newLevelCount);
        yield return null;
    }

    public void DecExp(long _exp)
    {
        if(Exp > 0)
        {
            Exp -= _exp;
            eventManager.PostNotification(EVENT_TYPE.EXP_FALL, null);
        }
        else
        {
            Exp = 0;
        }

    }

    public LevelProgressData GetProgressData()
    {
        return new LevelProgressData(Exp, MaxExp, CurrentLevel);
    }


    public void LevelUp(int _newLevelsNumber = 1)
    {
        LevelisReadyToUp = false;
        Exp = 0;
        CurrentLevel += _newLevelsNumber;
        MaxExp = GetMaxExpValue();

        LevelProgressData levelProgress = GetProgressData();


        eventManager.PostNotification(EVENT_TYPE.LEVEL_UP, null, _newLevelsNumber);

        Save();
    }

    int GetMaxExpValue()
    {
        return BalancedValues.GetMaxExp(currentLevel);
    }

    public void Dispose()
    {
        eventManager = null;
    }

}


public class LevelProgressData
{
    public int CurrentLevel;
    public float Exp, MaxExp;
    public int CountOfNewLevels;

    public LevelProgressData(float exp, float maxExp, int currentLevel)
    {
        Exp = exp;
        MaxExp = maxExp;
        CurrentLevel = currentLevel;
    }


}