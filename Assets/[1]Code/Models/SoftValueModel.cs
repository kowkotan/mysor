﻿using UnityEngine;
using System.Collections;
using Adic;
using System;

public class SoftValueModel : ValueModelBase, IDisposable
{
    [Inject] MagicShopController magicShopController;


    [Inject]
    public  void Init()
    {
        base.Init("soft", 1000, 999999);

        OnNotEnoughValues += SoftValueModel_OnNotEnoughValues;
    }


    void SoftValueModel_OnNotEnoughValues()
    {
        magicShopController.OpenMagicShop();
    }


    public void Dispose()
    {
        OnNotEnoughValues -= SoftValueModel_OnNotEnoughValues;
        magicShopController = null;
    }

}
