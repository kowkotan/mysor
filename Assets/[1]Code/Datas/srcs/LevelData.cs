﻿using System;
using UnityEngine;

[System.Serializable]
public struct LevelData
{
    public string id;
    public int level;
    public int reloadTimeSec;

    public bool reloadInProgress;
    public DateTime reloadStartDate;
}
