﻿using System;
using UnityEngine;

[System.Serializable]
public struct FactoryData
{
    public string id;
    public int level;
    public int maxLevel;
    public int buyPrice;
    public int upgradeTimeSec;

    public bool buildInProgress;
    public bool isAdSpeeded;
    public DateTime buildStartDate;

}

