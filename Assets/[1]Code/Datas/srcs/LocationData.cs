﻿using System;
using UnityEngine;

[System.Serializable]
public struct LocationData
{
    public string id;
    public string levelToLoad;
    public int upgradeTimeSec;
    
    public bool reloadInProgress;
    public bool isComplited;
    public DateTime reloadStartDate;
}
