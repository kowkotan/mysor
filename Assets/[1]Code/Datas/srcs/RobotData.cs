﻿using System;
using UnityEngine;

[System.Serializable]
public class RobotData
{
    public string robotId;

    public float moveSpeed;

    public float pickupZoneRadius;

    [NonSerialized]
    public int currentTrashTank;

    public int maxTrashTank;

    [NonSerialized]
    public float currentFuelTank;

    public float maxFuelTank;

}

