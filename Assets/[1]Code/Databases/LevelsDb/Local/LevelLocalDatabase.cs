﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLocalDatabase : MonoBehaviour, ILevelLocalDatabase
{
    [SerializeField] LevelLocalStorage levelLocalStorage;

    LevelDBData[] levelDatas;

    private void Awake() {
        levelDatas = levelLocalStorage.levelDatas;
    }

    public LevelDBData GetFullData(string _levelId)
    {
        for(int i = 0; i < levelDatas.Length; i++)
        {
            if(levelDatas[i].levelId == _levelId)
            {
                return levelDatas[i];
            }
        }
        throw new ArgumentNullException(string.Format("Данные для уровня {0} не найдены! ", _levelId));
    }

    public List<LevelDBData> GetUnlockedLevelForLevel(int level)
    {
        List<LevelDBData> outputLevel = new List<LevelDBData>();
        foreach(var map in levelDatas)
        {
            if(map.unlockLevel == level)
            {
                outputLevel.Add(map);
            }
        }

        return outputLevel;
    }

    public int GetUnlockLevel(string _levelId)
    {
        return GetFullData(_levelId).unlockLevel;
    }

    public Sprite GetIcon(string _levelId)
    {
        return GetFullData(_levelId).levelSprite;
    }

    public string GetTitle(string _levelId)
    {
        return GetFullData(_levelId).title;
    }
}
