﻿using UnityEngine;

[CreateAssetMenu(fileName = "LevelLocalStorage", menuName = "Create level data local storage")]

public class LevelLocalStorage : ScriptableObject
{
    public LevelDBData[] levelDatas;
}
