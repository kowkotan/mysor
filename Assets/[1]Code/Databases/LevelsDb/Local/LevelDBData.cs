﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelDBData", menuName = "Create level data")]
public class LevelDBData : ScriptableObject
{
    public string levelId;
    public string title;
    public int unlockLevel;
    public Sprite levelSprite;
}
