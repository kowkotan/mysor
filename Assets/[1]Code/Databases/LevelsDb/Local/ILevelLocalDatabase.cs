﻿using UnityEngine;

public interface ILevelLocalDatabase
{
    LevelDBData GetFullData(string _levelId);
    Sprite GetIcon(string _levelId);
    string GetTitle(string _levelId);
    int GetUnlockLevel(string _levelId);
    System.Collections.Generic.List<LevelDBData> GetUnlockedLevelForLevel(int level);
}
