﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LocationLocalStorage", menuName = "Create location data local storage")]
public class LocationLocalStorage : ScriptableObject
{
    public LocationDBData[] locationDBDatas;
}
