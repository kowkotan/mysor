﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LocationDBData", menuName = "Create location data")]
public class LocationDBData : ScriptableObject
{
    public string locationId;
    public string title;
    public string levelToUnlock;
}
