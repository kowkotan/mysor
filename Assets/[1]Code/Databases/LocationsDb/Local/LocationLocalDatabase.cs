﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationLocalDatabase : MonoBehaviour, ILocationLocalDatabase
{
    [SerializeField] LocationLocalStorage locationLocalStorage;

    LocationDBData[] locationDatas;

    private void Awake() {
        locationDatas = locationLocalStorage.locationDBDatas;
    }

    public LocationDBData GetFullData(string _locationId)
    {
        for(int i = 0; i < locationDatas.Length; i++)
        {
            if(locationDatas[i].locationId == _locationId)
            {
                return locationDatas[i];
            }
        }
        throw new ArgumentNullException(string.Format("Данные для локации {0} не найдены! ", _locationId));
    }

    public string GetTitle(string _locationId)
    {
        return GetFullData(_locationId).title;
    }

    public string GetLevelToLoad(string _locationId)
    {
        return GetFullData(_locationId).levelToUnlock;
    }
}
