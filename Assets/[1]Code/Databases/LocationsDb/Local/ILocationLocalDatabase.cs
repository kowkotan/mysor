﻿using UnityEngine;

public interface ILocationLocalDatabase
{
    LocationDBData GetFullData(string _locationId);
    string GetTitle(string _locationId);
    string GetLevelToLoad(string _locationId);
}
