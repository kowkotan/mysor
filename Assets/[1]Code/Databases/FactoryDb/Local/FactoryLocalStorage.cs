﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "FactoryLocalStorage", menuName = "Create factory data local storage")]

public class FactoryLocalStorage : ScriptableObject
{
    public FactoryDBData[] factoryDatas;
}
