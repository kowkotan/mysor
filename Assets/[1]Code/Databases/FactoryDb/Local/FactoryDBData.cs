﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "FactoryDBData", menuName = "Create factory data")]
public class FactoryDBData : ScriptableObject
{
    public string factoryId;
    public string title;
    public int unlockLevel;
    public Sprite factoryLogo;
    public Sprite containerLogo;
    public Sprite factoryIcon;
    public TrashMaterials trashType;
  
}