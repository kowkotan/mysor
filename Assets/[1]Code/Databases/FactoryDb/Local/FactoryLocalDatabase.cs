﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FactoryLocalDatabase : MonoBehaviour, IFactoryLocalDatabase
{
    [SerializeField]
    FactoryLocalStorage factoryLocalStorage;

    FactoryDBData[] factoryDatas;

    void Awake()
    {
        factoryDatas = factoryLocalStorage.factoryDatas;
    }

    public FactoryDBData GetFullData(string _factoryId)
    {
        for (int i = 0; i < factoryDatas.Length; i++)
        {
            if (factoryDatas[i].factoryId == _factoryId)
            {
                return factoryDatas[i];
            }
        }
        throw new ArgumentNullException(string.Format("Данные для завода {0} не найдены! ", _factoryId));
    }

    public List<FactoryDBData> GetUnlockedFactoryForLevel(int level)
    {
        List<FactoryDBData> outputFactory = new List<FactoryDBData>();
        foreach(var factory in factoryDatas)
        {
            if(factory.unlockLevel == level)
            {
                outputFactory.Add(factory);
            }
        }

        return outputFactory;
    }

    public int GetUnlockLevel(string _factoryId)
    {
        return GetFullData(_factoryId).unlockLevel;
    }

    public Sprite GetLogo(string _factoryId)
    {
       return GetFullData(_factoryId).factoryLogo;
    }

    public Sprite GetIcon(string _factoryId)
    {
        return GetFullData(_factoryId).factoryIcon;
    }

    public string GetTitle(string _factoryId)
    {
        return GetFullData(_factoryId).title;
    }

    public Sprite GetContainerIcon(string _factoryId)
    {
        return GetFullData(_factoryId).containerLogo;
    }

}
