﻿using UnityEngine;

public interface IFactoryLocalDatabase
{
    Sprite GetContainerIcon(string _factoryId);
    FactoryDBData GetFullData(string _factoryId);
    Sprite GetIcon(string _factoryId);
    Sprite GetLogo(string _factoryId);
    string GetTitle(string _factoryId);
    int GetUnlockLevel(string _factoryId);
    System.Collections.Generic.List<FactoryDBData> GetUnlockedFactoryForLevel(int level);
}