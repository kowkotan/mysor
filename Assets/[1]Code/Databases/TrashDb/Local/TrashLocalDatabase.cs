﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TrashLocalDatabase : MonoBehaviour, ITrashDatabase
{
    [SerializeField]
     TrashLocalStorage trashLocalStorage;

    TrashData[] trashDatas;

    void Awake()
    {
        trashDatas = trashLocalStorage.trashDatas;
    }

    public Dictionary<Sprite,TrashMaterials> GetIconsAndMaterialsTrashWithIds(int[] trashIds)
    {
        Dictionary<Sprite, TrashMaterials> resp = new Dictionary<Sprite, TrashMaterials>(); 
        for(int i = 0; i < trashDatas.Length; i++)
        {
            for (int k = 0; k < trashIds.Length; k++)
            {
                if ((int)trashDatas[i].trashType == trashIds[k])
                {
                    resp.Add(trashDatas[i].icon, trashDatas[i].materialType);
                }
            }
        }
        return resp;
    }

    public TrashMaterials GetMaterial(int _trashId)
    {
        for (int i = 0; i < trashDatas.Length; i++)
        {
            if ((int)trashDatas[i].trashType == _trashId)
            {
                return trashDatas[i].materialType;
            }
        }
        throw new ArgumentNullException();
    }

    public Sprite GetSortingItem(int _trashId)
    {
        for (int i = 0; i < trashDatas.Length; i++)
        {
            if ((int)trashDatas[i].trashType == _trashId)
            {
                return trashDatas[i].sortingItem;
            }
        }
        throw new ArgumentNullException();
    }

    public Sprite GetTrashIcon(int _trashId) 
    {
        for (int i = 0; i < trashDatas.Length; i++)
        {       
            if ((int)trashDatas[i].trashType == _trashId)
            {
                return trashDatas[i].icon;
            }
        }
        throw new ArgumentNullException();
    }
}
