﻿using System.Collections.Generic;
using UnityEngine;

public interface ITrashDatabase
{
    Dictionary<Sprite, TrashMaterials> GetIconsAndMaterialsTrashWithIds(int[] trashIds);
    Sprite GetTrashIcon(int _trashId);
    Sprite GetSortingItem(int _trashId);
    TrashMaterials GetMaterial(int _trashId);
}