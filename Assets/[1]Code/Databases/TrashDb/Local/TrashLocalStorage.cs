﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "TrashLocalStorage", menuName = "Create trash data local storage")]

public class TrashLocalStorage : ScriptableObject
{
   public TrashData[] trashDatas;
}
