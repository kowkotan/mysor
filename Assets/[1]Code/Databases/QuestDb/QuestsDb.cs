﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "questDb", menuName = "Create Quest Db Info", order = 51)]
public class QuestsDb : ScriptableObject
{
    public string Description, Id;
    public int BaseCount, Increment, IncrementLevel;

    public string genDescr(int count, TrashMaterials type){
        return $"";
    }
}
