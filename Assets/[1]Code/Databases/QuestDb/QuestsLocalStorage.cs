﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "QuestLocalStorage", menuName = "Create Quest Local Storage", order = 51)]
public class QuestsLocalStorage : ScriptableObject
{
    public QuestsDb[] questsDatas;
}
