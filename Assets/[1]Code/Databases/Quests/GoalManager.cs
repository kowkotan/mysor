﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoalManager : MonoBehaviour
{
    [SerializeField] Goal[] goals;
    [SerializeField] int questCount;
    [SerializeField] Text endText;
    [SerializeField] bool isInitialized;

    private void Awake()
    {
        //dayCheck();

        if (PlayerPrefs.GetInt("GoalManagerIsInitialized") == 1)
        {
            isInitialized = true;
            dayCheck();
        }
        else if (!isInitialized)
        {
            isInitialized = true;
            PlayerPrefs.SetInt("GoalManagerIsInitialized", 1);
            PlayerPrefs.SetString("PlayDate", DateTime.Now.ToString());
            dayCheck();
            Debug.Log("firsttime");
        }
    }

    private void Start()
    {
        foreach (var goal in goals)
        {
            goal.SetText();
        }
        questCount = PlayerPrefs.GetInt("questCountFoDay");

        if (questCount == 5)
        {
            //goal.GenerateNextQuest();
            endText.gameObject.SetActive(true);
            endText.text = "Вы выполнили все задания на сегодня, приходите завтра за новыми!";
        }
    }

    private void Update()
    {
        foreach (var goal in goals)
        {
            if (goal.IsAchieved())
            {
                goal.Complete();
                //Destroy(goal);
            }
        }
    }

    public void dayCheck()
    {
        string stringDate = PlayerPrefs.GetString("PlayDate");
        DateTime oldDate = Convert.ToDateTime(stringDate);
        DateTime newDate = DateTime.Now;
        Debug.Log("LastDay: " + oldDate);
        Debug.Log("CurrDay: " + newDate);

        TimeSpan difference = newDate.Subtract(oldDate);
        if (difference.Days >= 1)
        {
            ResetAllQuests();
            Debug.Log("New Reward!");
            string newStringDate = Convert.ToString(newDate);
            PlayerPrefs.SetString("PlayDate", newStringDate);
        }
    }

    public void CheckForCompleted()
    {
        questCount++;
        PlayerPrefs.SetInt("questCountFoDay", questCount);

        if (questCount == 5)
        {
            //goal.GenerateNextQuest();
            endText.gameObject.SetActive(true);
            endText.text = "Вы выполнили все задания на сегодня, приходите завтра за новыми!";
        }
    }

    public void ResetAllQuests()
    {
        Debug.Log("ALL QUESTS RESETED");
        PlayerPrefs.DeleteKey("GoalManagerIsInitialized");
        PlayerPrefs.DeleteKey("questCountFoDay");
        PlayerPrefs.DeleteKey("PlayDate");

        foreach(var goal in goals)
        {
            goal.ResetQuest();
        }
    }
}
