﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Goal : MonoBehaviour
{
    public abstract bool IsAchieved();
    public abstract bool IsCompleted();
    public abstract bool IsTakeReward();
    public abstract void Complete();
    public abstract void CheckToComplete();
    public abstract void GenerateNextQuest();
    public abstract void ResetQuest();
    public abstract void SetText();
}
