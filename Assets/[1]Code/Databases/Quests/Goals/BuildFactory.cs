﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Adic;
using UnityEngine.UI;

public static class ForFactoryLevelIndex1
{
    public static int IndexOfMin(this IList<int> self)
    {
        if (self == null)
        {
            throw new ArgumentNullException("self");
        }

        if (self.Count == 0)
        {
            throw new ArgumentException("List is empty.", "self");
        }

        int min = self[0];
        int minIndex = 0;

        for (int i = 1; i < self.Count; ++i)
        {
            if (self[i] < min)
            {
                min = self[i];
                minIndex = i;
            }
        }

        return minIndex;
    }
}

public class BuildFactory : Goal
{
    [Inject] SoftValueModel softValueModel;
    [SerializeField] GoalManager goalManager;

    [SerializeField] int requiredFactoryLevel;
    [SerializeField] int needScore = 1;
    [SerializeField] int rewardCount;
    [SerializeField] string factoryNameToBuild;
    [SerializeField] FM_Factory[] factory;

    [SerializeField] int buildedFactory;

    [SerializeField] bool isInitialized;
    [SerializeField] bool isCompleted;
    [SerializeField] bool isCollected;

    [Header("Sources")]
    [SerializeField] Text title;
    [SerializeField] Text description;
    [SerializeField] Text score;
    [SerializeField] Text reward;
    [SerializeField] Button btnToCollect;


    //private PlayerStats playerStats;

    void Awake()
    {
        this.Inject();
        CheckForFactories();
        //playerStats = GameObject.Find("Player").GetComponent<PlayerStats>();
        //sortedTrash = PlayerPrefs.GetInt("CollectableTrashes");
        if (PlayerPrefs.GetInt("BuildQuestIsInitialized") == 1)
        {
            isInitialized = true;
            requiredFactoryLevel = PlayerPrefs.GetInt("BuildQuestRequiredTask");
            buildedFactory = PlayerPrefs.GetInt("BuildFactoryQuest");

            factoryNameToBuild = PlayerPrefs.GetString("BuildQuestFactoryNameForUpgrade");

            SetText();
        }
        if (!isInitialized)
        {
            factoryNameToBuild = RandomTrashType();
            PlayerPrefs.SetString("BuildQuestFactoryNameForUpgrade", factoryNameToBuild);
            //PlayerPrefs.SetInt("UpgradingQuestRequiredTask", requiredFactoryLevel);
            //isInitialized = true;
            PlayerPrefs.SetInt("BuildQuestIsInitialized", 1);
        }
        if (PlayerPrefs.GetInt("BuildFactoryQuestRewardTaked") == 1)
            isCollected = true;
        if (PlayerPrefs.GetInt("BuildFactoryQuestCompleted") == 1)
            isCompleted = true;
        if (isCollected && isCompleted)
        {
            this.gameObject.SetActive(false);
            goalManager.CheckForCompleted();
        }
    }

    string RandomTrashType()
    {
        List<int> factoryLevel = new List<int>();
        for (int i = 0; i < factory.Length; i++)
        {
            factoryLevel.Add(factory[i].factoryData.level);
        }

        int minimalLevelIndex = ForFactoryLevelIndex1.IndexOfMin(factoryLevel);
        string factoryName = string.Empty;

        if (minimalLevelIndex == 0)
            factoryName = "по производству еды";
        else if (minimalLevelIndex == 1)
            factoryName = "по производству бумаги";
        else if (minimalLevelIndex == 2)
            factoryName = "по производству пластика";
        else if (minimalLevelIndex == 3)
            factoryName = "по производству стекла";

        requiredFactoryLevel = factory[minimalLevelIndex].factoryData.level + 1;
        PlayerPrefs.SetInt("BuildQuestRequiredTask", requiredFactoryLevel);

        return factoryName;
    }

    void CheckForFactories()
    {
        if (factory[0].factoryData.level !=0  && factory[1].factoryData.level != 0 && factory[2].factoryData.level != 0 && factory[3].factoryData.level != 0)
        {
            IsCompleted();
            IsTakeReward();
            CheckToComplete();
            goalManager.CheckForCompleted();
        }
    }

    public override void CheckToComplete()
    {
        this.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            this.buildedFactory++;
            SetText();
        }
        SetText();
        PlayerPrefs.SetInt("BuildFactoryQuest", buildedFactory);
    }

    public void Increment()
    {
        this.buildedFactory = PlayerPrefs.GetInt("BuildFactoryQuest");
        this.buildedFactory++;
        PlayerPrefs.SetInt("BuildFactoryQuest", buildedFactory);
        Debug.Log("upgraded factory " + buildedFactory + " " + PlayerPrefs.GetInt("BuildFactoryQuest"));
    }

    public void CollectReward()
    {
        softValueModel.AddValue(rewardCount);
        this.gameObject.SetActive(false);
        isCollected = true;
        PlayerPrefs.SetInt("BuildFactoryQuestRewardTaked", 1);
        goalManager.CheckForCompleted();
        //GenerateNextQuest();
    }

    public override bool IsAchieved()
    {
        return (buildedFactory >= needScore);
    }

    public override void Complete()
    {
        btnToCollect.interactable = true;
        isCompleted = true;
        PlayerPrefs.SetInt("BuildFactoryQuestCompleted", 1);
    }

    public override bool IsCompleted()
    {
        return isCompleted;
    }

    public override bool IsTakeReward()
    {
        return isCollected;
    }

    public override void SetText()
    {
        title.text = "Строительство завода";
        description.text = string.Format("Построй завод {0}", factoryNameToBuild);
        score.text = string.Format("{0}/{1}", buildedFactory, needScore);
    }

    public override void GenerateNextQuest()
    {
        Debug.Log("GO TO THE NEXT QUEST");
    }

    public override void ResetQuest()
    {
        //PlayerPrefs.DeleteKey("CollectableTrashes");
        PlayerPrefs.DeleteKey("BuildQuestIsInitialized");
        PlayerPrefs.DeleteKey("BuildQuestRequiredTask");
        PlayerPrefs.DeleteKey("BuildFactoryQuest");
        PlayerPrefs.DeleteKey("BuildFactoryQuestRewardTaked");
        PlayerPrefs.DeleteKey("BuildFactoryQuestCompleted");
    }

    private void OnEnable()
    {
        score.text = string.Format("{0}/{1}", buildedFactory, needScore);
        buildedFactory = PlayerPrefs.GetInt("BuildFactoryQuest");
        reward.text = rewardCount.ToString();
    }
}
