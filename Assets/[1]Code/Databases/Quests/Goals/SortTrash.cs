﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Adic;
using UnityEngine.UI;

public class SortTrash : Goal
{
    [Inject] SoftValueModel softValueModel;
    [Inject] LevelModel levelModel;
    [SerializeField] GoalManager goalManager;

    [SerializeField] int requiredSort;
    [SerializeField] int rewardCount;
    [SerializeField] TrashMaterials trashTypeToSort;
    [SerializeField] FM_Factory[] factory;

    [SerializeField] int sortedTrash;

    [SerializeField] bool isInitialized;
    [SerializeField] bool isCompleted;
    [SerializeField] bool isCollected;

    [Header("Sources")]
    [SerializeField] Text title;
    [SerializeField] Text description;
    [SerializeField] Text score;
    [SerializeField] Text reward;
    [SerializeField] Button btnToCollect;


    //private PlayerStats playerStats;

    void Awake()
    {
        this.Inject();
        //sortedTrash = PlayerPrefs.GetInt("CollectableTrashes");
        if (PlayerPrefs.GetInt("SortingQuestIsInitialized") == 1)
        {
            isInitialized = true;
            requiredSort = PlayerPrefs.GetInt("SortingQuestRequiredTask");
            sortedTrash = PlayerPrefs.GetInt("SortingTrashQuest");
            rewardCount = GetSortQuestReward(requiredSort);

            if (PlayerPrefs.GetString("SortingQuestTrashType") == "FOOD")
                trashTypeToSort = TrashMaterials.FOOD;
            else if (PlayerPrefs.GetString("SortingQuestTrashType") == "GLASS")
                trashTypeToSort = TrashMaterials.GLASS;
            else if (PlayerPrefs.GetString("SortingQuestTrashType") == "PLASTIC")
                trashTypeToSort = TrashMaterials.PLASTIC;
            else if (PlayerPrefs.GetString("SortingQuestTrashType") == "PAPER")
                trashTypeToSort = TrashMaterials.PAPER;

            SetText();
        }
        if (!isInitialized)
        {
            //System.Random rnd = new System.Random();
            //requiredSort = rnd.Next(2, 10);
            requiredSort = BalancedValues.GetSortingQuestValues(levelModel.CurrentLevel);
            rewardCount = GetSortQuestReward(requiredSort);
            trashTypeToSort = RandomTrashType();
            PlayerPrefs.SetString("SortingQuestTrashType", trashTypeToSort.ToString());
            PlayerPrefs.SetInt("SortingQuestRequiredTask", requiredSort);
            //isInitialized = true;
            PlayerPrefs.SetInt("SortingQuestIsInitialized", 1);
        }
        if (PlayerPrefs.GetInt("SortingQuestRewardTaked") == 1)
            isCollected = true;
        if (PlayerPrefs.GetInt("SortingQuestCompleted") == 1)
            isCompleted = true;
        if (isCollected && isCompleted)
        {
            this.gameObject.SetActive(false);
            goalManager.CheckForCompleted();
        }
    }

    TrashMaterials RandomTrashType()
    {
        Array values = Enum.GetValues(typeof(TrashMaterials));
        System.Random random = new System.Random();
        int values1 = 0;
        for (int i = 0; i < factory.Length; i++)
        {
            if (factory[i].factoryData.level > 1 && factory[i].factoryData.id == "foodFactory1")
                values1 = 1;
            if (factory[i].factoryData.level > 1 && factory[i].factoryData.id == "paperFactory1")
                values1 = 2;
            if (factory[i].factoryData.level > 1 && factory[i].factoryData.id == "plasticFactory1")
                values1 = 3;
            if (factory[i].factoryData.level > 1 && factory[i].factoryData.id == "glassFactory1")
                values1 = 4;
        }
        TrashMaterials randomBar = (TrashMaterials)values.GetValue(random.Next(values1));
        return randomBar;
    }

    public static int GetSortQuestReward(int _needToCollect)
    {
        int[] upValues = { 0, 0, 0, 0, 0, 250, 0, 500, 0, 0, 750, 0, 0, 1000, 0, 1250 };

        return upValues[_needToCollect];
    }

    public override void CheckToComplete()
    {
        this.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            this.sortedTrash++;
            SetText();
        }
        //SetText();
        PlayerPrefs.SetInt("SortingTrashQuest", sortedTrash);
    }

    public void Increment()
    {
        sortedTrash = PlayerPrefs.GetInt("SortingTrashQuest");
        sortedTrash++;
        PlayerPrefs.SetInt("SortingTrashQuest", sortedTrash);
    }

    public void CollectReward()
    {
        softValueModel.AddValue(rewardCount);
        this.gameObject.SetActive(false);
        isCollected = true;
        PlayerPrefs.SetInt("SortingQuestRewardTaked", 1);
        goalManager.CheckForCompleted();
        //GenerateNextQuest();
    }

    public override bool IsAchieved()
    {
        return (sortedTrash >= requiredSort);
    }

    public override void Complete()
    {
        btnToCollect.interactable = true;
        isCompleted = true;
        PlayerPrefs.SetInt("SortingQuestCompleted", 1);
    }

    public override bool IsCompleted()
    {
        return isCompleted;
    }

    public override bool IsTakeReward()
    {
        return isCollected;
    }

    public override void SetText()
    {
        title.text = "Сортировка мусора";

        string trashTypeText = String.Empty;

        if (trashTypeToSort == TrashMaterials.FOOD)
            trashTypeText = "органического";
        else if (trashTypeToSort == TrashMaterials.GLASS)
            trashTypeText = "стеклянного";
        else if (trashTypeToSort == TrashMaterials.PLASTIC)
            trashTypeText = "пластикового";
        else if (trashTypeToSort == TrashMaterials.PAPER)
            trashTypeText = "бумажного";

        description.text = string.Format("Сортировать {0} единиц(ы) {1} мусора", requiredSort, trashTypeText);
        score.text = string.Format("{0}/{1}", sortedTrash, requiredSort);
        rewardCount = GetSortQuestReward(requiredSort);
        reward.text = rewardCount.ToString();
    }

    public override void GenerateNextQuest()
    {
        Debug.Log("GO TO THE NEXT QUEST");
    }

    public override void ResetQuest()
    {
        //PlayerPrefs.DeleteKey("CollectableTrashes");
        PlayerPrefs.DeleteKey("SortingQuestIsInitialized");
        PlayerPrefs.DeleteKey("SortingQuestRequiredTask");
        PlayerPrefs.DeleteKey("SortingTrashQuest");
        PlayerPrefs.DeleteKey("SortingQuestTrashType");
        PlayerPrefs.DeleteKey("SortingQuestRewardTaked");
        PlayerPrefs.DeleteKey("SortingQuestCompleted");
    }

    private void OnDisable()
    {
        PlayerPrefs.SetInt("SortingTrashQuest", sortedTrash);
    }
}
