﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Adic;
using UnityEngine.UI;

public static class ForFactoryLevelIndex
{
    public static int IndexOfMin(this IList<int> self)
{
    if (self == null)
    {
        throw new ArgumentNullException("self");
    }

    if (self.Count == 0)
    {
        throw new ArgumentException("List is empty.", "self");
    }

    int min = self[0];
    int minIndex = 0;

    for (int i = 1; i < self.Count; ++i)
    {
        if (self[i] < min)
        {
            min = self[i];
            minIndex = i;
        }
    }

    return minIndex;
}
}

public class UpgradeFactory : Goal
{
    [Inject] SoftValueModel softValueModel;
    [SerializeField] GoalManager goalManager;

    [SerializeField] int requiredFactoryLevel;
    [SerializeField] int needScore = 1;
    [SerializeField] int rewardCount;
    [SerializeField] string factoryNameToUpgrade;
    [SerializeField] FM_Factory[] factory;

    [SerializeField] int upgradedFactory;

    [SerializeField] bool isInitialized;
    [SerializeField] bool isCompleted;
    [SerializeField] bool isCollected;

    [Header("Sources")]
    [SerializeField] Text title;
    [SerializeField] Text description;
    [SerializeField] Text score;
    [SerializeField] Text reward;
    [SerializeField] Button btnToCollect;


    //private PlayerStats playerStats;

    void Awake()
    {
        this.Inject();
        //playerStats = GameObject.Find("Player").GetComponent<PlayerStats>();
        //sortedTrash = PlayerPrefs.GetInt("CollectableTrashes");
        if (PlayerPrefs.GetInt("UpgradingQuestIsInitialized") == 1)
        {
            isInitialized = true;
            requiredFactoryLevel = PlayerPrefs.GetInt("UpgradingQuestRequiredTask");
            upgradedFactory = PlayerPrefs.GetInt("UpgradingFactoryQuest");
            rewardCount = GetUpgradeFacQuestReward(requiredFactoryLevel);

            factoryNameToUpgrade = PlayerPrefs.GetString("CollectQuestFactoryNameForUpgrade");

            SetText();
        }
        if (!isInitialized)
        {
            factoryNameToUpgrade = RandomTrashType();
            PlayerPrefs.SetString("CollectQuestFactoryNameForUpgrade", factoryNameToUpgrade);
            //PlayerPrefs.SetInt("UpgradingQuestRequiredTask", requiredFactoryLevel);
            //isInitialized = true;
            rewardCount = GetUpgradeFacQuestReward(requiredFactoryLevel);
            PlayerPrefs.SetInt("UpgradingQuestIsInitialized", 1);
        }
        if (PlayerPrefs.GetInt("UpgradingFactoryQuestRewardTaked") == 1)
            isCollected = true;
        if (PlayerPrefs.GetInt("UpgradingFactoryQuestCompleted") == 1)
            isCompleted = true;
        if (isCollected && isCompleted)
        {
            this.gameObject.SetActive(false);
            goalManager.CheckForCompleted();
        }
    }

    string RandomTrashType()
    {
        List<int> factoryLevel = new List<int>();
        for (int i = 0; i < factory.Length; i++)
        {
            if(factory[i].factoryData.level <= 0)
            {

            }
            else factoryLevel.Add(factory[i].factoryData.level);
        }

        int minimalLevelIndex = ForFactoryLevelIndex.IndexOfMin(factoryLevel);
        string factoryName = string.Empty;

        if (minimalLevelIndex == 0)
            factoryName = "по производству еды";
        else if (minimalLevelIndex == 1)
            factoryName = "по производству бумаги";
        else if (minimalLevelIndex == 2)
            factoryName = "по производству пластика";
        else if (minimalLevelIndex == 3)
            factoryName = "по производству стекла";

        requiredFactoryLevel = factory[minimalLevelIndex].factoryData.level + 1;
        PlayerPrefs.SetInt("UpgradingQuestRequiredTask", requiredFactoryLevel);

        return factoryName;
    }

    public static int GetUpgradeFacQuestReward(int _levelNeedToUpgrade)
    {
        int[] upValues = { 0, 250, 500, 750, 1000, 1500 };

        return upValues[_levelNeedToUpgrade];
    }

    public override void CheckToComplete()
    {
        this.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            this.upgradedFactory++;
            SetText();
        }
        SetText();
        PlayerPrefs.SetInt("UpgradingFactoryQuest", upgradedFactory);
    }

    public void Increment()
    {
        upgradedFactory = PlayerPrefs.GetInt("UpgradingFactoryQuest");
        upgradedFactory++;
        PlayerPrefs.SetInt("UpgradingFactoryQuest", upgradedFactory);
        Debug.Log("upgraded factopry " + upgradedFactory + " " + PlayerPrefs.GetInt("UpgradingFactoryQuest"));
    }

    public void CollectReward()
    {
        softValueModel.AddValue(rewardCount);
        this.gameObject.SetActive(false);
        isCollected = true;
        PlayerPrefs.SetInt("UpgradingFactoryQuestRewardTaked", 1);
        goalManager.CheckForCompleted();
        //GenerateNextQuest();
    }

    public override bool IsAchieved()
    {
        return (upgradedFactory >= needScore);
    }

    public override void Complete()
    {
        btnToCollect.interactable = true;
        isCompleted = true;
        PlayerPrefs.SetInt("UpgradingFactoryQuestCompleted", 1);
    }

    public override bool IsCompleted()
    {
        return isCompleted;
    }

    public override bool IsTakeReward()
    {
        return isCollected;
    }

    public override void SetText()
    {
        title.text = "Улучшение завода";
        description.text = string.Format("Улучшите завод {0} до уровня {1}", factoryNameToUpgrade, requiredFactoryLevel);
        score.text = string.Format("{0}/{1}", upgradedFactory, needScore);
    }

    public override void GenerateNextQuest()
    {
        Debug.Log("GO TO THE NEXT QUEST");
    }

    public override void ResetQuest()
    {
        //PlayerPrefs.DeleteKey("CollectableTrashes");
        PlayerPrefs.DeleteKey("UpgradingQuestIsInitialized");
        PlayerPrefs.DeleteKey("UpgradingQuestRequiredTask");
        PlayerPrefs.DeleteKey("UpgradingFactoryQuest");
        PlayerPrefs.DeleteKey("UpgradingFactoryQuestRewardTaked");
        PlayerPrefs.DeleteKey("UpgradingFactoryQuestCompleted");
    }

    private void OnEnable()
    {
        score.text = string.Format("{0}/{1}", upgradedFactory, needScore);
        upgradedFactory = PlayerPrefs.GetInt("UpgradingFactoryQuest");
        rewardCount = GetUpgradeFacQuestReward(requiredFactoryLevel);
        reward.text = rewardCount.ToString();
    }
}
