﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Adic;
using UnityEngine.UI;

public class CreateTrash : Goal
{
    [Inject] SoftValueModel softValueModel;
    [Inject] LevelModel levelModel;
    [SerializeField] GoalManager goalManager;

    [SerializeField] int requiredCreate;
    [SerializeField] int rewardCount;
    [SerializeField] TrashMaterials trashTypeToCreate;
    [SerializeField] FM_Factory[] factory;

    [SerializeField] int createdTrash;

    [SerializeField] bool isInitialized;
    [SerializeField] bool isCompleted;
    [SerializeField] bool isCollected;

    [Header("Sources")]
    [SerializeField] Text title;
    [SerializeField] Text description;
    [SerializeField] Text score;
    [SerializeField] Text reward;
    [SerializeField] Button btnToCollect;


    //private PlayerStats playerStats;

    void Awake()
    {
        this.Inject();
        //sortedTrash = PlayerPrefs.GetInt("CollectableTrashes");
        if (PlayerPrefs.GetInt("CreatingQuestIsInitialized") == 1)
        {
            isInitialized = true;
            requiredCreate = PlayerPrefs.GetInt("CreatingQuestRequiredTask");
            createdTrash = PlayerPrefs.GetInt("CreatingTrashQuest");
            rewardCount = GetCreateQuestReward(requiredCreate);

            if (PlayerPrefs.GetString("CreatingQuestTrashType") == "FOOD")
                trashTypeToCreate = TrashMaterials.FOOD;
            else if (PlayerPrefs.GetString("CreatingQuestTrashType") == "GLASS")
                trashTypeToCreate = TrashMaterials.GLASS;
            else if (PlayerPrefs.GetString("CreatingQuestTrashType") == "PLASTIC")
                trashTypeToCreate = TrashMaterials.PLASTIC;
            else if (PlayerPrefs.GetString("CreatingQuestTrashType") == "PAPER")
                trashTypeToCreate = TrashMaterials.PAPER;

            SetText();
        }
        if (!isInitialized)
        {
            //System.Random rnd = new System.Random();
            //requiredSort = rnd.Next(2, 10);
            requiredCreate = BalancedValues.GetCreatingQuestValues(levelModel.CurrentLevel);
            rewardCount = GetCreateQuestReward(requiredCreate);
            trashTypeToCreate = RandomTrashType();
            //createdTrash = PlayerPrefs.GetInt("CreatingTrashQuest");
            PlayerPrefs.SetString("CreatingQuestTrashType", trashTypeToCreate.ToString());
            PlayerPrefs.SetInt("CreatingQuestRequiredTask", requiredCreate);
            //isInitialized = true;
            PlayerPrefs.SetInt("CreatingQuestIsInitialized", 1);
        }
        if (PlayerPrefs.GetInt("CreatingQuestRewardTaked") == 1)
            isCollected = true;
        if (PlayerPrefs.GetInt("CreatingQuestCompleted") == 1)
            isCompleted = true;
        if (isCollected && isCompleted)
        {
            this.gameObject.SetActive(false);
            goalManager.CheckForCompleted();
        }
    }

    TrashMaterials RandomTrashType()
    {
        Array values = Enum.GetValues(typeof(TrashMaterials));
        System.Random random = new System.Random();
        int values1 = 0;
        for (int i = 0; i < factory.Length; i++)
        {
            if (factory[i].factoryData.level > 1 && factory[i].factoryData.id == "foodFactory1")
                values1 = 1;
            if (factory[i].factoryData.level > 1 && factory[i].factoryData.id == "paperFactory1")
                values1 = 2;
            if (factory[i].factoryData.level > 1 && factory[i].factoryData.id == "plasticFactory1")
                values1 = 3;
            if (factory[i].factoryData.level > 1 && factory[i].factoryData.id == "glassFactory1")
                values1 = 4;
        }
        TrashMaterials randomBar = (TrashMaterials)values.GetValue(random.Next(values1));
        return randomBar;
    }

    public static int GetCreateQuestReward(int _needToCreate)
    {
        int[] upValues = { 0, 0, 500, 1000, 0, 1500, 0, 2000, 0, 0, 2500 };

        return upValues[_needToCreate];
    }

    public override void CheckToComplete()
    {
        this.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            this.createdTrash++;
            SetText();
        }
        //SetText();
        //PlayerPrefs.SetInt("CreatingTrashQuest", createdTrash);
    }

    public void Increment()
    {
        createdTrash = PlayerPrefs.GetInt("CreatingTrashQuest");
        createdTrash++;
        PlayerPrefs.SetInt("CreatingTrashQuest", createdTrash);
        Debug.Log("CURRENT CREATED TRASH " + createdTrash);
    }

    public void CollectReward()
    {
        softValueModel.AddValue(rewardCount);
        this.gameObject.SetActive(false);
        isCollected = true;
        PlayerPrefs.SetInt("CreatingQuestRewardTaked", 1);
        goalManager.CheckForCompleted();
        //GenerateNextQuest();
    }

    public override bool IsAchieved()
    {
        return (createdTrash >= requiredCreate);
    }

    public override void Complete()
    {
        btnToCollect.interactable = true;
        isCompleted = true;
        PlayerPrefs.SetInt("CreatingQuestCompleted", 1);
    }

    public override bool IsCompleted()
    {
        return isCompleted;
    }

    public override bool IsTakeReward()
    {
        return isCollected;
    }

    public override void SetText()
    {
        title.text = "Производство мусора";

        string trashTypeText = String.Empty;

        if (trashTypeToCreate == TrashMaterials.FOOD)
            trashTypeText = "органических";
        else if (trashTypeToCreate == TrashMaterials.GLASS)
            trashTypeText = "стеклянных";
        else if (trashTypeToCreate == TrashMaterials.PLASTIC)
            trashTypeText = "пластиковых";
        else if (trashTypeToCreate == TrashMaterials.PAPER)
            trashTypeText = "стеклянных";

        description.text = string.Format("Произвести {0} {1} товара(ов)", requiredCreate, trashTypeText);
    }

    public override void GenerateNextQuest()
    {
        Debug.Log("GO TO THE NEXT QUEST");
    }

    public override void ResetQuest()
    {
        //PlayerPrefs.DeleteKey("CollectableTrashes");
        PlayerPrefs.DeleteKey("CreatingQuestIsInitialized");
        PlayerPrefs.DeleteKey("CreatingQuestRequiredTask");
        PlayerPrefs.DeleteKey("CreatingTrashQuest");
        PlayerPrefs.DeleteKey("CreatingQuestTrashType");
        PlayerPrefs.DeleteKey("CreatingQuestRewardTaked");
        PlayerPrefs.DeleteKey("CreatingQuestCompleted");
    }

    private void OnEnable()
    {
        createdTrash = PlayerPrefs.GetInt("CreatingTrashQuest");
        score.text = string.Format("{0}/{1}", createdTrash, requiredCreate);
        rewardCount = GetCreateQuestReward(requiredCreate);
        reward.text = rewardCount.ToString();
    }
}
