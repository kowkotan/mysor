﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Adic;
using UnityEngine.UI;

public class CollectTrash : Goal
{
    [Inject] SoftValueModel softValueModel;
    [Inject] LevelModel levelModel;
    [SerializeField] GoalManager goalManager;

    [SerializeField] int requiredCollect;
    [SerializeField] int rewardCount;
    [SerializeField] TrashMaterials trashTypeToCollect;
    [SerializeField] FM_Factory[] factory;

    [SerializeField] int collectedTrash;

    [SerializeField] bool isInitialized;
    [SerializeField] bool isCompleted;
    [SerializeField] bool isCollected;

    [Header("Sources")] 
    [SerializeField] Text title;
    [SerializeField] Text description;
    [SerializeField] Text score;
    [SerializeField] Text reward;
    [SerializeField] Button btnToCollect;


    //private PlayerStats playerStats;

    void Awake()
    {
        this.Inject();
        //playerStats = GameObject.Find("Player").GetComponent<PlayerStats>();
        //collectedTrash = PlayerPrefs.GetInt("CollectableTrashes");
        if (PlayerPrefs.GetInt("CollectQuestIsInitialized") == 1)
        {
            isInitialized = true;
            requiredCollect = PlayerPrefs.GetInt("CollectQuestRequiredTask");
            collectedTrash = PlayerPrefs.GetInt("CollectTrashQuest");
            rewardCount = GetCollectQuestReward(requiredCollect);

            if (PlayerPrefs.GetString("CollectQuestTrashType") == "FOOD")
                trashTypeToCollect = TrashMaterials.FOOD;
            else if (PlayerPrefs.GetString("CollectQuestTrashType") == "GLASS")
                trashTypeToCollect = TrashMaterials.GLASS;
            else if (PlayerPrefs.GetString("CollectQuestTrashType") == "PLASTIC")
                trashTypeToCollect = TrashMaterials.PLASTIC;
            else if (PlayerPrefs.GetString("CollectQuestTrashType") == "PAPER")
                trashTypeToCollect = TrashMaterials.PAPER;

            SetText();
        }
        if (!isInitialized)
        {
            //System.Random rnd = new System.Random();
            //requiredCollect = rnd.Next(2, 10);
            requiredCollect = BalancedValues.GetCollectQuestValues(levelModel.CurrentLevel);
            rewardCount = GetCollectQuestReward(requiredCollect);
            trashTypeToCollect = RandomTrashType();
            PlayerPrefs.SetString("CollectQuestTrashType", trashTypeToCollect.ToString());
            PlayerPrefs.SetInt("CollectQuestRequiredTask", requiredCollect);
            //isInitialized = true;
            PlayerPrefs.SetInt("CollectQuestIsInitialized", 1);
        }

        if (PlayerPrefs.GetInt("CollectQuestRewardTaked") == 1)
            isCollected = true;
        if (PlayerPrefs.GetInt("CollectQuestCompleted") == 1)
            isCompleted = true;

        if (isCollected && isCompleted)
        {
            this.gameObject.SetActive(false);
            goalManager.CheckForCompleted();
        }
    }

    TrashMaterials RandomTrashType()
    {
        Array values = Enum.GetValues(typeof(TrashMaterials));
        System.Random random = new System.Random();
        int values1 = 0;
        for(int i = 0; i < factory.Length; i++)
        {
            if (factory[i].factoryData.level > 1 && factory[i].factoryData.id == "foodFactory1")
                values1 = 1;
            if (factory[i].factoryData.level > 1 && factory[i].factoryData.id == "paperFactory1")
                values1 = 2;
            if (factory[i].factoryData.level > 1 && factory[i].factoryData.id == "plasticFactory1")
                values1 = 3;
            if (factory[i].factoryData.level > 1 && factory[i].factoryData.id == "glassFactory1")
                values1 = 4;
        }
        TrashMaterials randomBar = (TrashMaterials)values.GetValue(random.Next(values1));
        Debug.Log("CURRENT FACTORY VALUE " + values1);
        return randomBar;
    }

    public static int GetCollectQuestReward(int _needToCollect)
    {
        int[] upValues = { 0, 0, 0, 0, 0, 250, 0, 500, 0, 0, 750, 0, 0, 1000, 0, 1250 };

        return upValues[_needToCollect];
    }

    public override void CheckToComplete()
    {
        this.gameObject.SetActive(false);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.D))
        {
            this.collectedTrash++;
            SetText();
        }
        //SetText();
        PlayerPrefs.SetInt("CollectTrashQuest", collectedTrash);
    }

    public void Increment()
    {
        collectedTrash = PlayerPrefs.GetInt("CollectTrashQuest");
        collectedTrash++;
        PlayerPrefs.SetInt("CollectTrashQuest", collectedTrash);
    }

    public void CollectReward()
    {
        softValueModel.AddValue(rewardCount);
        this.gameObject.SetActive(false);
        isCollected = true;
        PlayerPrefs.SetInt("CollectQuestRewardTaked", 1);
        goalManager.CheckForCompleted();
        //GenerateNextQuest();
    }

    public override bool IsAchieved()
    {
        return (collectedTrash >= requiredCollect);
    }

    public override void Complete()
    {
        btnToCollect.interactable = true;
        isCompleted = true;
        PlayerPrefs.SetInt("CollectQuestCompleted", 1);
    }

    public override bool IsCompleted()
    {
        return isCompleted;
    }

    public override bool IsTakeReward()
    {
        return isCollected;
    }

    public override void SetText()
    {
        title.text = "Сбор мусора";

        string trashTypeText = String.Empty;

        if (trashTypeToCollect == TrashMaterials.FOOD)
            trashTypeText = "органического";
        else if (trashTypeToCollect == TrashMaterials.GLASS)
            trashTypeText = "стеклянного";
        else if (trashTypeToCollect == TrashMaterials.PLASTIC)
            trashTypeText = "пластикового";
        else if (trashTypeToCollect == TrashMaterials.PAPER)
            trashTypeText = "бумажного";

        description.text = string.Format("Собрать {0} единиц(ы) {1} мусора", requiredCollect, trashTypeText);
        score.text = string.Format("{0}/{1}", collectedTrash, requiredCollect);
        rewardCount = GetCollectQuestReward(requiredCollect);
        reward.text = rewardCount.ToString();
    }

    public override void GenerateNextQuest()
    {
        Debug.Log("GO TO THE NEXT QUEST");
    }

    public override void ResetQuest()
    {
        PlayerPrefs.DeleteKey("CollectableTrashes");
        PlayerPrefs.DeleteKey("CollectQuestIsInitialized");
        PlayerPrefs.DeleteKey("CollectQuestRequiredTask");
        PlayerPrefs.DeleteKey("CollectTrashQuest");
        PlayerPrefs.DeleteKey("CollectQuestTrashType");
        PlayerPrefs.DeleteKey("CollectQuestRewardTaked");
        PlayerPrefs.DeleteKey("CollectQuestCompleted");
    }

    private void OnDisable()
    {
        PlayerPrefs.SetInt("CollectTrashQuest", collectedTrash);
    }
}
