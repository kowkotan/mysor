﻿using System;
using System.Collections.Generic;

public class AdPlacementEnum
{
    Dictionary<string, string> ads = new Dictionary<string, string>();
    public AdPlacementEnum()
    {
        ads.Add("rewarded_video", "REWARDED VIDEO");
    }

    public string GetPlacment(string _key)
    {
        if (ads.ContainsKey(_key))
        {
            return ads[_key];
        }
        return "unknown";
    }
}
