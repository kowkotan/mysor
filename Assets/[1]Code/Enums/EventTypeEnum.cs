using System;
public enum EVENT_TYPE 
{
    GAME_STARTED,
    EXP_GROW,
    EXP_FALL,
    LEVEL_READY_UP,
    LEVEL_UP,
    UNLOAD_LEVEL,
    LEVEL_LOADED,
    LOADING_FINISHED,
};
