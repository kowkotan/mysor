﻿//using UnityEngine;
//using Adic;
//using Framework.Interfaces;
//using Framework.Managers.Event;
//using Framework.Managers.States;
//using System;
//using Framework.Managers.Ads;
////using Facebook.Unity;
////using GameAnalyticsSDK;

//public class GameResultController : IEventListener,IDisposable
//{

//    [Inject] EventManager eventManager;
//    [Inject] GameConfig gameConfig;
//    [Inject] StateManager stateManager;
//    [Inject] LevelModel levelModel;

//    [Inject] GameWinPopupController gameWinPopup;

//    [Inject] GameWinPopupController gameEnd;
//    [Inject] GameLoosePopupController gameLoose;
//    [Inject] LevelMapController levelMapController;
//    [Inject] PreGameMenuController preGameMenuController;
//    [Inject] BaseAdSystem baseAdSystem;
//    [Inject] IVibrationSystem vibrationSystem;



//    int winCounter;

//    [Inject]
//    public void Init()
//    {
//        eventManager.AddListener(EVENT_TYPE.GAME_WIN, this);
//        eventManager.AddListener(EVENT_TYPE.GAME_LOOSE, this);
       
//    }

//    void LevelWin()
//    {
//        winCounter++;
//        vibrationSystem.MediumVibrate();

//       // GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Level " + levelModel.CurrentLevel.ToString());


//        if (winCounter % 3 == 0)
//        {
//            baseAdSystem.ShowInterstetial((bool isLoaded) =>
//            {
//                if (isLoaded)
//                {
//                    //FB.LogAppEvent("InterstetialAdShown");
//                    //GameAnalytics.NewDesignEvent("InterstetialAdShown");

//                }
//            });
//        }


//        stateManager.LoadState(gameWinPopup);
//    }


//    void LevelLoose()
//    {
//        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, "Level " + levelModel.CurrentLevel.ToString());
//        vibrationSystem.MediumVibrate();

//        stateManager.LoadState(gameLoose);
//    }

//    public void OnEvent(EVENT_TYPE Event_Type, Component Sender, UnityEngine.Object Param = null)
//    {
//        switch ( Event_Type )
//        {
//            case EVENT_TYPE.GAME_WIN:
//                LevelWin();
//                break;
//            case EVENT_TYPE.GAME_LOOSE:
//                LevelLoose();
//                break;
//        }

//    }

//    public void Dispose()
//    {
//        eventManager = null;
//        gameConfig = null;
//        stateManager = null;
//        gameWinPopup = null;
//        gameEnd = null;
//        gameLoose = null;
//        levelModel = null;
//        levelMapController = null;
//        preGameMenuController = null;

//        baseAdSystem = null;
//        vibrationSystem = null;
//    }

//}
