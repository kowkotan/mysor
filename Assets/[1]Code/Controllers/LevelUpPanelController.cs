﻿using UnityEngine;
using Adic;
using Framework.Interfaces;
using Framework.Managers.Event;
using System;
using System.Collections.Generic;

public class LevelUpPanelController : MonoBehaviour, IEventListener,IDisposable
{

    [Inject] EventManager eventManager;

    [Inject] LevelModel levelModel;

    [Inject] SoftValueModel softValueModel;
    [Inject] HardValueModel hardValueModel;
    [Inject] EnergyModel energyModel;
    [Inject] IFactoryLocalDatabase factoryDB;

    GameObject levelUpWin;

    int softValueReward;
    int hardValueReward;

    int countOfNewLevels;

    LevelUpWindowView levelUpWindowView;


    void Start()
    {
        this.Inject();
        eventManager.AddListener(EVENT_TYPE.LEVEL_UP, this);
    }

    public void OnEvent(EVENT_TYPE Event_Type, Component Sender, System.Object Param = null)
    {
        countOfNewLevels = (int)Param;
        Show();
    }

    public void Show( )
    {
        string pathToResource = "GlobalWindows/LevelUpWindowUI";
        levelUpWin = (GameObject) Instantiate(Resources.Load(pathToResource));
        levelUpWindowView = levelUpWin.GetComponent<LevelUpWindowView>();

        int newLevel = levelModel.CurrentLevel;
        softValueReward = BalancedValues.GetUserLevelUpSoftValueReward(newLevel) * countOfNewLevels;
        hardValueReward = BalancedValues.GetUserLevelUpHardValueReward(newLevel) * countOfNewLevels;

        softValueModel.AddValue(softValueReward);
        hardValueModel.AddValue(hardValueReward);
        energyModel.AddEnergy(energyModel.MaxEnergy-energyModel.Energy);

        levelUpWindowView.SetLevelTxt(newLevel);
        levelUpWindowView.SetSoftRewardValue(softValueReward);
        levelUpWindowView.SetHardRewardValue(hardValueReward);
        Dictionary<string, Sprite> items = new Dictionary<string, Sprite>();
        List<FactoryDBData> factoryList = factoryDB.GetUnlockedFactoryForLevel(levelModel.CurrentLevel);
        foreach(var factory in factoryList)
        {
            items.Add(factory.title, factory.factoryIcon);
        }
        levelUpWindowView.SetUnlockedItems(items);

        levelUpWindowView.OnRewardBtnClicked += Hide;


    }

    public void Hide()
    {
        levelUpWindowView.OnRewardBtnClicked -= Hide;


        levelUpWin.gameObject.SetActive(false);

        Destroy(levelUpWin.gameObject,1);
    }

    public void Dispose()
    {


        eventManager = null;
        levelModel = null;
        softValueModel = null;
        hardValueModel = null;
    }

}
