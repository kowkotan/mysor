﻿using System;
using Adic;
using Framework.Interfaces;
using Framework.Managers.Event;
//using Facebook.Unity;
//using GameAnalyticsSDK;
using UnityEngine;

public class GameProcessUIController : IState,IDisposable,ITick
{
    [Inject] GameProcessView GameProcessView;

    [Inject] GameControlModel GameControlModel;
    [Inject] LevelModel levelModel;
    [Inject] PreGameMenuController PreGameMenuController;
    [Inject] RewardedVideoButtonController rewardedVideoButtonController;
    //[Inject] WinCheck winCheck;
    [Inject] EventManager eventManager;
    [Inject] UpdateManager updateManager;


    [Inject]
    void Init()
    {
        GameProcessView.restartBtnClicked += GameProcessView_RestartBtnClicked;
        GameProcessView.menuBtnClicked += GameProcessView_MenuBtnClicked;
    }




    void GameProcessView_MenuBtnClicked()
    {
        Unload();
        PreGameMenuController.Load();
    }


    public void Load()
    {
        GameProcessView.Load();
        updateManager.AddTo(this);
        rewardedVideoButtonController.Load(GameProcessView.GethintRewardedBtnView());

        GameProcessView.SetLevelTextFieldText(levelModel.CurrentLevel.ToString());

    }

    void GameProcessView_RestartBtnClicked()
    {

        GameControlModel.Restart();

    }

    public void Unload()
    {
        updateManager.RemoveFrom(this);
        rewardedVideoButtonController.Unload();
        GameProcessView.Unload();  
    }

    public void Dispose()
    {
        GameProcessView.restartBtnClicked -= GameProcessView_RestartBtnClicked;
        GameProcessView.menuBtnClicked -= GameProcessView_MenuBtnClicked;

        GameProcessView = null;
        GameControlModel = null;

        eventManager = null;
        PreGameMenuController = null;
        //        winCheck = null;
    }

    public void Tick()
    {
        rewardedVideoButtonController.Tick();
        if (rewardedVideoButtonController.OnRewardedVideoFinishedSucces)
        {

//            FB.LogAppEvent("RewardVideoAdShown");
//            GameAnalytics.NewDesignEvent("RewardVideoAdShown");

            rewardedVideoButtonController.OnRewardedVideoFinishedSucces = false;
            rewardedVideoButtonController.TickLate();

        }
    }
}
