﻿using System;
using Adic;
using Framework.Interfaces;
using UnityEngine;

public class LevelMapController : IDisposable
{
    [Inject] LevelMapView LevelMapView;
    [Inject] LevelModel levelModel;

    IState prevState;

    [Inject]
    void Init()
    {
        LevelMapView.backBtnClicked += LevelMapView_BackBtnClicked;
    }

    public void Load(IState currentState)
    {
        prevState = currentState;
        LevelMapView.Load();


//        LevelMapView.InitLevelMap(levelModel.MaxLevel, levelModel.MaxOpenedLevel,levelModel.LastLevelCompleted);
    }

    void LevelMapView_BackBtnClicked()
    {
        Unload();
        LoadPrevState();
    }


    void LoadPrevState()
    {
        prevState.Load();
    }

    public void Unload()
    {

        LevelMapView.Unload();
    }

    public void Dispose()
    {
        LevelMapView.backBtnClicked -= LevelMapView_BackBtnClicked;
        LevelMapView = null;
        levelModel = null;
    }
}
