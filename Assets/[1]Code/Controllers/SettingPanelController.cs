using System;
using Adic;
using Framework.Interfaces;
using UnityEngine;

public class SettingPanelController : IDisposable, ITick, ITickLate
{
    [Inject] SettingPanelView  settingPanelView;
    [Inject] VibroSwitcherController VibrationSwitcherController;
    [Inject] UpdateManager updateManager;



    public void Load()
    {
        settingPanelView.Load();
        updateManager.AddTo(this);

        VibrationSwitcherController.Load(settingPanelView.GetVibroBtnView(),updateManager);

    }


    public void UnLoad()
    {
        updateManager.RemoveFrom(this);
        VibrationSwitcherController.Unload();
        settingPanelView.Unload();
    }

   
    public void Tick()
    {
        if(settingPanelView.onCloseBtnPressed)
        {
            settingPanelView.Unload();
        }
    }

    public void TickLate()
    {
        settingPanelView.TickLate();
    }

    public void Dispose()
    {
        updateManager = null;
        settingPanelView = null;
        VibrationSwitcherController = null;


    }
}
