﻿using System;
using Adic;
using Framework.Interfaces;
using UnityEngine;

public class PreGameMenuController : IState,IDisposable
{
    [Inject] PreGameMenuView PreGameMenuView;
    [Inject] SettingPanelController SettingPanelController;
    [Inject] LevelMapController LevelMapController;


    [Inject]
    void Init()
    {
        PreGameMenuView.playBtnClicked += PreGameMenuView_PlayBtnClicked;
    }


    public void Load()
    {
        SettingPanelController.Load();
        PreGameMenuView.Load();


    }

    void PreGameMenuView_PlayBtnClicked()
    {
        LevelMapController.Load(this);
        Unload(); 
    }


    public void Unload()
    {

        SettingPanelController.UnLoad();
        PreGameMenuView.Unload();
    }

    public void Dispose()
    {
        PreGameMenuView.playBtnClicked -= PreGameMenuView_PlayBtnClicked;
        PreGameMenuView = null;
        LevelMapController = null;
    }
}
