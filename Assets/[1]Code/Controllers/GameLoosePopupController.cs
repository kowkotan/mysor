﻿using System;
using Adic;
using Framework.Interfaces;
using Framework.Managers.Ads;
using UnityEngine;

public class GameLoosePopupController : IState,IDisposable
{
    [Inject] GameLoosePopupView GameLoosePopupView;
    [Inject] GameControlModel GameControlModel;



    [Inject]
    void Init()
    {
        GameLoosePopupView.restartBtnClicked += GameProcessView_RestartBtnClicked;
    }


    public void Load()
    {
        GameLoosePopupView.Load();

    }

    void GameProcessView_RestartBtnClicked()
    {
        GameControlModel.Restart();
        Unload();
    }

    public void Unload()
    {

        GameLoosePopupView.Unload();
    }

    public void Dispose()
    {

        GameLoosePopupView.restartBtnClicked -= GameProcessView_RestartBtnClicked;
        GameLoosePopupView = null;
        GameControlModel = null;
    }
}
