﻿using System;
using Adic;
using Framework.Interfaces;

public class GameWinPopupController: IState,IDisposable
{
    [Inject] GameWinPopupView GameWinPopupView;
    [Inject] ICommandDispatcher commandDispatcher;
    [Inject] LevelModel levelModel;


    [Inject]
    void Init()
    {
        GameWinPopupView.nextLevelBtnClicked += GameWinPopupView_NextLevelBtnClicked;
    }

    public void Load()
    {
        GameWinPopupView.Load();

    }

    void GameWinPopupView_NextLevelBtnClicked()
    {
//        commandDispatcher.Dispatch<LoadLevelCommand>(levelModel.CurrentLevel+1);
        Unload();
    }

    public void Unload()
    {

        GameWinPopupView.Unload();
    }

    private void OnDisable()
    {

    }

    public void Dispose()
    {
        GameWinPopupView.nextLevelBtnClicked -= GameWinPopupView_NextLevelBtnClicked;
        GameWinPopupView = null;
        levelModel = null;
        commandDispatcher = null;
    }
}
