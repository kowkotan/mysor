﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicShopController : MonoBehaviour
{
    public void OpenMagicShop()
    {
        string pathToResource = "GlobalWindows/MagicShopUI";
         Instantiate(Resources.Load(pathToResource));
    }
}
