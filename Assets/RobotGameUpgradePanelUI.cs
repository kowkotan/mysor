﻿using UnityEngine;
using Framework.Managers.States;
using Adic;
using UnityEngine.UI;

public class RobotGameUpgradePanelUI : MonoBehaviour
{
    [SerializeField] EnergyGate energyGate;

    [SerializeField] Button OnCloseBtnClicked;
    [Inject] StateManager stateManager;

    void Start()
    {
        this.Inject();
        OnCloseBtnClicked.onClick.AddListener(CloseButtonClicked);
    }

    public void ClosePanel()
    {
        if(energyGate.Unlock())
        {
            gameObject.SetActive(false);
        }
    }

    void CloseButtonClicked()
    {
       stateManager.ChangeScene("FactoryMap");
    }

    private void OnDestroy()
    {
        OnCloseBtnClicked.onClick.RemoveListener(CloseButtonClicked);
    }
}
