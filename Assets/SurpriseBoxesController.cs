﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurpriseBoxesController : MonoBehaviour
{
    [SerializeField] SurBoxUI[] surBoxes;

    RobotGameResult robotGameResult;
    void OnEnable()
    {
        robotGameResult = FindObjectOfType<RobotGameResult>();

        for(int i = 0; i < robotGameResult.boxFounded; i++)
        {
            surBoxes[i].UnlockBox();
        }
    }
}
