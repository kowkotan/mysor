﻿using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct MShop_CellData
{
    public string title;
    public MShop_ItemEnum itemType;
    public Sprite img;
    public int stackSize;
    public int defPrice;
}

public enum MShop_ItemEnum
{
        SOFT_VALUE,
        HARD_VALUE,
        ENERGY,
        DETALS,
        Diam10
}

public abstract class MShop_CellBase : MonoBehaviour
{
    [SerializeField]
    Text titleTxt;
    [SerializeField]
    Image itemIcon;
    [SerializeField]
    Text stackSizeTxt;
    [SerializeField]
    Text priceTxt;

    [SerializeField]
    Button buyBtn;

    [SerializeField]
    MShop_CellData mShopCellData;

    protected int DefPrice => mShopCellData.defPrice;
    protected int StackSize => mShopCellData.stackSize;
    protected MShop_ItemEnum ItemType => mShopCellData.itemType;

    void Awake()
    {
        Init(mShopCellData);

        if (buyBtn != null)
        {
            buyBtn.onClick.AddListener(HandleUnityAction);
        }
    }

    void HandleUnityAction()
    {
        OnBuy();
    }


    void Init(MShop_CellData _shopCellData)
    {
        titleTxt.text = _shopCellData.title;
        // itemIcon.sprite = _shopCellData.img;
        if (stackSizeTxt != null)
        {
            stackSizeTxt.text = _shopCellData.stackSize.ToString();
        }
        if (priceTxt != null)
        {
            priceTxt.text = GetPrice().ToString();
        }
    }

    public void SetupTitleIcon(string title, Sprite icon)
    {
        titleTxt.text = title;
        itemIcon.sprite = icon;
    }

   public abstract int GetPrice();
    public abstract void OnBuy();

    private void OnDestroy()
    {
        buyBtn.onClick.RemoveListener(HandleUnityAction);
    }
}
