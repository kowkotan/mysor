﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;
using Framework.Views;

public class RobotGameTutorialController : MonoBehaviour
{

    [SerializeField] Button upWindowResumeBtn;
    [SerializeField] Button looseWindowSortingBtn;

    [SerializeField] GameObject robotControllerUIHint;

    [Inject] RobotMinigameSystem robotMinigameSystem;

    [SerializeField] GameObject robotGameUpgradePanelUI;

    [SerializeField] Image maskedObject;

    [SerializeField] Image fuelBar;
    
    [Inject] RobotBarController robotBarController;
    [SerializeField] GameObject joystickObject;
    [SerializeField] GameObject joystickBack;
    GameObject robotObject;

    void Start()
    {
        this.Inject();
        if (PlayerPrefs.GetInt("robotGameTutorialPart1Completed") == 1) return;
        TutorialLoader.instance.Load("rg3", true);
        maskedObject.raycastTarget = true;

        //upWindowResumeBtn.interactable = false; 
        robotControllerUIHint.SetActive(true);

        robotMinigameSystem.OnMinigameLoose += RobotMinigameSystem_OnMinigameLoose;

        if (PlayerPrefs.GetInt("robotGameTutorialMoveComplete") == 1)
        {
            TutorialEvents.OnTutorialComplete.AddListener(Rg2Comp);
            robotControllerUIHint.SetActive(false);
        }
    }

    private void Update() {
        if (PlayerPrefs.GetInt("robotGameTutorialPart1Completed") == 1 || PlayerPrefs.GetInt("robotGameTutorFuelComplete") == 1) return;

        if(PlayerPrefs.GetInt("robotGameTutorialMoveComplete") == 1)
        {
            if(fuelBar.fillAmount <= 0.8f)
            {
                robotControllerUIHint.SetActive(false);

                robotObject = GameObject.FindGameObjectWithTag("Player");
                robotObject.GetComponent<Robot>().enabled = false;
                joystickObject.GetComponent<FloatingJoystick>().enabled = false;
                joystickBack.SetActive(false);
                PlayerPrefs.SetInt("RobotFuelStart", 1);

                TutorialLoader.instance.Load("rg6", true);
                TutorialEvents.OnTutorialComplete.AddListener(Rg6Comp);
                PlayerPrefs.SetInt("robotGameTutorFuelComplete", 1);
            }
        }
    }

    void RobotMinigameSystem_OnMinigameLoose()
    {
        TutorialLoader.instance.Load("rg4", true);
        TutorialEvents.OnTutorialComplete.AddListener(Rg4Comp);
        maskedObject.raycastTarget = false;
        PlayerPrefs.SetInt("robotGameTutorialPart1Completed", 1);
        robotMinigameSystem.OnMinigameLoose -= RobotMinigameSystem_OnMinigameLoose;
        looseWindowSortingBtn.interactable = false;

    }

    void Rg6Comp()
    {
        PlayerPrefs.SetInt("RobotFuelStart", 0);
        joystickObject.GetComponent<FloatingJoystick>().enabled = true;

        TutorialEvents.OnTutorialComplete.RemoveListener(Rg4Comp);
        //robotBarController.On50PercentFuel -= RobotBarController_On50PercentFuel;
        TutorialEvents.OnTutorialComplete.AddListener(Rg2Comp);
    }

    void Rg1Comp()
    {
        TutorialEvents.OnTutorialComplete.RemoveListener(Rg1Comp);

        upWindowResumeBtn.interactable = true;

        TutorialLoader.instance.Load("rg2", true);

        TutorialEvents.OnTutorialComplete.AddListener(Rg2Comp);


    }

    void Rg2Comp()
    {
        robotControllerUIHint.SetActive(false);
        TutorialEvents.OnTutorialComplete.RemoveListener(Rg2Comp);

        //TutorialLoader.instance.Load("rg3", true);
    }

    void Rg3Comp()
    {
        TutorialEvents.OnTutorialComplete.RemoveListener(Rg3Comp);
        
    }

    void Rg4Comp()
    {
        TutorialEvents.OnTutorialComplete.RemoveListener(Rg4Comp);

        TutorialLoader.instance.Load("rg5", true, upWindowResumeBtn);

        looseWindowSortingBtn.interactable = true;

        TutorialEvents.OnTutorialComplete.AddListener(Rg5Comp);
    }

    void Rg5Comp()
    {
        TutorialEvents.OnTutorialComplete.RemoveListener(Rg2Comp);
        //PlayerPrefs.SetInt("robotGameTutorialPart1Completed", 1);
    }
}
