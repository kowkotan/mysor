﻿using UnityEngine;
using UnityEngine.UI;

public class MoveShopScroll : MonoBehaviour
{
    [SerializeField] Scrollbar shopScrollbar;

    public void MoveContentPane(float value)
    {
        shopScrollbar.value = value;
        this.GetComponent<AudioSource>().Play();
    }
}
