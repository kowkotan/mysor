﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TouchCameraScroll : MonoBehaviour
{
    Camera mainCamera;
    [SerializeField]
    GameObject mainCanvas;

    [SerializeField]
    SpriteRenderer boundsMap;

    Vector3 min;
    Vector3 max;

    Vector2 prevMousePos = new Vector2();
    bool clicked = false;

    void Start()
    {
        mainCamera = GetComponent<Camera>();
        CalculateBounds();
    }

    public void CalculateBounds()
    {
        if (boundsMap == null) return;
        Bounds bounds = Camera2DBounds();
        min = bounds.max + boundsMap.bounds.min;
        max = bounds.min + boundsMap.bounds.max;
    }

    Bounds Camera2DBounds()
    {
        float height = mainCamera.orthographicSize * 2;
        return new Bounds(Vector3.zero, new Vector3(height * mainCamera.aspect, height, 0));
    }

    public static bool IsPointerOverGameObject()
    {
        //check mouse
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return true;
        }

        //check touch
        if (Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Began)
        {
            if (EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId))
                return true;
        }

        return false;
    }

    public bool IsOverUI()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            if (EventSystem.current.currentSelectedGameObject != null)
            {
                Transform trans = EventSystem.current.currentSelectedGameObject.transform;
                while (true)
                {
                    if (trans == null)
                    {
                        return true;
                    }
                    if (trans.name == "[LEVELS]")
                    {
                        return false;
                    }
                    trans = trans.parent;
                }
            }
        }
        GraphicRaycaster raycaster = mainCanvas.GetComponent<GraphicRaycaster>();
        if (raycaster != null)
        {
            PointerEventData pointer = new PointerEventData(EventSystem.current);
            pointer.position = Input.mousePosition;
            List<RaycastResult> results = new List<RaycastResult>();
            raycaster.Raycast(pointer, results);
            if (results.Count > 0)
            {
                return true;
            }
        }
        return false;
    }

    void Update()
    {
        if (TutorialController.TUTORIAL_ACTIVE || IsOverUI())
        {
            return;
        }
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (Input.GetMouseButtonDown(0))
        {
            prevMousePos = mousePos;
            clicked = true;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            clicked = false;
        }

        if (clicked)
        {
            Vector3 pos = transform.position;
            pos += (Vector3)(prevMousePos - (Vector2)mousePos);
            pos.x = Mathf.Clamp(pos.x, min.x, max.x);
            pos.y = Mathf.Clamp(pos.y, min.y, max.y);
            transform.position = pos;
        }
    }
}
