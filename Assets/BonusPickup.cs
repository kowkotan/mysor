﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum BONUST_TYPE { MAGNET = 0, SPEEDUP = 1, FUEL = 2 }

public class BonusPickup : MonoBehaviour,IPickup
{
    [SerializeField]
    BONUST_TYPE bonusType;

    Collider2D collider2d;
    bool isPickuped;

    void Start()
    {
        collider2d = GetComponent<Collider2D>();
    }

    public void Pickup()
    {
        if (isPickuped) return;

        switch (bonusType)
        {
            case BONUST_TYPE.FUEL:
                FindObjectOfType<AddFuelEffect>().LaunchEffect();
                break;
            case BONUST_TYPE.MAGNET:
                FindObjectOfType<MagnetEffect>().LaunchEffect();
                break;
            case BONUST_TYPE.SPEEDUP:
                FindObjectOfType<SpeedUpEffect>().LaunchEffect();
                break;
        }

        isPickuped = true;
        collider2d.enabled = false;
    }

}
