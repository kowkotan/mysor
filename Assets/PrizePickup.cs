﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrizePickup : MonoBehaviour,  IPickup
{
    public bool isPickuped;
    BoxCollider2D boxCollider;

    void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
    }

    public void Pickup()
    {
        isPickuped = true;
        boxCollider.enabled = false;
        //-gameObject.SetActive(false);
    }
}
