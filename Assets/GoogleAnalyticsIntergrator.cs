﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Adic;
using Framework.Interfaces;
using Framework.Managers.Event;

public class GoogleAnalyticsIntergrator : MonoBehaviour, IEventListener
{
    [Inject] EventManager eventManager;

    public GoogleAnalyticsV4 googleAnalytics;

    private void Start() {
        this.Inject();

        //googleAnalytics.StartSession();
        //googleAnalytics.LogScreen("FactoryMap");
        //Debug.Log("Google analytics sent");

        eventManager.AddListener(EVENT_TYPE.GAME_STARTED, this);
    }

    public void OnEvent(EVENT_TYPE Event_Type, Component Sender, object Param = null)
    {
        //TODO
        StartGameAnalyticHook();
    }

    void StartGameAnalyticHook()
    {
        //Firebase.Analytics.FirebaseAnalytics.LogEvent("Enter in game");
        googleAnalytics.StartSession();
        googleAnalytics.LogScreen("FactoryMap");
        Debug.Log("Google analytics sent");
    }
}
