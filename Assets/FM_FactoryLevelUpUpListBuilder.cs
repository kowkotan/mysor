﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FM_FactoryLevelUpUpListBuilder : MonoBehaviour
{
    [SerializeField] Transform upgradesContainer;


    [SerializeField] GameObject productStorageLimitUpPrefab;

    [SerializeField] GameObject newProductLineUpPrefab;

    [SerializeField] GameObject productionTimeUpgradePrefab;


    FactoryData factoryData;
    FM_FactoryPanelController factoryPanelController;


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BuildFactoryList(FM_Factory _factory)
    {
        factoryData = _factory.GetFactoryData();
        factoryPanelController = _factory.GetComponent<FM_FactoryPanelController>();
        ClearUpgradeList();


        if (factoryPanelController.productionLine2.reqLevel == factoryData.level + 1)
        {
            LevelUpgradedItem newCell1 = Instantiate(newProductLineUpPrefab, upgradesContainer).GetComponent<LevelUpgradedItem>();
            newCell1.Fill(factoryPanelController.productionLineView2.GetProductSprite());
        }

     
        if (factoryPanelController.productionLine3.reqLevel == factoryData.level + 1)
        {
            LevelUpgradedItem newCell2 = Instantiate(newProductLineUpPrefab, upgradesContainer).GetComponent<LevelUpgradedItem>();
            newCell2.Fill(factoryPanelController.productionLineView3.GetProductSprite());
        }

        LevelUpgradedItem newCell3 = Instantiate(productStorageLimitUpPrefab, upgradesContainer).GetComponent<LevelUpgradedItem>();
        int currentMaxOfProduct = _factory.MaxNumberOfProduct;

        int nextMaxOfProduct = BalancedValues.GetFactoryMaxProductLimit(factoryData.level + 1, 1);// TEMP TODO: для каждой линии свое

        //int delta = nextMaxOfProduct - currentMaxOfProduct;
        newCell3.Fill(+nextMaxOfProduct);

        float currentProductCreateTime = factoryPanelController.productionLine1.timeToNextProduct;

        float nextProductCreateTime = BalancedValues.GetFactoryCreatingNewProductTime(factoryData.level + 1, factoryPanelController.productionLine1.numberOfPruductionLine); // TEMP TODO: для каждой линии свое отобразить

        LevelUpgradedItem newCell4 = Instantiate(productionTimeUpgradePrefab, upgradesContainer).GetComponent<LevelUpgradedItem>();
        productionTimeUpgradePrefab.GetComponent<LevelUpgradedItem>().Fill( -Mathf.FloorToInt(currentProductCreateTime - nextProductCreateTime) );
    }

    void ClearUpgradeList()
    {
        foreach (Transform child in upgradesContainer)
        {
            Destroy(child.gameObject);
        }
    }
}
