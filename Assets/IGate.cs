﻿public interface IGate
{
    bool IsUnlocked();
     void TryUnlock();
}