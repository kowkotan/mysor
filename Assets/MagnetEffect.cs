﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework.Utility;

public class MagnetEffect : CountdownEffect 
{
    [SerializeField]  List<IMagnetEffect> countdownEffects;

    [SerializeField] float activeTime = 12;


    public void LaunchEffect()
    {
        countdownEffects = Extensions.FindOnCurrentScene<IMagnetEffect>();

        StartEffect(activeTime);
    }

   public void StartMagnetEffect()
    {

        for (int i = 0; i < countdownEffects.Count; i++)
        {
            countdownEffects[i].StartMagnetEffect();
        }
    }

    public void StopMagnetEffect()
    {
        for (int i = 0; i < countdownEffects.Count; i++)
        {
            countdownEffects[i].StopMagnetEffect();
        }
    }

    public override void ApplyEffect()
    {
        StartMagnetEffect();
    }

    public override void ResetEffect()
    {
        StopMagnetEffect();
    }


}
