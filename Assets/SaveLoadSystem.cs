﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SaveLoadSystem : ISaveLoadSystem
{
    public bool IsSaved(string _key)
    {
        return PlayerPrefs.HasKey(_key);
    }

    public void SaveStruct(string _key, ValueType valueType)
    {
        PlayerPrefs.SetString(_key, JsonUtility.ToJson(valueType));
    }

    public void SaveClass(string _key, object classType)
    {
        PlayerPrefs.SetString(_key, JsonUtility.ToJson(classType));
    }

    public T LoadClass<T>(string _key)
    {
        return LoadStruct<T>(_key);
    }

    public T LoadStruct<T>( string _key)
    {
        if (IsSaved(_key))
        {
            return JsonUtility.FromJson<T>(PlayerPrefs.GetString(_key));
        }
        throw new ArgumentNullException(string.Format("Saved structure via key {0} was not found ",_key));
    }

    public  string GetString(string _key, string _defValue = "")
    {
        return PlayerPrefs.GetString(_key, _defValue);
    }

    public  void SetString(string _key, string _value)
    {
        PlayerPrefs.SetString(_key, _value);
    }

    public  int GetInt(string _key, int _defValue = 0)
    {
        return int.Parse(GetString(_key, _defValue.ToString()));
    }

    public  void SetInt(string _key, int _value)
    {
        SetString(_key, _value.ToString());
    }

    public  float GetFloat(string _key, float _defValue = 0)
    {
        return float.Parse(GetString(_key, _defValue.ToString()));
    }

    public  void SetFloat(string _key, float _value)
    {
        SetString(_key, _value.ToString());
    }


}

