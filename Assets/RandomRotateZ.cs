﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotateZ : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        transform.Rotate(  new Vector3(transform.rotation.x, transform.rotation.y, Random.Range(-360, 360)));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
