﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework.Managers.States;
using Adic;

public class LevelsMenu : MonoBehaviour
{

    [Inject] StateManager stateManager; 

    private void Start() {
        this.Inject();
    }

    public void Show()
    {
        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    public void LoadLevels(string levelName)
    {
        if(levelName == "DesertMap")
            PlayerPrefs.SetString("PlayerLevelTest", "2");
        else PlayerPrefs.SetString("PlayerLevelTest", "1");
        stateManager.ChangeScene(levelName);
    }
}
