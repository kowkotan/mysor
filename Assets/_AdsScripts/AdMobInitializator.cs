﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class AdMobInitializator : MonoBehaviour
{
    private void Start() {
        DontDestroyOnLoad(gameObject);
        MobileAds.Initialize(initStatus => {});
    }
}
