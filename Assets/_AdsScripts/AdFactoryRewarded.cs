﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class AdFactoryRewarded : MonoBehaviour
{
    public static AdFactoryRewarded instance;

    private RewardBasedVideoAd rewardBasedVideoAd;

    [SerializeField] FM_Factory factory;

    private void Awake() {
        if(instance == null)
            instance = this;
        else Destroy(this);
    }

    void Start()
    {
        rewardBasedVideoAd = RewardBasedVideoAd.Instance;

        #if UNITY_ANDROID
            string appId = "ca-app-pub-6764297309816114~6497630016";
        #elif UNITY_IPHONE
            string appId = "ca-app-pub-6764297309816114~5518400896";
        #else
            string appId = "unexpected_platform";
        #endif

        MobileAds.SetiOSAppPauseOnBackground(true);

        MobileAds.Initialize(appId);

        RequestRewardedAd();

        rewardBasedVideoAd.OnAdLoaded += HandleRewardBasedVideoLoaded;
        // Called when an ad request failed to load.
        rewardBasedVideoAd.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        // Called when an ad is shown.
        rewardBasedVideoAd.OnAdOpening += HandleRewardBasedVideoOpened;
        // Called when the ad starts to play.
        rewardBasedVideoAd.OnAdStarted += HandleRewardBasedVideoStarted;
        // Called when the user should be rewarded for watching a video.
        rewardBasedVideoAd.OnAdRewarded += HandleRewardBasedVideoRewarded;
        // Called when the ad is closed.
        rewardBasedVideoAd.OnAdClosed += HandleRewardBasedVideoClosed;
        // Called when the ad click caused the user to leave the application.
        rewardBasedVideoAd.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;

    }

    public void RequestRewardedAd()
    {
        #if UNITY_EDITOR
            string adUnitId = "unused";
        #elif UNITY_ANDROID
            string adUnitId = "ca-app-pub-6764297309816114/2389674674";
        #elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-6764297309816114/5288490124";
        #else
            string adUnitId = "unexpected_platform";
        #endif

        AdRequest request = new AdRequest.Builder().Build();
        
        rewardBasedVideoAd.LoadAd(request, adUnitId);
    }

    public void ShowRewardedAd()
    {
        if(rewardBasedVideoAd.IsLoaded())
            rewardBasedVideoAd.Show();
        else Debug.Log("REWARDED AD IS NOT READY");
    }

    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLoaded event received");
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardBasedVideoFailedToLoad event received with message: "
                             + args.Message);
    }

    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoOpened event received");
    }

    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoStarted event received");
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
        this.RequestRewardedAd();
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        MonoBehaviour.print(
            "HandleRewardBasedVideoRewarded event received for "
                        + amount.ToString() + " " + type);
        factory.SetSpeed();
        OnDestroy();
    }

    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");
    }

    private void OnDestroy() {
        rewardBasedVideoAd.OnAdLoaded -= HandleRewardBasedVideoLoaded;
        rewardBasedVideoAd.OnAdFailedToLoad -= HandleRewardBasedVideoFailedToLoad;
        rewardBasedVideoAd.OnAdOpening -= HandleRewardBasedVideoOpened;
        rewardBasedVideoAd.OnAdStarted -= HandleRewardBasedVideoStarted;
        rewardBasedVideoAd.OnAdRewarded -= HandleRewardBasedVideoRewarded;
        rewardBasedVideoAd.OnAdClosed -= HandleRewardBasedVideoClosed;
        rewardBasedVideoAd.OnAdLeavingApplication -= HandleRewardBasedVideoLeftApplication;
    }
}
