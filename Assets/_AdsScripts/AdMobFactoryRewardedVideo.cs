﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Adic;
using GoogleMobileAds.Api;

public class AdMobFactoryRewardedVideo : MonoBehaviour
{
    [SerializeField] FM_Factory factory;

    private RewardedAd rewardedAd;
    private float deltaTime = 0.0f;
    private static string outputMessage = string.Empty;

    public static string OutputMessage
    {
        set { outputMessage = value; }
    }

    public void Start()
    {
        #if UNITY_ANDROID
            string appId = "ca-app-pub-6764297309816114~6497630016";
        #elif UNITY_IPHONE
            string appId = "ca-app-pub-6764297309816114~5518400896";
        #else
            string appId = "unexpected_platform";
        #endif

        MobileAds.SetiOSAppPauseOnBackground(true);

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);

        this.CreateAndLoadRewardedAd();
    }

    public void Update()
    {
        // Calculate simple moving average for time to render screen. 0.1 factor used as smoothing
        // value.
        this.deltaTime += (Time.deltaTime - this.deltaTime) * 0.1f;
    }

    // Returns an ad request with custom ad targeting.
    // private AdRequest CreateAdRequest()
    // {
    //     return new AdRequest.Builder()
    //         .AddTestDevice(AdRequest.TestDeviceSimulator)
    //         .AddTestDevice("0123456789ABCDEF0123456789ABCDEF")
    //         .AddKeyword("game")
    //         .SetGender(Gender.Male)
    //         .SetBirthday(new DateTime(1985, 1, 1))
    //         .TagForChildDirectedTreatment(false)
    //         .AddExtra("color_bg", "9B30FF")
    //         .Build();
    // }

    public void CreateAndLoadRewardedAd()
    {
        #if UNITY_EDITOR
            string adUnitId = "unused";
        #elif UNITY_ANDROID
            string adUnitId = "ca-app-pub-6764297309816114/2389674674";
        #elif UNITY_IPHONE
            string adUnitId = "ca-app-pub-6764297309816114/5288490124";
        #else
            string adUnitId = "unexpected_platform";
        #endif
        // Create new rewarded ad instance.
        this.rewardedAd = new RewardedAd(adUnitId);

        // Called when an ad request has successfully loaded.
        this.rewardedAd.OnAdLoaded += HandleRewardedAdLoaded;
        // Called when an ad request failed to load.
        this.rewardedAd.OnAdFailedToLoad += HandleRewardedAdFailedToLoad;
        // Called when an ad is shown.
        this.rewardedAd.OnAdOpening += HandleRewardedAdOpening;
        // Called when an ad request failed to show.
        this.rewardedAd.OnAdFailedToShow += HandleRewardedAdFailedToShow;
        // Called when the user should be rewarded for interacting with the ad.
        this.rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
        // Called when the ad is closed.
        this.rewardedAd.OnAdClosed += HandleRewardedAdClosed;

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the rewarded ad with the request.
        this.rewardedAd.LoadAd(request);
    }

    public void ShowRewardedAd()
    {
        if (this.rewardedAd.IsLoaded())
        {
            this.rewardedAd.Show();
        }
        else
        {
            MonoBehaviour.print("Rewarded ad is not ready yet");
        }
    }

    #region RewardedAd callback handlers

    public void HandleRewardedAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdLoaded event received");
    }

    public void HandleRewardedAdFailedToLoad(object sender, AdErrorEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardedAdFailedToLoad event received with message: " + args.Message);
        this.CreateAndLoadRewardedAd();
    }

    public void HandleRewardedAdOpening(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardedAdOpening event received");
    }

    public void HandleRewardedAdFailedToShow(object sender, AdErrorEventArgs args)
    {
        MonoBehaviour.print(
            "HandleRewardedAdFailedToShow event received with message: " + args.Message);
        this.CreateAndLoadRewardedAd();
    }

    public void HandleRewardedAdClosed(object sender, EventArgs args)
    {
        this.CreateAndLoadRewardedAd();
    }

    public void HandleUserEarnedReward(object sender, Reward args)
    {
        factory.SetSpeed();
        this.OnDestroy();
        this.CreateAndLoadRewardedAd();
    }

    private void OnDestroy() {
        this.rewardedAd.OnAdLoaded -= HandleRewardedAdLoaded;
        this.rewardedAd.OnAdFailedToLoad -= HandleRewardedAdFailedToLoad;
        this.rewardedAd.OnAdOpening -= HandleRewardedAdOpening;
        this.rewardedAd.OnAdFailedToShow -= HandleRewardedAdFailedToShow;
        this.rewardedAd.OnUserEarnedReward -= HandleUserEarnedReward;
        this.rewardedAd.OnAdClosed -= HandleRewardedAdClosed;
    }

    #endregion
}
