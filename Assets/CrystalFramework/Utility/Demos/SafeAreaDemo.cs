﻿using UnityEngine;
using System;

namespace Crystal
{
    public class SafeAreaDemo : MonoBehaviour
    {
        //[SerializeField] KeyCode KeySafeArea = KeyCode.A;
        [SerializeField] bool isNeedToRectForIphoneX;
        SafeArea.SimDevice[] Sims;
        int SimIdx;

        void Awake ()
        {
            DontDestroyOnLoad(this);
            if (!Application.isEditor)
                Destroy (this);

            Sims = (SafeArea.SimDevice[])Enum.GetValues (typeof (SafeArea.SimDevice));

            if(SafeArea.Sim == SafeArea.SimDevice.iPhoneX)
                isNeedToRectForIphoneX = true;
            else isNeedToRectForIphoneX = false;
        }

        void Update ()
        {
            if (isNeedToRectForIphoneX)
                ToggleSafeArea (4);
            else ToggleSafeArea(0);
        }

        /// <summary>
        /// Toggle the safe area simulation device.
        /// </summary>
        void ToggleSafeArea (int index)
        {
            SimIdx = index;

            SafeArea.Sim = Sims[SimIdx];
            Debug.LogFormat ("Switched to sim device {0}", Sims[SimIdx]);
        }
    }
}
