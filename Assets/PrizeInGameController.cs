﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrizeInGameController : MonoBehaviour
{
    RobotGameResult robotGameResult;

    [SerializeField]
    GameObject[] prizesActiveStateGameObjectsUI;


    void Start()
    {
        robotGameResult = FindObjectOfType<RobotGameResult>();
        robotGameResult.boxFounded = 0;
    }

    public void PickupPrize()
    {
        robotGameResult.boxFounded++;
        prizesActiveStateGameObjectsUI[robotGameResult.boxFounded-1].SetActive(true);
    }


}
