﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;

public class MShop_ExchangeCell : MShop_CellBase
{
    [Inject] HardValueModel HardValueModel;
    [Inject] SoftValueModel SoftValueModel;
    [Inject] EnergyValueModel EnergyModel;

    [Inject] DetalsModel DetalsModel;

    [SerializeField] AudioSource audioSource;


    int Price => GetPrice();

    private void Start()
    {
        this.Inject();
    }

    public override int GetPrice()
    {
        return DefPrice;
    }

    public override void OnBuy()
    {
        if(HardValueModel.DecValue(Price))
        {
            GetBuyedItemValueModel().AddValue(StackSize);
            audioSource.Play();
        }

    }

    ValueModelBase GetBuyedItemValueModel()
    {
        switch(ItemType)
        {
            case MShop_ItemEnum.SOFT_VALUE:
                return SoftValueModel; 
            case MShop_ItemEnum.HARD_VALUE:
                return HardValueModel;
            case MShop_ItemEnum.ENERGY:
                return EnergyModel;
                break;
            case MShop_ItemEnum.DETALS:
                return DetalsModel;
        }
        throw new ArgumentOutOfRangeException();
    }
}
