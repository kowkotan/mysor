﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RG_Bird : MonoBehaviour
{
    [SerializeField] Animator animator;

    bool isFly;

    Vector3 flyVector;

    float speed = 4;

    void Fly()
    {

        if (isFly == false)
        {
            animator.SetBool("isFly", true);
            isFly = true;
            flyVector = GetRandomFlyVector();
            speed = Random.Range(5, 6);
            animator.speed += speed / 3.5f;
        }
    }

    private void Update()
    {
        if(isFly)
        {
            transform.position += flyVector * Time.deltaTime * speed;

            float angle = Mathf.Atan2(flyVector.y, flyVector.x) * Mathf.Rad2Deg;
            if (angle < 0f)
            {
                angle += 360f;
            }
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        }
    }

    Vector3 GetRandomFlyVector()
    {
        return new Vector3(Random.value, Random.value, 0);
    }


    private void OnTriggerEnter(Collider other)
    {
        Fly();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Fly();
    }
}
