﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FloatingJoystick : Joystick
{
    GameObject robot;

    protected override void Start()
    {
        base.Start();
        background.gameObject.SetActive(false);
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        
        if (PlayerPrefs.GetInt("robotGameTutorialPart1Completed") == 1)  
        {
            PlayerPrefs.SetInt("robotGameTutorialMoveComplete", 0);
        }
        else
        {
            PlayerPrefs.SetInt("robotGameTutorialMoveComplete", 1);
            Debug.Log(PlayerPrefs.GetInt("robotGameTutorialMoveComplete"));
        }

        if (PlayerPrefs.GetInt("robotGameTutorialPart1Completed") == 0 && PlayerPrefs.GetInt("RobotFuelStart") == 0)
        {
            robot = GameObject.FindGameObjectWithTag("Player");
            robot.GetComponent<Robot>().enabled = true;
        }



        background.anchoredPosition = ScreenPointToAnchoredPosition(eventData.position);
        background.gameObject.SetActive(true);
        base.OnPointerDown(eventData);
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        background.gameObject.SetActive(false);
        base.OnPointerUp(eventData);
    }
}