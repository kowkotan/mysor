﻿using Adic;
using Framework.Common;
using Framework.Controllers.Loader;
using Framework.Interfaces;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Framework.Managers.States
{
    public class StateManager: MonoBehaviour
    {
        [Inject] ISceneLoader loader;

        string currentSceneName = null;
        
        private  void Start()
        {
            this.Inject();
        }

        public void LoadState( IState state )
        {
            state.Load();
        }

        public bool AdditionalScenesIsLoaded()
        {
            return (SceneManager.sceneCount > 1);
        }

        public void UnloadCurrentScene()
        {
            Debug.Log("UNLOAD" + currentSceneName);
            SceneManager.UnloadSceneAsync(currentSceneName);
        }

        public void ReloadCurrentScene()
        {
            AddScene(currentSceneName);
        }

        public void AddScene(string sceneName, bool unloadCurrentScene = true)
        {
            Debug.Log("ADDSCENE" + sceneName);
            if (unloadCurrentScene == true && currentSceneName != null)
            {
                UnloadCurrentScene();

            }
                
            currentSceneName = sceneName;
            loader.StartLoad(sceneName, LoadSceneMode.Additive);

        }

        public void ChangeScene(string sceneName)
        {
            currentSceneName = sceneName;
            loader.StartLoad(sceneName, LoadSceneMode.Single); 
        }



    }
}
