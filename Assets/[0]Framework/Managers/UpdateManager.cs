﻿using System.Collections.Generic;
using Framework.Interfaces;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UpdateManager : MonoBehaviour
{
    private List<ITick> ticks = new List<ITick>();
    private List<ITickFixed> ticksFixes = new List<ITickFixed>();
    private List<ITickLate> ticksLate = new List<ITickLate>();
    private List<ITickSec> tickSecs = new List<ITickSec>();

    bool OnSceneChanged;

    float sec = 1;
    private void Start()
    {
        SceneManager.activeSceneChanged += SceneManager_ActiveSceneChanged;


    }

    void SceneManager_ActiveSceneChanged(Scene arg0, Scene arg1)
    {
        OnSceneChanged = true;
        SceneManager.activeSceneChanged -= SceneManager_ActiveSceneChanged;
    }


    public void AddTo(object updateble)
    {
      
        if (updateble is ITick)
            ticks.Add(updateble as ITick);

        if (updateble is ITickFixed)
            ticksFixes.Add(updateble as ITickFixed);

        if (updateble is ITickLate)
            ticksLate.Add(updateble as ITickLate);

        if (updateble is ITickSec)
            tickSecs.Add(updateble as ITickSec);



    }
    public void RemoveFrom(object updateble)
    {
    
        if (updateble is ITick)
           ticks.Remove(updateble as ITick);

        if (updateble is ITickFixed)
            ticksFixes.Remove(updateble as ITickFixed);

        if (updateble is ITickLate)
            ticksLate.Remove(updateble as ITickLate);

        if (updateble is ITickSec)
            tickSecs.Remove(updateble as ITickSec);

    }

     void Tick()
    {
        for (var i = 0; i < ticks.Count; i++)
        {
            ticks[i].Tick();
        }
    }

     void TickFixed()
    {
        for (var i = 0; i < ticksFixes.Count; i++)
        {
            ticksFixes[i].TickFixed();
        }
    }

     void TickLate()
    {
        for (var i = 0; i < ticksLate.Count; i++)
        {
            ticksLate[i].TickLate();
        }
    }

    void TickSec()
    {
        for (var i = 0; i < tickSecs.Count; i++)
        {
            tickSecs[i].TickSec();
        }
    }


    private void Update()
    {
        if (!OnSceneChanged)
        {
            if(sec > 0)
            {
                sec -= Time.deltaTime;
            }
            else
            {
                sec = 1;
                TickSec();
            }
            Tick();
        }
    }

    private void FixedUpdate()
    {
        if (!OnSceneChanged)
            TickFixed();
    }

    private void LateUpdate()
    {
        if (!OnSceneChanged)
            TickLate();
    }

    private void OnDestroy()
    {
        ticks.Clear();
        ticksLate.Clear();
        ticksFixes.Clear();
        tickSecs.Clear();

    }
}