﻿using System.Collections.Generic;
using Framework.Common;
using Framework.Interfaces;
using UnityEngine;

namespace Framework.Managers.Event
{
    public class EventManager:MonoBehaviour
    {
        private Dictionary<EVENT_TYPE, List<IEventListener>> Listeners 
        = new Dictionary<EVENT_TYPE, List<IEventListener>>();

        public void AddListener(EVENT_TYPE Event_Type, IEventListener Listener)
        {
            List<IEventListener> ListenList = null;

            if (Listeners.TryGetValue(Event_Type,out ListenList))
            {
                ListenList.Add(Listener);
                return;
            }

            ListenList = new List<IEventListener>();
            ListenList.Add(Listener);
            Listeners.Add(Event_Type, ListenList);
        }

        public void PostNotification(EVENT_TYPE Event_Type, Component Sender, System.Object Param = null)
        {
            List<IEventListener> ListenList = null;

            if (!Listeners.TryGetValue(Event_Type, out ListenList))
                return;

            for ( int i = 0; i < ListenList.Count; i++ )
            {
                if (!ListenList[i].Equals(null))
                    ListenList[i].OnEvent(Event_Type, Sender, Param);
            }
        }

        public void RemoveEvent(EVENT_TYPE Event_Type)
        {
            Listeners.Remove(Event_Type);
        }

        public void RemoveRedundancies()
        {
            Dictionary<EVENT_TYPE, List<IEventListener>> TmpListeners  = new Dictionary<EVENT_TYPE, List<IEventListener>>();

            foreach(KeyValuePair<EVENT_TYPE,List<IEventListener>> Item in Listeners )
            {
                for ( int i = Item.Value.Count-1; i >0; i--)
                {
                    if (Item.Value[i].Equals(null))
                        Item.Value.RemoveAt(i);

                    if (Item.Value.Count > 0)
                        TmpListeners.Add(Item.Key, Item.Value);
                }
            }
            Listeners = TmpListeners;
        }

        private void OnLevelWasLoaded(int level)
        {
            RemoveRedundancies();
        }
    }
}
