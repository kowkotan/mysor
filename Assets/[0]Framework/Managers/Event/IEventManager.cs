﻿using Framework.Interfaces;
using UnityEngine;

namespace Framework.Managers.Event
{
    public interface IEventManager
    {
        void AddListener(EVENT_TYPE Event_Type, IEventListener Listener);
        void PostNotification(EVENT_TYPE Event_Type, Component Sender, System.Object Param = null);
        void RemoveEvent(EVENT_TYPE Event_Type);
        void RemoveRedundancies();
    }
}