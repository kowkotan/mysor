﻿using System.Collections.Generic;
using Framework.Utility;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Framework.Managers.Statistics
{
    public class StatisticSystemController:MonoBehaviour
    {
        List<IStatisticSystem> statisticSystemList = new List<IStatisticSystem>();

        public void AddStatisticSystem(IStatisticSystem statisticSystem)
        {
            if(!statisticSystemList.Contains(statisticSystem))
            statisticSystemList.Add(statisticSystem);
        }

        public void Init()
        {
            for(int i = 0; i < statisticSystemList.Count; i++)
            {
                statisticSystemList[i].Init();
            }
        }

        void OnApplicationPause(bool pauseStatus)
        {
        
            for (int i = 0; i < statisticSystemList.Count; i++)
            {
                statisticSystemList[i].OnApplicationPause(pauseStatus);
            }
        }

        void OnApplicationFocus(bool hasFocus)
        {
            for (int i = 0; i < statisticSystemList.Count; i++)
            {
                statisticSystemList[i].OnApplicationFocus(hasFocus);
            }
        }

       public void SendEvent(string eventName)
        {
            for (int i = 0; i < statisticSystemList.Count; i++)
            {
                statisticSystemList[i].SendEvent(eventName);
            }
        }

        public void SendLevelWinEvent(int levelNumb)
        {
            for (int i = 0; i < statisticSystemList.Count; i++)
            {
                statisticSystemList[i].SendLevelWinEvent(levelNumb);
            }
        }

        public void SendLevelLooseEvent(int levelNumb)
        {
            for (int i = 0; i < statisticSystemList.Count; i++)
            {
                statisticSystemList[i].SendLevelLooseEvent(levelNumb);
            }
        }
    }
}
