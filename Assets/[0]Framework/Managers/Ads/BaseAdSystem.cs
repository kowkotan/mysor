﻿using System.Collections.Generic;
using System;
using Framework.Interfaces;
using Framework.Security.SafeTypes;
using Framework.Security;

namespace Framework.Managers.Ads
{
    public abstract class BaseAdSystem
    {
        SafeInt adEnableStatus;

        protected IRewardedVideoAdListener rewardedVideoAdListener;
        protected IInterstitialAdListener interstitialAdListener;

        public abstract void Init();
        public abstract bool rewardVideoIsLoaded();
        public abstract void ShowInterstetial(Action<bool> IsLoaded);
        public abstract void ShowRewardedVideo(Action<bool> IsLoaded);
        public abstract KeyValuePair<string,double> GetRewardParameters();
        public abstract void ShowBanner();
        public abstract void HideBanner();

        protected BaseAdSystem()
        {
            adEnableStatus = new SafeInt( SafePlayerPrefs.GetInt("adEnableStatus",1) );
        }

        public bool AdIsEnable()
        {
            return adEnableStatus == new SafeInt(1);
        }

        public void DisableAds()
        {
            adEnableStatus = new SafeInt(0);
            SafePlayerPrefs.SetInt("adEnableStatus", adEnableStatus.GetValue());
            HideBanner();
        }

        public void SetRewardedVideoCallbacks(IRewardedVideoAdListener listener)
        {
            rewardedVideoAdListener = listener;
        }

        public void SetInterstitialCallbacks(IInterstitialAdListener listener)
        {
            interstitialAdListener = listener;
        }
    }
}
