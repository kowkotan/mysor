﻿//using System;
//using Adic;
//using Framework.Interfaces;
//using Framework.Managers.Ads;

//public class DisableAdsPopupModel : IDisposable
//{
//    [Inject] IPurchaseSystem purchaseSystem;
//    [Inject] BaseAdSystem adSystem;
//    [Inject] GameConfig gameConfig;

//    public bool IsLoadingStarted;
//    public bool IsLoadingFinished;
//    public bool IsPopupClosed;

//    string productId;

//    public void OnLoad()
//    {
//        purchaseSystem.OnConnectHandler += OnConnectHandler;
//        purchaseSystem.OnRestoreHandler += OnRestorePurchases;
//        purchaseSystem.OnBuyProductHandler += OnBuyProduct;
//        purchaseSystem.Connect();

//        #if UNITY_ANDROID
//                productId = gameConfig.googlePlayNoAdsPurchId;
//        #elif UNITY_IOS
//                productId = gameConfig.iOSNoAdsPurchId;
//        #endif

//        IsLoadingStarted = true;

//    }

//    public void MyLateUpdate()
//    {
//        IsLoadingStarted = false;
//        IsLoadingFinished = false;
//        IsPopupClosed = false;
//    }


//    private void OnConnectHandler(bool result, string mess)
//    { 
//        IsLoadingFinished = true;
//    }

//    private void OnBuyProduct(bool result, string mess, Product product)
//    {
//        if ( result)
//        {
//            adSystem.DisableAds(); 
//        }
//        IsLoadingFinished = true;
//        IsPopupClosed = true;
//    }

//    private void OnRestorePurchases(bool result, string mess)
//    {
//        IsLoadingFinished = true;
//    }


//    public string GetPopUpText()
//    {
//        string _text = "Buying Premium will remove all banner ads and interstitial ads \n( fullscreens between levels )";
//        return _text;
//    }

//    public void DisableAdBtnClicked()
//    {
//        IsLoadingStarted = true;
//        purchaseSystem.BuyProduct(productId, 1, true);
//    }

//    public void RestoreBtnClicked()
//    {
//        IsLoadingStarted = true;
//        purchaseSystem.RestorePurchases();
//    }

//    public void CloseBtnClicked()
//    {
//        IsPopupClosed = true;
//    }

//    public void OnUnload()
//    {
//        purchaseSystem.OnConnectHandler -= OnConnectHandler;
//        purchaseSystem.OnRestoreHandler -= OnRestorePurchases;
//        purchaseSystem.OnBuyProductHandler -= OnBuyProduct;
//        purchaseSystem.Disconnect();
//    }

//    public void Dispose()
//    {
//        gameConfig = null;
//        purchaseSystem = null;
//    }
//}


