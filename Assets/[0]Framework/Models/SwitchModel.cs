﻿using System;
using Adic;
using UnityEngine;

public abstract class SwitchModel
{

    public bool currentSwitchStateValueLoaded;
    public bool state;

    public virtual void LoadCurrentStateValue()
    {
        currentSwitchStateValueLoaded = true;
    }

    public abstract void StateChanged(bool _state);

    public virtual void MyLateUpdate()
    {
        currentSwitchStateValueLoaded = false;
    }

}

