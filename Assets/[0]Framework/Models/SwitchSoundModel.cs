﻿using System;
using Adic;
using UnityEngine;

public class SwitchSoundModel : SwitchModel,IDisposable
{
    [Inject] IAudioSystem audioSystem;

    public override void LoadCurrentStateValue()
    {
        currentSwitchStateValueLoaded = true;
        GlobalVars.Sound = PlayerPrefs.GetInt("sound", 1);
        if (GlobalVars.Sound == 1)
        {
            state = true;
         }
        else
        {
            state = false;
         }
        UpdateSoundSettings(state);
    }

    void UpdateSoundSettings(bool _state)
    {
        if (_state)
        {
            GlobalVars.Sound = 1;
            audioSystem.Unmute();
        }
        else
        {
            GlobalVars.Sound = 0;
            audioSystem.Mute();
        }
    }

    public override void StateChanged(bool _state)
    {
        UpdateSoundSettings(_state);
        PlayerPrefs.SetInt("sound", GlobalVars.Sound);
        state = _state;
    }

    public override void MyLateUpdate()
    {
        base.MyLateUpdate();
    }

    public void Dispose()
    {
        audioSystem = null;
    }
}

