﻿using System;
using Adic;
using Framework.Managers.Ads;
using Framework.Interfaces;

public class RewardedVideoButtonModel:IDisposable,IRewardedVideoAdListener
{

    [Inject] BaseAdSystem baseAdSystem;

    public bool OnRewardedVideoStarted;
    public bool OnRewardedVideoShowSucces;
    public bool OnRewardedVideoShowError;
    public bool OnRewardedVideoIsLoaded;

    public void Init()
    {
        baseAdSystem.SetRewardedVideoCallbacks(this);
    }

    public bool RewardedBtnActiveStatus()
    {
        return baseAdSystem.rewardVideoIsLoaded();
    }

    public void ButtonClicked()
    {
        baseAdSystem.ShowRewardedVideo((bool onLoaded) =>
        {
            OnRewardedVideoStarted = true;
        });
    }

    public void Unload()
    {
        OnRewardedVideoStarted = false;
        OnRewardedVideoShowSucces = false;
        OnRewardedVideoShowError = false;
    }

    public void TickLate()
    {
        OnRewardedVideoStarted = false;
        OnRewardedVideoShowSucces = false;
        OnRewardedVideoShowError = false;
      
    }

    public void OnRewardedVideoLoaded()
    {
        OnRewardedVideoIsLoaded = true;
    }

    public void OnRewardedVideoClosed(bool finished)
    {
        if (finished) OnRewardedVideoShowSucces = true;
        else OnRewardedVideoShowError = true;
    }

    public void OnRewardedVideoFinished(double amount, string name)
    {
        OnRewardedVideoShowSucces = true;
    }

    public void Dispose()
    {
        baseAdSystem.SetRewardedVideoCallbacks(null);
        baseAdSystem = null;
    }

    public void OnRewardedVideoFailedToLoad() { }
  
    public void OnRewardedVideoShown(){}
}

