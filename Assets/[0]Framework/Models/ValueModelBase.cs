﻿using System;
using Adic;

using UnityEngine;

public class ValueModelBase 
{
     string valueId;
     int defStartValue = 200;
     int defMaxValue = 999999;

    public Action OnValueChanged = delegate {};
    public Action OnNotEnoughValues = delegate {};

    int currentValue;
    public int CurrentValue
    {
        get
        {
            return currentValue;
        }
       private set
        {
            currentValue = value;
            OnValueChanged();
            PlayerPrefs.SetInt("current"+ valueId, currentValue);
        
        }

    }

     int maxValue;
    public int MaxValue
    {
        get
        {
            return maxValue;
        }
      private  set
        {
            maxValue = value;
            PlayerPrefs.SetInt("max" + valueId, maxValue);
        }
    }

    public virtual void AddValue(int _value)
    {
        CurrentValue += _value;

        if (currentValue > MaxValue) CurrentValue = MaxValue;
    }

    public bool DecValue(int _value)
    {
        if (!isValueEnough(_value))
        {
            OnNotEnoughValues();
            return false;
        }

        CurrentValue -= _value;

        return true;
    }


    public  void Init(string _valueId, int _defStartValue, int _defMaxValue)
    {
        valueId = _valueId;
        defStartValue = _defStartValue;
        defMaxValue = _defMaxValue;

        Load();

    }

    public bool isValueEnough(int _cost)
    {
        return (currentValue - _cost) >= 0;
    }

    public void Load()
    {
        CurrentValue = PlayerPrefs.GetInt("current" + valueId, defStartValue);
        MaxValue = PlayerPrefs.GetInt("max" + valueId, defMaxValue);
    }
}

