﻿using System;
using Adic;
using Framework.Interfaces;
using UnityEngine;

public class SwitchVibroModel : SwitchModel,IDisposable
{

    [Inject] IVibrationSystem vibrationSystem;
    public override void LoadCurrentStateValue()
    {
        currentSwitchStateValueLoaded = true;
        GlobalVars.Vibration = PlayerPrefs.GetInt("vibro", 1);
        if (GlobalVars.Vibration == 1) state = true;
        else state = false;

    }

    public override void StateChanged(bool _state)
    {
        if (_state)
        {
            GlobalVars.Vibration = 1;
            vibrationSystem.VibrationOn();
           
        }
        else
        {
            GlobalVars.Vibration = 0;
            vibrationSystem.VibrationOff();
     
        }
        PlayerPrefs.SetInt("vibro", GlobalVars.Vibration);
        state = _state;
    }

    public override void MyLateUpdate()
    {
        base.MyLateUpdate();
    }

    public void Dispose()
    {
        vibrationSystem = null;
    }
}

