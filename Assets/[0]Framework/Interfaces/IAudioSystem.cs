﻿public interface IAudioSystem
{
    void Mute();
    void PlayBgMusic(SoundEnum musicType);
    void PlayOnce(SoundEnum soundType);
    void Unmute();
}