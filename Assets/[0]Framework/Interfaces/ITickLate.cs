﻿namespace Framework.Interfaces
{
    public interface ITickLate
    {
        void TickLate();
    }
}
