﻿namespace Framework.Interfaces
{
    public interface ILoadingProgressListener
    {
        void SetLoadingProgress(float _progress);
    }
}
