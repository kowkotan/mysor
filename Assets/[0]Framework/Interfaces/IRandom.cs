﻿using System;
namespace Framework.Interfaces
{
    public interface IRandom
    {
        float returnChance { get; }
    }
}
