﻿using UnityEngine;

namespace Framework.Interfaces
{

    public interface IState
    {
        void Load();
        void Unload();
    }
}
