﻿using System;

public interface IUnbreakable
{
    bool Unbreakable { get; set; }
}

