﻿using System;

namespace Framework.Interfaces
{
    public interface IInterstitialAdListener
    {
        #region Interstitial callback handlers
        void OnInterstitialLoaded(bool isPrecache);
        void OnInterstitialShown();
        void OnInterstitialClosed();
        void OnInterstitialClicked();
        void OnInterstitialFailedToLoad();
        #endregion
    }
}
