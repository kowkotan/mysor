﻿public interface IObstacleCollisionListener
{
    void OnCollision();
}