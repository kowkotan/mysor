﻿using Framework.Security.Cryptography;
using Framework.Utility;
using UnityEngine;

namespace Framework.Security
{
    public static class SafePlayerPrefs
    {
        readonly static string secretKey = "zFrqoln5";

        public static string GetString(string _key, string _defValue = "" )
        {
            string encryptedValue = PlayerPrefs.GetString(_key, _defValue);

            if (encryptedValue == _defValue)
                return _defValue;
            return AES.Decrypt(encryptedValue, secretKey);
        }

        public static void SetString(string _key,string _value)
        {
            string encryptedData = AES.Encrypt(Converter.StringToBytesArray(_value), secretKey);
            PlayerPrefs.SetString(_key,encryptedData);
        }

        public static int GetInt(string _key, int _defValue = 0)
        {
            return int.Parse(GetString(_key, _defValue.ToString()));
        }

        public static void SetInt(string _key,int _value)
        {
            SetString(_key, _value.ToString());
        }

        public static float GetFloat(string _key, float _defValue = 0)
        {
            return float.Parse(GetString(_key, _defValue.ToString()));
        }

        public static void SetFloat(string _key, float _value)
        {
            SetString(_key, _value.ToString());
        }

    }
}
