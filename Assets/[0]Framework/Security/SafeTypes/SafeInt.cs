﻿using System;
namespace Framework.Security.SafeTypes
{
    public struct SafeInt
    {
        private int offset;
        private int value;

        public SafeInt(int value = 0)
        {
            Random random = new Random();
            offset = random.Next(-1000, +1000);
            this.value = value + offset;
        }

        public int GetValue()
        {
            return value - offset;
        }

        public void Dispose()
        {
            offset = 0;
            value = 0;
        }

        public override string ToString()
        {
            return GetValue().ToString();
        }

        public static SafeInt operator +(SafeInt f1, SafeInt f2)
        {
            return new SafeInt(f1.GetValue() + f2.GetValue());
        }

        public static SafeInt operator -(SafeInt f1, SafeInt f2)
        {
            return new SafeInt(f1.GetValue() - f2.GetValue());
        }

        public static SafeInt operator /(SafeInt f1, SafeInt f2)
        {
            return new SafeInt(f1.GetValue() / f2.GetValue());
        }

        public static SafeInt operator *(SafeInt f1, SafeInt f2)
        {
            return new SafeInt(f1.GetValue() * f2.GetValue());
        }

        public static SafeInt operator %(SafeInt f1, SafeInt f2)
        {
            return new SafeInt(f1.GetValue() % f2.GetValue());
        }

        public static bool operator ==(SafeInt f1, SafeInt f2)
        {
            return f1.GetValue() == f2.GetValue();
        }

        public static bool operator !=(SafeInt f1, SafeInt f2)
        {
            return f1.GetValue() != f2.GetValue();
        }

        public static bool operator <(SafeInt f1, SafeInt f2)
        {
            return f1.GetValue() < f2.GetValue();
        }

        public static bool operator >(SafeInt f1, SafeInt f2)
        {
            return f1.GetValue() < f2.GetValue();
        }

        public static bool operator <=(SafeInt f1, SafeInt f2)
        {
            return f1.GetValue() <= f2.GetValue();
        }

        public static bool operator >=(SafeInt f1, SafeInt f2)
        {
            return f1.GetValue() >= f2.GetValue();
        }
    }
}
