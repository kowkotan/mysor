﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;

public class ValueTextIndicatorUIBase : MonoBehaviour
{
    ValueModelBase valueModel;

    [SerializeField]
    Text valueTxt;

    public virtual void Init(ValueModelBase _valueModel )
    {
        valueModel = _valueModel;

        valueModel.OnValueChanged += ValueModel_OnValueChanged;
        UpdateDetalText(); 
    }

    void ValueModel_OnValueChanged()
    {
        UpdateDetalText();
    }

    private void UpdateDetalText()
    {
        valueTxt.text = valueModel.CurrentValue.ToString();
    }

    private void OnDestroy()
    {
        valueModel.OnValueChanged -= ValueModel_OnValueChanged;
    }
}
