﻿using System;
using System.Collections;
using System.Collections.Generic;
using Adic;
using UnityEngine;
using UnityEngine.UI;

public abstract class ValueTextIndicatorUIWithButtonBase : ValueTextIndicatorUIBase, IDisposable
{
    [SerializeField] Button actionBtnClicked;


    public override void Init(ValueModelBase _valueModel)
    {
        base.Init(_valueModel);
        actionBtnClicked.onClick.AddListener(HandleUnityAction);

    }

    void HandleUnityAction()
    {
        BtnClicked();
    }

    public abstract void BtnClicked();

   
    public virtual void Dispose()
    {
        actionBtnClicked.onClick.RemoveListener(HandleUnityAction);
    }

}
