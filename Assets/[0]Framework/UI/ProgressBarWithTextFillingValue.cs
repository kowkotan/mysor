﻿using System.Collections;
using System.Collections.Generic;
using Framework.Views;
using UnityEngine;

public class ProgressBarWithTextFillingValue : MonoBehaviour, IProgressBar
{
    [SerializeField]
    PercentFillingValueView percentFillingValue;

    [SerializeField]
    FillingImageView fillingImageView;

    public void FillValue(float minValue, float maxValue )
    {
        percentFillingValue.SetNormalizedValue(minValue / maxValue);
        fillingImageView.SetNormalizedValue(minValue / maxValue);
    }

}
