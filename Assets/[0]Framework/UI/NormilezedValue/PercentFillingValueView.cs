﻿using System.Collections;
using System.Collections.Generic;
using Framework.Views;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PercentFillingValueView : BaseFloatValueView
{
    [SerializeField] Text textField;

    public override void SetNormalizedValue(float value)
    {
        textField.text = string.Format("{0:0}%",value * 100);
    }
}
