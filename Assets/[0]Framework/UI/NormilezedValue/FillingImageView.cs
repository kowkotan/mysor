﻿using UnityEngine;
using UnityEngine.UI;

namespace Framework.Views
{
    public class FillingImageView : BaseFloatValueView
    {
        [SerializeField]
        private Image _image;
        public override void SetNormalizedValue(float value)
        {
            _image.fillAmount = value;
        }
    }
}
