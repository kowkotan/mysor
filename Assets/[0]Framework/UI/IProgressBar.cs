﻿public interface IProgressBar
{
    void FillValue(float minValue, float maxValue);
}