﻿using Framework.Utility;
using UnityEngine;

namespace Framework.Components
{
    [RequireComponent(typeof(SphereCollider))]
    public class Explosion3D : MonoBehaviour
    {

        [SerializeField] float explosion_force;
        [SerializeField] float explosion_rate;
        [SerializeField] float explosion_max_sixe;
        [SerializeField] float currentRadius;

        [SerializeField] GameObject visualSide;
        [SerializeField] GameObject boomEffectPrefab;

        bool exploded;

        Rigidbody rb;
        SphereCollider expRadius;

        public void Awake()
        {
            expRadius = GetComponent<SphereCollider>();
        }

        private void FixedUpdate()
        {
            if (exploded)
            {
                if (currentRadius < explosion_max_sixe)
                {
                    currentRadius += explosion_rate;
                }
                else
                {
                    gameObject.SetActive(false);
                }
                expRadius.radius = currentRadius;
            }
        }

        public void Boom()
        {
           if( boomEffectPrefab != null )
            Instantiate(boomEffectPrefab, new Vector3( transform.position.x, transform.position.y, transform.position.z-1), Quaternion.identity);

            visualSide.SetActive(false);
            exploded = true;

        }

        private void OnTriggerEnter(Collider collision)
        {
            Rigidbody rb;
            if (exploded)
            {
                rb = collision.gameObject.GetComponent<Rigidbody>();
                if (rb != null)
                {
                    rb.AddExplosionForce( explosion_force, transform.position, currentRadius);
                }
            }
        }
    }
}
