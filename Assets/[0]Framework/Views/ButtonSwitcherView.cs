﻿using System;
using Framework.Interfaces;
using Framework.Views;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSwitcherView : BaseGoSwitcherView
{
    [SerializeField] Button changeStateBtn;

    public bool onStateChanged;
    public bool state;

    void Start()
    {
        changeStateBtn.onClick.AddListener(() =>
        {
           state = Switch();
            onStateChanged = true;
        });

    }

    public void MyLateUpdate()
    {
        onStateChanged = false;
    }

    void OnDestroy()
    {
        changeStateBtn.onClick.RemoveAllListeners();
    }
}
