﻿using Framework.Interfaces;
using UnityEngine;
using UnityEngine.UI;

public class LevelCheatPanelView : MonoBehaviour ,ITickLate, IState
{

    [SerializeField]
    Button loadPrevLevelBtn;

    [SerializeField]
    Button loadSelectedLevelBtn;

    [SerializeField]
    Button loadNextLevelBtn;

    [SerializeField]
    InputField levelInputField;

    public bool onLoadPrevLevelBtnPressed;
    public bool onLoadSelectedLevelBtnPressed;
    public bool onLoadNextLevelBtnPressed;


    private void Start()
    {
        loadPrevLevelBtn.onClick.AddListener(() =>
        {
            onLoadPrevLevelBtnPressed = true;
        });

        loadSelectedLevelBtn.onClick.AddListener(() =>
        {
            onLoadSelectedLevelBtnPressed = true;
        });

        loadNextLevelBtn.onClick.AddListener(() =>
        {
            onLoadNextLevelBtnPressed = true;
        });
    }

    public string GetLevelInputFieldText()
    {
        return levelInputField.text;
    }

    public void TickLate()
    {
        onLoadPrevLevelBtnPressed = false;
        onLoadSelectedLevelBtnPressed = false;
        onLoadNextLevelBtnPressed = false;
    }

    private void OnDestroy()
    {
        loadPrevLevelBtn.onClick.RemoveAllListeners();
        loadSelectedLevelBtn.onClick.RemoveAllListeners();
        loadNextLevelBtn.onClick.RemoveAllListeners();
    }

    public void Load()
    {
        gameObject.SetActive(true);
    }

    public void Unload()
    {
        gameObject.SetActive(false);
    }
}

