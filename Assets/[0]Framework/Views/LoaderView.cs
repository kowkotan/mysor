﻿using System;
using Framework.Interfaces;
using UnityEngine;

namespace Framework.Views
{
    public class LoaderView : MonoBehaviour, IState, ILoadingProgressListener
    {
        [SerializeField] BaseFloatValueView[] loadingIndicators;


        void Start()
        {
       
        }
        public void Load()
        {
            gameObject.SetActive(true);
        }

        public void SetLoadingProgress(float _progress)
        {
            for (int i = 0; i < loadingIndicators.Length; i++)
            {
                loadingIndicators[i].SetNormalizedValue(_progress);
            }
        }

        public void Unload()
        {
            gameObject.SetActive(false);

        }

    }
}