﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RewardedVideoButtonView : MonoBehaviour
{
    [SerializeField] Button rewardVideoBtn;
    public bool rewardVideoBtnClicked;

    void Start()
    {
        rewardVideoBtn.onClick.AddListener(() =>
        {
            rewardVideoBtnClicked = true;
        });
    }

    public void UpdateRewardButtonActiveStatus( bool activeStatus)
    {
        rewardVideoBtn.interactable = activeStatus;
    }

    public void Unload()
    {
        rewardVideoBtnClicked = false;
    }

    public void TickLate()
    {
        rewardVideoBtnClicked = false;
    }

    private void OnDestroy()
    {
        rewardVideoBtn.onClick.RemoveAllListeners();
    }
}
