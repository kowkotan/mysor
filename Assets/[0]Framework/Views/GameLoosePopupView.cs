﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLoosePopupView : MonoBehaviour
{

    [SerializeField] Button restartBtn;
    [SerializeField] Text levelTextField;

    public event Action restartBtnClicked = delegate { };

    public void Load()
    {
        gameObject.SetActive(true);
    }

    public void Unload()
    {
        gameObject.SetActive(false);
    }

    public void SetLevelTextFieldText(string _text)
    {
        levelTextField.text = "LEVEL " + _text;
    }


    void Start()
    {

        restartBtn.onClick.AddListener(() =>
        {
            restartBtnClicked();
        });

    }

    void OnDestroy()
    {
        restartBtn.onClick.RemoveAllListeners();
    }
}
