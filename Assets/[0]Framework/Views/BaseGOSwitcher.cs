﻿using System;
using UnityEngine;

namespace Framework.Views
{
    public class BaseGoSwitcherView : MonoBehaviour
    {
        bool state;

        [SerializeField]
        GameObject gameObjectOn;
        [SerializeField]
        GameObject gameObjectOff;

        public bool Switch()
        {
            if (state) TurnOFff(); else TurnOn();
            return state;
        }

        public bool Switch(bool state)
        {
            if (state) TurnOn(); else TurnOFff();
            return state;
        }

         void TurnOn()
        {
            state = true;
            StateChanged();
        }

         void TurnOFff()
        {
            state = false;
            StateChanged();
        }

        void StateChanged()
        {
            gameObjectOn.SetActive(false);
            gameObjectOff.SetActive(false);
            if ( state)
            {
                gameObjectOn.SetActive(true);
            }
            else
            {
                gameObjectOff.SetActive(true);
            }
        }
    }
}
