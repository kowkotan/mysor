﻿using System;
using Framework.Interfaces;
using UnityEngine;
using UnityEngine.UI;

public class DisableAdsPopupView : MonoBehaviour,IState {

    [SerializeField] Button disableButton;
    [SerializeField] Button restorePurchasesButton;
    [SerializeField] Button closeButton;
    [SerializeField] Text noAdsText;

    [SerializeField] GameObject loadingPanel;

    public bool disableButtonClicked;
    public bool restorePurchasesButtonClicked;
    public  bool closeButtonClicked;

    public void Load()
    {
        gameObject.SetActive(true);
    }

    public void Unload()
    {
        gameObject.SetActive(false);
    }

    public void Loading(bool _isActive)
    {
        Debug.Log(_isActive);
        loadingPanel.SetActive(_isActive);
    }

    public void SetPopupText( string _text )
    {
        noAdsText.text = _text;
    }

    public void MyLateUpdate()
    {
        disableButtonClicked = false;
        restorePurchasesButtonClicked = false;
        closeButtonClicked = false;
    }

    void Start()
    {

        disableButton.onClick.AddListener(() =>
        {
            disableButtonClicked = true;
        });

        restorePurchasesButton.onClick.AddListener(() =>
        {
            restorePurchasesButtonClicked = true;
        });

        closeButton.onClick.AddListener(() =>
        {
            closeButtonClicked = true;
        });
    }

    void OnDestroy()
    {
        disableButton.onClick.RemoveAllListeners();
        restorePurchasesButton.onClick.RemoveAllListeners();
        closeButton.onClick.RemoveAllListeners();
    }

}
