﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Framework.Editor
{
    [InitializeOnLoad]
    public class SceneGenerator
    {


        static SceneGenerator()
        {
            EditorSceneManager.newSceneCreated += EditorSceneManager_NewSceneCreated;
        }

        static void EditorSceneManager_NewSceneCreated(Scene scene, NewSceneSetup setup, NewSceneMode mode)
        {

            CreateInitFolder();
            CreateWorldFolder();
            CreateUIFolder();
            CreateLevelFolder();
            CreateScriptFolder();
        }

        static Transform CreateFolderAndGetTransform(string folderName)
        {
            return new GameObject(folderName).transform;
        }

        static Transform CreateAndAttach(string newFolderName, Transform parentFolder)
        {
            var newFolder = CreateFolderAndGetTransform(newFolderName);
            newFolder.parent = parentFolder;
            return newFolder;
        }

        static Transform Attach(Transform child, Transform parent)
        {
            return child.parent = parent;
        }

        static Transform CreateInitFolder()
        {
            return CreateFolderAndGetTransform("[INIT]");
        }

        static Transform CreateWorldFolder()
        {
            var camGO = Camera.main.transform;
            var worldFolder = CreateFolderAndGetTransform("[WORLD]");
            var renderFolder = CreateAndAttach("Render", worldFolder);

            Attach(camGO, renderFolder);

            CreateAndAttach("Static", worldFolder);
            CreateAndAttach("Dynamics", worldFolder);


            return worldFolder;
        }

        static Transform CreateLevelFolder()
        {
            return CreateFolderAndGetTransform("[LEVELS]");
        }

        static Transform CreateUIFolder()
        {
            return CreateFolderAndGetTransform("[UI]");
        }

        static Transform CreateScriptFolder()
        {
            return CreateFolderAndGetTransform("[SCRIPTS]");
        }
    }
}
