﻿using Adic;
using Framework.Common;
using Framework.Controllers.Loader;
using Framework.Interfaces;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using Framework.Managers.Event;

namespace Framework
{
    public class LevelLoader:MonoBehaviour
    {
        string levelFolderName = "[LEVELS]";
        Transform levelFolder;
        [Inject] EventManager eventManager;

        private void Awake()
        {
            levelFolder = GameObject.Find(levelFolderName).transform;
        }

        void ClearLevelFolder()
        {
            foreach (Transform child in levelFolder)
            {
                Destroy(child.gameObject);
            }
        }

        public void LoadLevel(IPathProvider pathProvider)
        {
            eventManager.PostNotification(EVENT_TYPE.UNLOAD_LEVEL, null);
            ClearLevelFolder();
            string pathToResource = pathProvider.GetLevelPath();
            Instantiate(Resources.Load(pathToResource), levelFolder);
        }
    }
}
