﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using Framework.Interfaces;
using UnityEngine.SceneManagement;

namespace Framework.Utility
{
    public static partial class Extensions
    {
        static System.Random _r = new System.Random();

        #region SPRITE RENDER

        public static void SetAlpha(this SpriteRenderer r, float alpha)
        {
            var c = r.color;
            r.color = new Color(c.r, c.g, c.b, alpha);
        }

        public static Color SetAlpha(this SpriteRenderer r, Color c, float alpha)
        {
            return new Color(c.r, c.g, c.b, alpha);
        }

        #endregion

        #region PHYSICS
        public static void AddExplosionForce2D( this Rigidbody2D body, float expForce, Vector3 expPosition, float expRadius)
        {
            var dir = (body.transform.position - expPosition);
            float calc = 1 - (dir.magnitude / expRadius);
            if (calc <= 0)
            {
                calc = 0;
            }

            body.AddForce(dir.normalized * expForce * calc);

        }
        #endregion

        #region MATH
        public static bool Every(this float step, float time)
        {
            if (step % time == 0)
            {
                return true;
            }

            return false;
        }

        public static bool Every(this int step, float time)
        {
            if (step % time == 0)
            {
                return true;
            }

            return false;
        }

        public static float Plus(this float arg0, float val, float clamp = 1f) { return Math.Min(clamp, arg0 + val); }
        public static float Minus(this float arg0, float val, float clamp = 0f) { return Math.Max(clamp, arg0 - val); }
        public static int Plus(this int arg0, int val, int clamp = 1) { return Math.Min(clamp, arg0 + val); }
        public static int Minus(this int arg0, int val, int clamp = 0) { return Math.Max(clamp, arg0 - val); }
        #endregion
        
        #region RANDOM
        public static float Between(this Vector2 v) { return UnityEngine.Random.value > 0.5f ? v.x : v.y; }
        public static int Between(this object o, int a, int b, float chance = 0.5f) { return UnityEngine.Random.value > chance ? a : b; }
        public static float Between(this object o, float a, float b, float chance = 0.5f) { return UnityEngine.Random.value > chance ? a : b; }

        public static T RandomExcept<T>(this T[] list, T exceptVal)
        {
            var val = list[UnityEngine.Random.Range(0, list.Length)];

            while (val.Equals(exceptVal))
                val = list[UnityEngine.Random.Range(0, list.Length)];

            return val;
        }

        public static T RandomExcept<T>(this List<T> list, T exceptVal)
        {
            var val = list[UnityEngine.Random.Range(0, list.Count)];

            while (val.Equals(exceptVal))
                val = list[UnityEngine.Random.Range(0, list.Count)];

            return val;
        }

        public static T Random<T>(this List<T> list, T[] itemsToExclude)
        {
            var val = list[UnityEngine.Random.Range(0, list.Count)];

            while (itemsToExclude.Contains(val))
                val = list[UnityEngine.Random.Range(0, list.Count)];

            return val;
        }

        public static T Random<T>(this List<T> list)
        {
            var val = list[UnityEngine.Random.Range(0, list.Count)];
            return val;
        }

        public static T Select<T>(this List<T> vals) where T : IRandom
        {
            float total = 0f;

            var probs = new float[vals.Count];


            for (int i = 0; i < probs.Length; i++)
            {
                probs[i] = vals[i].returnChance;
                total += probs[i];
            }


            float randomPoint = (float)_r.NextDouble() * total;


            for (int i = 0; i < probs.Length; i++)
            {
                if (randomPoint < probs[i])
                    return vals[i];
                randomPoint -= probs[i];
            }

            return vals[0];
        }

        public static T Select<T>(this T[] vals) where T : IRandom
        {
            var total = 0f;
            var probs = new float[vals.Length];


            for (int i = 0; i < probs.Length; i++)
            {
                probs[i] = vals[i].returnChance;
                total += probs[i];
            }


            var randomPoint = (float)_r.NextDouble() * total;


            for (int i = 0; i < probs.Length; i++)
            {
                if (randomPoint < probs[i])
                    return vals[i];
                randomPoint -= probs[i];
            }

            return vals[0];
        }

        public static T Select<T>(this T[] vals, out int index) where T : IRandom
        {
            index = -1;

            if (vals == null || vals.Length == 0) return default(T);


            float total = 0f;

            float[] probs = new float[vals.Length];


            for (int i = 0; i < probs.Length; i++)
            {
                probs[i] = vals[i].returnChance;
                total += probs[i];
            }


            float randomPoint = (float)_r.NextDouble() * total;


            for (int i = 0; i < probs.Length; i++)
            {
                if (randomPoint < probs[i])
                {
                    index = i;
                    return vals[i];
                }

                randomPoint -= probs[i];
            }

            return vals[0];
        }

        public static T Random<T>(this T[] vals) { return vals[UnityEngine.Random.Range(0, vals.Length)]; }

        public static T RandomDequeue<T>(this List<T> vals)
        {
            var index = UnityEngine.Random.Range(0, vals.Count);
            var val = vals[index];
            vals.RemoveAt(index);
            return val;
        }

        #endregion

        #region TRANSFORMS

        public static T AddGet<T>(this GameObject co) where T : Component
        {
            var c = co.GetComponent<T>();
            if (c == null)
                c = co.AddComponent<T>();

            return c;
        }

        public static T AddGet<T>(this Transform co) where T : Component
        {
            var c = co.GetComponent<T>();
            if (c == null)
                c = co.gameObject.AddComponent<T>();

            return c;
        }

        public static T Find<T>(this GameObject go, string path) { return go.transform.Find(path).GetComponent<T>(); }

        public static Transform FindDeep(this Transform obj, string id)
        {
            if (obj.name == id)
            {
                return obj;
            }

            int count = obj.childCount;
            for (int i = 0; i < count; ++i)
            {
                var posObj = obj.GetChild(i).FindDeep(id);
                if (posObj != null)
                {
                    return posObj;
                }
            }

            return null;
        }


        public static List<T> GetAll<T>(this Transform obj)
        {
            var results = new List<T>();
            obj.GetComponentsInChildren(results);
            return results;
        }

        public static List<T> FindOnCurrentScene<T>()
        {
            List<T> interfaces = new List<T>();
            GameObject[] rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
            foreach (var rootGameObject in rootGameObjects)
            {
                T[] childrenInterfaces = rootGameObject.GetComponentsInChildren<T>();
                foreach (var childInterface in childrenInterfaces)
                {
                    interfaces.Add(childInterface);
                }
            }
            return interfaces;
        }

        public static List<GameObject> FindObjectsWithTag(this Transform parent, string tag)
        {
            List<GameObject> taggedGameObjects = new List<GameObject>();

            for (int i = 0; i < parent.childCount; i++)
            {
                Transform child = parent.GetChild(i);
                if (child.tag == tag)
                {
                    taggedGameObjects.Add(child.gameObject);
                }
                if (child.childCount > 0)
                {
                    taggedGameObjects.AddRange(FindObjectsWithTag(child, tag));
                }
            }
            return taggedGameObjects;
        }
        #endregion
    }
}
