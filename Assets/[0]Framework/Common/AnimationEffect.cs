﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEffect : MonoBehaviour
{
    [SerializeField]
    bool destroyAftherEnd = true;

    public void AnimationEnd()
    {
        if (destroyAftherEnd)
        {
            Destroy(gameObject);
        }

    }
}
