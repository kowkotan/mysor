﻿using UnityEngine;

namespace Framework.Common
{
    public class BaseBehavior : MonoBehaviour
    {
        #region Cache Componets

        Transform _transform = null;
        bool _transformCached = false;
        public Transform CachedTransform
        {
            get
            {
                if (!_transformCached)
                {
                    _transformCached = true;
                    _transform = GetComponent<Transform>();
                }
                return _transform;
            }
        }

        GameObject _gameobject = null;
        bool _goCached = false;
        public GameObject CachedGameObject
        {
            get
            {
                if (!_goCached)
                {
                    _goCached = true;
                    _gameobject = GetComponent<GameObject>();
                }
                return _gameobject;
            }
        }

        Rigidbody2D _rigidbody2D = null;
        bool _rb2dCached = false;
        public Rigidbody2D CachedRigidBody2D
        {
            get
            {
                if (!_rb2dCached)
                {
                    _rb2dCached = true;
                    _rigidbody2D = GetComponent<Rigidbody2D>();
                }
                return _rigidbody2D;
            }
        }

        Rigidbody _rigidbody = null;
        bool _rbCached = false;
        public Rigidbody CachedRigidBody
        {
            get
            {
                if (!_rbCached)
                {
                    _rbCached = true;
                    _rigidbody = GetComponent<Rigidbody>();
                }
                return _rigidbody;
            }
        }


        Collider _collider = null;
        bool _colliderCached = false;
        public Collider CachedCollider
        {
            get
            {
                if (!_colliderCached)
                {
                    _colliderCached = true;
                    _collider = GetComponent<Collider>();
                }
                return _collider;
            }
        }

        RectTransform _recttransform = null;
        bool _recttransformCached = false;
        public RectTransform CachedRectTransform
        {
            get
            {
                if (!_recttransformCached)
                {
                    _recttransformCached = true;
                    _recttransform = GetComponent<RectTransform>();
                }
                return _recttransform;
            }
        }

        AudioSource _audioSource = null;
        bool _audioSourceCached = false;
        public AudioSource CachedAudioSource
        {
            get
            {
                if (!_audioSourceCached)
                {
                    _audioSourceCached = true;
                    _audioSource = GetComponent<AudioSource>();
                }
                return _audioSource;
            }
        }

        #endregion

        protected virtual void Start()
        {
            this.Inject();
        }

    }
}
