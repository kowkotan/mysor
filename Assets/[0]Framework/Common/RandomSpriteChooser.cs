﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework.Utility;

[RequireComponent(typeof(SpriteRenderer))]
public class RandomSpriteChooser : MonoBehaviour
{
    [SerializeField] List<Sprite> sprites;

    void OnEnable()
    {
        LoadRandomSprite();
    }

    public void LoadRandomSprite()
    {
        GetComponent<SpriteRenderer>().sprite = sprites.Random();
    }
}
