﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolObjectManager : MonoBehaviour
{
    [SerializeField]
    GameObject indiPrefab;

    [SerializeField]
    int indicatorPoolSize;

    protected GameObject indiObject;

    [SerializeField]
   protected string indicatorId;

    void Start()
    {
        PoolManager.instance.CreatePool(indicatorId, indiPrefab, indicatorPoolSize);

    }

    public void CreateIndiOnRandomPosInRadius(Vector3 _pos, float _radius)
    {
        Vector3 indiCatorPos = new Vector3(_pos.x + Random.value * _radius, _pos.y + Random.value * _radius);
        CreateIndiOnPos(indiCatorPos);
    }

    public void CreateIndiOnPos(Vector3 _pos)
    {
        indiObject = PoolManager.instance.GetObject(indicatorId, _pos, Quaternion.identity);

    }

}
