﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework.Utility;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class SpriteChooser : MonoBehaviour
{
    [SerializeField] List<Sprite> sprites;


    public void LoadSprite(int _spriteNumber)
    {
        GetComponent<Image>().sprite = sprites[_spriteNumber];
    }
}
