﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Framework.Utility;

[RequireComponent(typeof(MeshRenderer))]
public class RandomTextureChooser : MonoBehaviour
{

    [SerializeField] Texture[] textures;

    void Start()
    {
        GetComponent<MeshRenderer>().material.mainTexture = textures.Random();
    }
}
