﻿using UnityEngine;

namespace Framework.Common
{
    public class DontDestroyOnLoad : MonoBehaviour
    {

        [SerializeField]
        bool destroyDuplicates = true;

        void Awake()
        {

            DontDestroyOnLoad(gameObject);
            if (destroyDuplicates)
            {
                if (FindObjectsOfType(GetType()).Length > 1)
                    Destroy(gameObject);
            }
        }

    }
}
