﻿using Adic;
using Framework.Managers.Event;

namespace Framework.Common
{
    
    public abstract class BaseMainContext : ContextRoot
    {
        public override void SetupContainers()
        {
            this.AddContainer<InjectionContainer>()
                .RegisterExtension<UnityBindingContainerExtension>()
                .RegisterExtension<EventCallerContainerExtension>()
                .RegisterExtension<CommanderContainerExtension>();
   
            BindConfigs();
            BindComponents();
            BindView();
            BindManagers();
            BindModels();
            BindControllers();
            BindCommands();
            BindGlobalControllers();

        }

        protected abstract void BindCommands();
        protected abstract void BindView();
        protected abstract void BindManagers();
        protected abstract void BindConfigs();
        protected abstract void BindModels();
        protected abstract void BindControllers();
        protected abstract void BindComponents();

        void BindGlobalControllers()
        {
            this.containers[0].Bind<MagicShopController>().ToSingleton();
              
        }

        public override void Init() { }
    }

}