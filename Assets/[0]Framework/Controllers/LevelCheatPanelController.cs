using System;
using Adic;
using Framework.Interfaces;

public class LevelCheatPanelController : IState, IDisposable, ITick, ITickLate
{
    [Inject] LevelCheatPanelView LevelCheatPanelView;
    [Inject] LevelModel levelModel;
    [Inject] UpdateManager updateManager;

    public void Load()
    {
        updateManager.AddTo(this);
        LevelCheatPanelView.Load();
    }

    public void Unload()
    {
        updateManager.RemoveFrom(this);
        LevelCheatPanelView.Unload();
    }

    public void Tick()
    {

    }

    public void TickLate()
    {
        LevelCheatPanelView.TickLate();
    }

    public void Dispose()
    {
        updateManager = null;
        LevelCheatPanelView = null;
        levelModel = null;
    }
}
