﻿//using System;
//using Adic;
//using Framework.Interfaces;

//public class DisableAdsPopupController : IState,IDisposable,ITick,ITickLate
//{
//    [Inject] DisableAdsPopupView  DisableAdsPopupView;
//    [Inject] DisableAdsPopupModel DisableAdsPopupModel;

//    [Inject] UpdateManager managerUpdate;

//    public void Load()
//    {
//        managerUpdate.AddTo(this);
//        DisableAdsPopupModel.OnLoad();
//        DisableAdsPopupView.Load();

//        DisableAdsPopupView.SetPopupText(DisableAdsPopupModel.GetPopUpText());
//    }

//    public void Tick()
//    {
//        if (DisableAdsPopupView.closeButtonClicked)
//            DisableAdsPopupModel.CloseBtnClicked();

//        if (DisableAdsPopupView.disableButtonClicked)
//            DisableAdsPopupModel.DisableAdBtnClicked();

//        if (DisableAdsPopupView.restorePurchasesButtonClicked)
//            DisableAdsPopupModel.RestoreBtnClicked();

//        if (DisableAdsPopupModel.IsLoadingStarted)
//            DisableAdsPopupView.Loading(true);
        
//        if (DisableAdsPopupModel.IsLoadingFinished)
//            DisableAdsPopupView.Loading(false);

//        if (DisableAdsPopupModel.IsPopupClosed)
//            Unload();
//    }

//    public void TickLate()
//    {
//        DisableAdsPopupModel.MyLateUpdate();
//        DisableAdsPopupView.MyLateUpdate();
//    }


//    public void Unload()
//    {
//        DisableAdsPopupModel.OnUnload();
//        DisableAdsPopupView.Unload();
//        managerUpdate.RemoveFrom(this);
//    }

//    public void Dispose()
//    {
//        DisableAdsPopupView = null;
//        DisableAdsPopupModel = null;
//    }
//}
