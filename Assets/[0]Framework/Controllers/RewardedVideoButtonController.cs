﻿using System;
using Framework.Interfaces;
using Adic;

public class RewardedVideoButtonController : IDisposable, ITick, ITickLate
{
    RewardedVideoButtonView RewardedVideoButtonView;
    [Inject] RewardedVideoButtonModel RewardedVideoButtonModel;

    public bool OnRewardedVideoFinishedStarted;
    public bool OnRewardedVideoFinishedSucces;
    public bool OnRewardedVideoFinishedFailled;

    public void Load(RewardedVideoButtonView _rewardedVideoButtonView)
    {
        RewardedVideoButtonView = _rewardedVideoButtonView;
        RewardedVideoButtonModel.Init();
        UpdateRewardBtnActiveStatus();
        UpdateRewardBtnActiveStatus();
    }

    public bool GetRewardedBtnActiveStatus()
    {
        return RewardedVideoButtonModel.RewardedBtnActiveStatus();
    }

    public void UpdateRewardBtnActiveStatus()
    {
        RewardedVideoButtonView.UpdateRewardButtonActiveStatus(RewardedVideoButtonModel.RewardedBtnActiveStatus());
    }

    public void Unload()
    {
        RewardedVideoButtonView.Unload();
        RewardedVideoButtonModel.Unload();
        OnRewardedVideoFinishedStarted = false;
        OnRewardedVideoFinishedSucces = false;
        OnRewardedVideoFinishedFailled = false;
    }
    public void Tick()
    {
        if (RewardedVideoButtonView.rewardVideoBtnClicked)
            RewardedVideoButtonModel.ButtonClicked();

        if (RewardedVideoButtonModel.OnRewardedVideoShowSucces)
            OnRewardedVideoFinishedSucces = true;

        if (RewardedVideoButtonModel.OnRewardedVideoShowError)
            OnRewardedVideoFinishedFailled = true;

        if (RewardedVideoButtonModel.OnRewardedVideoStarted)
            OnRewardedVideoFinishedStarted = true;

        if (RewardedVideoButtonModel.OnRewardedVideoIsLoaded)
        {
            UpdateRewardBtnActiveStatus();
            RewardedVideoButtonModel.OnRewardedVideoIsLoaded = false;
        }

    }

    public void TickLate()
    {
        OnRewardedVideoFinishedStarted = false;
        OnRewardedVideoFinishedSucces = false;
        OnRewardedVideoFinishedFailled = false;

        RewardedVideoButtonModel.TickLate();
        RewardedVideoButtonView.TickLate();
    }

    public void Dispose()
    {
        RewardedVideoButtonView = null;
        RewardedVideoButtonModel = null;
  

    }
}
