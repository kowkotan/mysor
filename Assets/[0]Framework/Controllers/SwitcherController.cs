using System;
using Adic;
using Framework.Interfaces;
using Framework.Views;

public class SwitcherController: IDisposable,ITick,ITickLate
{
    ButtonSwitcherView SwitcherView;
    SwitchModel SwitchModel;
     UpdateManager updateManager;

    public SwitcherController( SwitchModel _switchModel)
    {
        SwitchModel = _switchModel;
    }


    public void Load(ButtonSwitcherView _buttonSwitcherView, UpdateManager _updateManager)
    {
        SwitcherView = _buttonSwitcherView;
        SwitchModel.LoadCurrentStateValue();
        updateManager = _updateManager;
        updateManager.AddTo(this);
    }

    public void Unload()
    {
        updateManager.RemoveFrom(this);
    }

    public void Tick()
    {
        if (SwitcherView.onStateChanged)
            SwitchModel.StateChanged(SwitcherView.state);

        if (SwitchModel.currentSwitchStateValueLoaded)
            SwitcherView.Switch(SwitchModel.state);
    }

    public void TickLate()
    {
        SwitchModel.MyLateUpdate();
        SwitcherView.MyLateUpdate();
    }

    public void Dispose()
    {
        SwitcherView = null;
        SwitchModel = null;
        updateManager = null;
    }
}
