﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Adic;
using Framework.Common;
using Framework.Views;
using Framework.Interfaces;
using System;

namespace Framework.Controllers.Loader
{
    public class ControllerLoader : MonoBehaviour, ISceneLoader, IDisposable
    {
        [Inject] protected LoaderView loadingView;

        string nameOfTheLoadedScene;
        protected bool waitingToStartLoading;
        LoadSceneMode loadSceneMode;

         void Start()
        {
            this.Inject();

        }

        public virtual void StartLoad(string _nameOfTheLoadedScene, LoadSceneMode _loadSceneMode)
        {
            loadingView.Load();
            Time.timeScale = 0;
            loadSceneMode = _loadSceneMode;
            nameOfTheLoadedScene = _nameOfTheLoadedScene;
            if (!waitingToStartLoading) LoadNextLevel();

        }

        protected virtual void LoadNextLevel()
        {
            Debug.Log("Загрузка началась");
            Time.timeScale = 1;
            SceneManager.LoadScene(nameOfTheLoadedScene, loadSceneMode);
            loadingView.Unload();
            Debug.Log("Загрузка завершилась");
        }

        public void Dispose()
        {
            loadingView = null;
        }
    }
}
