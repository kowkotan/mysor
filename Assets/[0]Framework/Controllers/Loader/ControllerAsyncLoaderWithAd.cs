﻿using Adic;
using Framework.Interfaces;
using Framework.Managers.Ads;
using UnityEngine.SceneManagement;

namespace Framework.Controllers.Loader
{
    public class ControllerAsyncLoaderWithAd : ControllerLoader, IInterstitialAdListener
    {
        [Inject] BaseAdSystem adSystem;

        bool waitingToFinishAdLoading;

        public override void StartLoad(string _nameOfTheLoadedScene, LoadSceneMode _loadSceneMode)
        {
            waitingToStartLoading = true;
            base.StartLoad(_nameOfTheLoadedScene,_loadSceneMode);

            adSystem.HideBanner();

            adSystem.SetInterstitialCallbacks(this);

            adSystem.ShowInterstetial((bool isLoaded) =>
            {
                if (isLoaded)
                {
                    waitingToFinishAdLoading = true;
                }
                else
                {
                    LoadNextLevel();
                }
            });
        }

        public void OnInterstitialClicked() { }
        public void OnInterstitialClosed()
        {
            if (waitingToFinishAdLoading)
            {
                LoadNextLevel();
            }
        }
        public void OnInterstitialLoaded(bool isPrecache) { }
        public void OnInterstitialShown()
        {
            if (waitingToFinishAdLoading)
            {
                LoadNextLevel();
            }
        }

        protected override void LoadNextLevel()
        {
            waitingToFinishAdLoading = false;
            base.LoadNextLevel();
        }

        public void OnInterstitialFailedToLoad()
        {

            if (waitingToFinishAdLoading)
            {
                LoadNextLevel();
            }
        }
    }
}
