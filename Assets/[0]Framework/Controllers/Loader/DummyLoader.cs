﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Adic;
using Framework.Common;
using Framework.Views;
using Framework.Interfaces;

namespace Framework.Controllers.Loader
{
    public class DummyLoader : BaseBehavior, ISceneLoader
    {

        public virtual void StartLoad(string _nameOfTheLoadedScene, LoadSceneMode _loadSceneMode)
        {
            Debug.Log("Загрузка началась");
            SceneManager.LoadScene(_nameOfTheLoadedScene, _loadSceneMode);
            Debug.Log("Загрузка завершилась");

        }
    }
}
