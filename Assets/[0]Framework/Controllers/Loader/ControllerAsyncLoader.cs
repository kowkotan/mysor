﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Adic;
using Framework.Common;
using Framework.Views;
using Framework.Interfaces;
using System;

namespace Framework.Controllers.Loader
{
    public class ControllerAsyncLoader : MonoBehaviour, ISceneLoader, IDisposable
    {
        [Inject] protected LoaderView loadingView;
        ILoadingProgressListener loadingProgressListener;

        string nameOfTheLoadedScene;
        protected bool waitingToStartLoading;
        LoadSceneMode loadSceneMode;

          void Start()
        {
      
            loadingProgressListener = loadingView.GetComponent<ILoadingProgressListener>();
        }

        public virtual void StartLoad(string _nameOfTheLoadedScene,LoadSceneMode _loadSceneMode)
        {
            loadingView.Load();
            Time.timeScale = 0;
            loadSceneMode = _loadSceneMode;
            nameOfTheLoadedScene = _nameOfTheLoadedScene;
            if (!waitingToStartLoading) LoadNextLevel();

        }

        protected virtual void LoadNextLevel()
        {
            StartCoroutine(LoadAsync(nameOfTheLoadedScene));

        }

        IEnumerator LoadAsync(string sceneName)
        {
            Debug.Log("Загрузка началась");
            Time.timeScale = 1;
            AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName, loadSceneMode);
            while (!operation.isDone)
            {
                float progress = Mathf.Clamp01(operation.progress / .9f);
               
                if (loadingProgressListener != null)
                    loadingProgressListener.SetLoadingProgress(progress);

                yield return null;
            }
            Debug.Log("Загрузка завершилась");
            loadingView.Unload();

        }

        public void Dispose()
        {
            loadingView = null;
        }
    }
}
