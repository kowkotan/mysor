using System;
using Adic;
using Framework.Interfaces;
using Framework.Views;

public class VibroSwitcherController : SwitcherController
{
    public VibroSwitcherController( [Inject("vibroSwitcherModel")] SwitchModel _switchModel) : base( _switchModel){}
}
