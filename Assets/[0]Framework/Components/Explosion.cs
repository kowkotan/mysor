﻿using Framework.Utility;
using UnityEngine;

namespace Framework.Components
{
    [RequireComponent(typeof(CircleCollider2D))]
    public class Explosion : MonoBehaviour
    {

        [SerializeField] float explosion_force;
        [SerializeField] float explosion_rate;
        [SerializeField] float explosion_max_sixe;
        [SerializeField] float currentRadius;


        bool exploded;

        Rigidbody2D rb;
        CircleCollider2D expRadius;

        public void Awake()
        {
            expRadius = GetComponent<CircleCollider2D>();
        }

        private void FixedUpdate()
        {
            if (exploded)
            {
                if (currentRadius < explosion_max_sixe)
                {
                    currentRadius += explosion_rate;
                }
                else
                {
                    Destroy(gameObject);
                }
                expRadius.radius = currentRadius;
            }
        }

        public void Boom()
        {
            exploded = true;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            Rigidbody2D rb;
            if (exploded)
            {
                rb = collision.gameObject.GetComponent<Rigidbody2D>();
                if (rb != null)
                {
                    rb.AddExplosionForce2D( explosion_force, transform.position, currentRadius);
                }
            }
        }
    }
}
