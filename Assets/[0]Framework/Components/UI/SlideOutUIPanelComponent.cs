﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class SlideOutUIPanelComponent : MonoBehaviour
{
    [SerializeField]
    bool horizontalSide;
    [SerializeField]
    bool verticalSide;

    [SerializeField]
    float openSpeed;

    [SerializeField]
    float closeSpeed;

    [SerializeField]
    Ease openEase;

    [SerializeField]
    Ease closeEase;

    [SerializeField]
    bool isOpened;

    [SerializeField]
    Vector3 defPos;

    RectTransform rectTransform;


    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        defPos = rectTransform.anchoredPosition;
        ClosePanel();

    }

    public void SwitchState()
    {

        if(isOpened)
        {
            ClosePanel();
        }
        else
        {
            OpenPanel();
        }


    }

    public void OpenPanel()
    {
        isOpened = true;
        MovePanel(getPosVecor(), openSpeed, openEase);
    }

    public void ClosePanel()
    {

        isOpened = false;
        MovePanel(getPosVecor(), closeSpeed, closeEase);

    }

    Vector3 getPosVecor()
    {
        float newPosX = defPos.x;
        float newPosY = defPos.y;

        if (isOpened == false)
        {
            if (horizontalSide)
                newPosX = -defPos.x;
            if (verticalSide)
                newPosY = -defPos.y;
        }

        return new Vector3(newPosX, newPosY);
    }

    void MovePanel(Vector3 pos,float speed,Ease ease)
    {
        rectTransform.DOAnchorPos(pos, speed).SetEase(ease);
    }

}
