﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class SlideToPointUIPanelComponent : MonoBehaviour
{
  

    [SerializeField]
    float openSpeed;

    [SerializeField]
    float closeSpeed;

    [SerializeField]
    Ease openEase;

    [SerializeField]
    Ease closeEase;

    bool isOpened = false;

    [Tooltip(" Точка для состояния открытия ")]
    [SerializeField]
    RectTransform targetPoint;

    Vector3 defPos;

    RectTransform rectTransform;


    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
        defPos = rectTransform.anchoredPosition;
        ClosePanel();

    }

    public void SwitchState()
    {

        if(isOpened)
        {
            ClosePanel();
        }
        else
        {
            OpenPanel();
        }


    }

    public void OpenPanel()
    {
        isOpened = true;
        MovePanel(new Vector3(targetPoint.anchoredPosition.x, targetPoint.anchoredPosition.y), openSpeed, openEase);
    }

    public void ClosePanel()
    {
        isOpened = false;
        MovePanel(new Vector3(defPos.x,defPos.y), closeSpeed, closeEase);
    }

    //Vector3 getPosVecor()
    //{
    //    float newPosX = defPos.x;
    //    float newPosY = defPos.y;

    //    if (isOpened == false)
    //    {
    //        newPosX = targetPoint.anchoredPosition.x;
    //        newPosY = targetPoint.anchoredPosition.y;
    //    }

    //    return new Vector3(newPosX, newPosY);
    //}

    void MovePanel(Vector3 pos,float speed,Ease ease)
    {
        rectTransform.DOAnchorPos(pos, speed).SetEase(ease);
    }

}
