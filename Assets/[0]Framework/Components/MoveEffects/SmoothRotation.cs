﻿using System.Collections;
using System.Collections.Generic;
using Adic;
using Framework.Common;
using Framework.Interfaces;
using UnityEngine;

public class SmoothRotation : BaseBehavior, ITick
{
    [SerializeField]
    Vector3 rotationVector;

    [Inject] UpdateManager updateManager;

    void Start()
    {
        base.Start();
        updateManager.AddTo(this);
    }

    public void Tick()
    {
        CachedTransform.Rotate(rotationVector, Time.deltaTime);
    }

    private void OnDestroy()
    {
        updateManager.RemoveFrom(this);
        updateManager = null;
    }
}
