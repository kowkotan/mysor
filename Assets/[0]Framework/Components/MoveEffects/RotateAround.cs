﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAround : MoveEffect
{
    [SerializeField] float rotateSpeeed;
   public Transform aroundPoint;
    [SerializeField] Vector3 direction;

    Vector3 startPos;
    Quaternion startRot;

    public void SetAroundPointPosition( Vector3 aroundPointPos)
    {
        if(aroundPoint != null)
        {
            aroundPoint.transform.position = aroundPointPos;
        }
    }

    public override void Reset()
    {
        transform.position = startPos;
        transform.rotation = startRot;
    }

  

    public override void StartEffect()
    {
        base.StartEffect();
        startPos = transform.position;
        startRot = transform.rotation;
    }

    void Update()
    {
        if (effectIsOn)
        {
            transform.RotateAround(aroundPoint.position, rotateSpeeed * Time.smoothDeltaTime);
        }

    }
}
