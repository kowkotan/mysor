﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MoveEffect : MonoBehaviour
{
    [SerializeField] bool playOnEnable;

    protected bool effectIsOn;
    protected virtual void OnEnable()
    {
        if (playOnEnable)
        {
            StartEffect();
        }
    }
    public virtual void StartEffect()
    {
        effectIsOn = true;
    }
    public abstract void Reset();
    public virtual void StopEffect()
    {
        effectIsOn = false;
    }

}
