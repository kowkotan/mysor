﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionPingPongChanger : MoveEffect
{
    Vector3 starPos;

    [SerializeField]
    Vector3 minPosOffset;
    [SerializeField]
    Vector3 maxPosOffset;

    Vector3 minPos;
    Vector3 maxPos;


   public float moveSpeed;
    

    int direction = 1;

    void ChangeDirection()
    {
        if( direction == 1)
        {
            direction = -1;
        }
        else
        {
            direction = 1;
        }
    }

    public override void Reset()
    {
        transform.position = starPos;
    }

    public override void StartEffect()
    {
        base.StartEffect();
        starPos = transform.position;
        minPos = transform.position + minPosOffset;
        maxPos = transform.position + maxPosOffset;
    }

    void Update()
    {
        if (effectIsOn)
        {

            if (minPosOffset.x != 0 || maxPosOffset.x != 0 )
            {
                if (transform.position.x <= minPos.x)
                {
                    ChangeDirection();
                }
                else if (transform.position.x >= maxPos.x)
                {
                    ChangeDirection();
                }
            }

            if (minPosOffset.y != 0 || maxPosOffset.y != 0)
            {
                if (transform.position.y <= minPos.y)
                {
                    ChangeDirection();
                }
                else if (transform.position.y >= maxPos.y)
                {
                    ChangeDirection();
                }
            }

            if (minPosOffset.z != 0 || maxPosOffset.z != 0)
            {
                if (transform.position.z <= minPos.z)
                {
                    ChangeDirection();
                }
                else if (transform.position.z >= maxPos.z)
                {
                    ChangeDirection();
                }
            }
            transform.Translate(0, moveSpeed*direction *Time.smoothDeltaTime, 0);
        }
    }
}
