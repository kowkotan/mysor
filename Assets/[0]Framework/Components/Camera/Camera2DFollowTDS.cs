﻿using Framework.Utility;
using UnityEngine;

namespace Framework.Common.Camera
{
    [RequireComponent(typeof(UnityEngine.Camera))]
    public class Camera2DFollowTDS : MonoSingleton<Camera2DFollowTDS>
    {

        private enum Mode { Player, Cursor };

        [SerializeField] private Mode face; // вектор смещения, относительно "лица" персонажа или положения курсора
        [SerializeField] private SpriteRenderer boundsMap; // спрайт, в рамках которого будет перемещаться камера
        [SerializeField] private bool useBounds = true; // использовать или нет, границы для камеры
        [SerializeField] private float smooth = 2.5f; // сглаживание при следовании за персонажем
        [SerializeField] private float offset;

        
        Transform _target;
        private Vector3 min, max, direction;
        private UnityEngine.Camera cam;


        void OnEnable()
        {
            cam = GetComponent<UnityEngine.Camera>();
            //cam.orthographic = true;
            CalculateBounds();
        }

        public override void Init()
        {

        }

        // переключатель, для использования из другого класса
        public void UseCameraBounds(bool value)
        {
            useBounds = value;
        }

        // найти персонажа, если он был уничтожен, а потом возрожден
        public void SetTarget(Transform newTarget)
        {
            _target = newTarget;
            if (_target) transform.position = new Vector3(_target.position.x, _target.position.y - 7, transform.position.z);
        }

        // если в процессе игры, было изменено разрешение экрана
        // или параметр "Orthographic Size", то следует сделать вызов данной функции повторно
        public void CalculateBounds()
        {
            if (boundsMap == null) return;
            Bounds bounds = Camera2DBounds();
            min = bounds.max + boundsMap.bounds.min;
            max = bounds.min + boundsMap.bounds.max;
        }

        Bounds Camera2DBounds()
        {
            float height = cam.orthographicSize * 2;
            return new Bounds(Vector3.zero, new Vector3(height * cam.aspect, height, 0));
        }

        Vector3 MoveInside(Vector3 current, Vector3 pMin, Vector3 pMax)
        {
            if (!useBounds || boundsMap == null) return current;
            current = Vector3.Max(current, pMin);
            current = Vector3.Min(current, pMax);
            return current;
        }

        Vector3 Mouse()
        {
            Vector3 mouse = UnityEngine.Input.mousePosition;
            mouse.z = -transform.position.z;
            return cam.ScreenToWorldPoint(mouse);
        }

        void Follow()
        {
            if (face == Mode.Player) direction = _target.right; else direction = (Mouse() - _target.position).normalized;
            Vector3 position = _target.position + direction * offset;
            position.z = transform.position.z;
            position = MoveInside(position, new Vector3(min.x, min.y, position.z), new Vector3(max.x, max.y, position.z));
            transform.position = Vector3.Lerp(transform.position, position, smooth * Time.deltaTime);
        }

        void LateUpdate()
        {
            if (_target)
            {
                Follow();
            }
        }
    }
}