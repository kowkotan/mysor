﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Framework.Common.Camera
{
    [RequireComponent(typeof(UnityEngine.Camera))]
    public class Camera3DFollow : MonoBehaviour
    {
        [SerializeField]
        Transform target;

        [Range(0.01f, 1.0f)]
        public float Smooth = 0.5f;

        Vector3 offset;

        private void Start()
        {

        }

        public void SetTarget(Transform newTarget)
        {
            target = newTarget;
            offset = transform.position - target.position;
        }
  
        void LateUpdate()
        {
            if (target != null)
            {
                Vector3 moveVector = target.position + offset;

                transform.position = Vector3.Slerp(transform.position, moveVector, Smooth);
            }
        }
    }
}
