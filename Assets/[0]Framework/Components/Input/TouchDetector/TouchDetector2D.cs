using UnityEngine.EventSystems;
using System.Linq;
using UnityEngine;
using Framework.Interfaces;

public class TouchDetector2D : MonoBehaviour {

    Vector2 mousePos;
    Vector2 lastMousePos;
    [SerializeField] LayerMask collisionLayerMask;
    [SerializeField] bool ignoreUI;

	void Update () {

        if (Input.GetMouseButton(0) )
        {
            if(ignoreUI == true && EventSystem.current.IsPointerOverGameObject() == true || Camera.main == null)
            {
                return;
            }

       
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (lastMousePos != mousePos)
            {

                var origin = new Vector2(mousePos.x, mousePos.y);
                RaycastHit2D[] hits = Physics2D.RaycastAll(origin, Vector2.zero, 0f, collisionLayerMask).OrderBy(h => h.distance).ToArray();
               
                if (  hits.Length > 0 && hits[hits.Length-1].collider != null )
                {
                    Debug.Log("dsds");
                    IRayCastListiner listiner = hits[hits.Length - 1].collider.GetComponent<IRayCastListiner>();
                    if (listiner != null)
                    {
                        listiner.RayCastClick();
                        lastMousePos = mousePos;

                    }
                }
            }
        }
    }
}
