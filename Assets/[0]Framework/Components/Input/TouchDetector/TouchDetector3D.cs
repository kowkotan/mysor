﻿using UnityEngine.EventSystems;
using System.Linq;
using UnityEngine;
using Framework.Interfaces;

namespace Framework.Components.Input.TouchDetector
{
    public class TouchDetector3D : MonoBehaviour
    {
        Vector2 mousePos;
        Vector2 lastMousePos;
        [SerializeField] LayerMask collisionLayerMask;

        void Update()
        {

            if (UnityEngine.Input.GetMouseButton(0) && EventSystem.current.IsPointerOverGameObject() == false)
            {

                mousePos = UnityEngine.Input.mousePosition;
   
                if (lastMousePos != mousePos)
                {
                    var origin = new Vector2(mousePos.x, mousePos.y);

                    Ray ray = Camera.main.ScreenPointToRay(UnityEngine.Input.mousePosition);
                    RaycastHit[] hits = Physics.RaycastAll(ray, 1000.0f, collisionLayerMask).OrderBy(h => h.distance).ToArray(); 

                    if (hits.Length > 0 && hits[hits.Length - 1].collider != null)
                    {
                        for(int i = 0; i < hits.Length; i++)
                        {
                            IRayCastListiner listiner = hits[i].collider.GetComponent<IRayCastListiner>();

                            if (listiner != null)
                            {
                                listiner.RayCastClick();
                                lastMousePos = mousePos;
                            }
                        }

                    }
                }
            }
        }
    }
}
