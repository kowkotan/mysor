﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LevelUpWindowView : MonoBehaviour
{
    [SerializeField]
    Text nextLevelTxt;
    [SerializeField]
    Text softValueText;
    [SerializeField]
    Text hardValueText;
    [SerializeField]
    GameObject unlockedItems;
    [SerializeField]
    GameObject unlockedItemTemplate;

    [SerializeField]
    Button getRewardBtn;

    public UnityAction OnRewardBtnClicked;

    void Start()
    {
        getRewardBtn.onClick.AddListener(OnRewardBtnClicked);
    }

    public void SetSoftRewardValue(int _softValue )
    {
        softValueText.text = _softValue.ToString();
    }

    public void SetHardRewardValue(int _hardValue)
    {
        hardValueText.text = _hardValue.ToString();
    }

    public void SetUnlockedItems(Dictionary<string, Sprite> items)
    {
        foreach(var item in items)
        {
            GameObject unlockedItem = Instantiate(unlockedItemTemplate, unlockedItems.transform);
            unlockedItem.SetActive(true);
            MShop_ExchangeCell itemData = unlockedItem.GetComponent<MShop_ExchangeCell>();
            if(itemData != null)
            {
                itemData.SetupTitleIcon(item.Key, item.Value);
            }
            Debug.Log("Unlocked item : " + item.Key);
        }
    }

    public void SetLevelTxt(int _level )
    {
        nextLevelTxt.text = _level.ToString();
    }

    private void OnDestroy()
    {
        if (OnRewardBtnClicked != null)
        {
            getRewardBtn.onClick.RemoveListener(OnRewardBtnClicked);
        }
    }
}
