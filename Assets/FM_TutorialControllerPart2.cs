﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FM_TutorialControllerPart2 : MonoBehaviour
{
    [SerializeField] FM_Trashbox trashbox;
    [SerializeField] Button OpenTrashboxBtn;
    [SerializeField] Button CloseTrashboxBtn;

    [SerializeField] Button closeWhButton;

    [SerializeField] Button openFactoryButton;

    [SerializeField] Button upMenuFactoryButton;

    [SerializeField] Button upFactoryButton;

    [SerializeField] Button speedUpBtn;

    [SerializeField] Button startProcessProductBtn;

    [SerializeField] Button speedUpProcessProductBtn;

    [SerializeField] Button sellProductBtn;

    [SerializeField] Button openFoodFacButton;

    [SerializeField] FM_Factory factory;



    void Start()
    {
        if (PlayerPrefs.GetInt("factoryTutorialPart2Ready") == 0)
            return;

        if (PlayerPrefs.GetInt("factoryTutorialPart2Competed") == 1)
            return;

        Invoke("LoadTutorial",0.5f);



    }

    void LoadTutorial()
    {
        TutorialLoader.instance.Load("fTut8", true);
        //TutorialController.instance.HideNextButton();

        TutorialEvents.OnTutorialComplete.AddListener(loadTut8Plus);
    }

    void loadTut8Plus()
    {
        TutorialEvents.OnTutorialComplete.RemoveAllListeners();
        TutorialLoader.instance.Load("fTut13", true, openFactoryButton);
        TutorialController.instance.HideNextButton();
        TutorialEvents.OnTutorialComplete.AddListener(Tut13Comp);
    }

    void Tut8Comp()
    {
        TutorialEvents.OnTutorialComplete.RemoveAllListeners();
        TutorialLoader.instance.Load("fTut9", true, openFactoryButton);
        TutorialEvents.OnTutorialComplete.AddListener(Tut13Comp);
    }

    void Tut9Comp()
    {
        TutorialEvents.OnTutorialComplete.RemoveAllListeners();
        TutorialLoader.instance.Load("fTut10", true, upMenuFactoryButton);
        TutorialEvents.OnTutorialComplete.AddListener(Tut101Comp);
        upFactoryButton.interactable = false;
    }

    void Tut101Comp()
    {
        TutorialEvents.OnTutorialComplete.RemoveAllListeners();
        TutorialLoader.instance.Load("fTut10.1", true);
        TutorialEvents.OnTutorialComplete.AddListener(Tut10Comp);
    }

    void Tut10Comp()
    {
        TutorialEvents.OnTutorialComplete.RemoveAllListeners();
        upFactoryButton.interactable = true;
        TutorialLoader.instance.Load("fTut11", true, upFactoryButton);
        TutorialEvents.OnTutorialComplete.AddListener(Tut11Comp);

    }

    void Tut11Comp()
    {
        TutorialEvents.OnTutorialComplete.RemoveAllListeners();
        TutorialLoader.instance.Load("fTut12", true, speedUpBtn);
        TutorialEvents.OnTutorialComplete.AddListener(Tut12Comp);
    }

    void Tut12Comp()
    {
        TutorialEvents.OnTutorialComplete.RemoveAllListeners();
        TutorialLoader.instance.Load("fTut13", true, openFactoryButton);
        TutorialEvents.OnTutorialComplete.AddListener(Tut13Comp);
    }

    void Tut13Comp()
    {
        TutorialEvents.OnTutorialComplete.RemoveAllListeners();
        Invoke("Open14Tut", 0.15f);
      
    }

    void Open14Tut()
    { 
        TutorialLoader.instance.Load("fTut14.1", true, startProcessProductBtn);
        TutorialEvents.OnTutorialComplete.AddListener(Tut14Comp);
    }

    void Tut14Comp()
    {
        TutorialEvents.OnTutorialComplete.RemoveAllListeners();
        TutorialLoader.instance.Load("fTut15", true);
        //TutorialEvents.OnTutorialComplete.AddListener(Tut15Comp);
        PlayerPrefs.SetInt("factoryTutorialPart2Competed", 1);
    }

    void Tut15Comp()
    {
        TutorialEvents.OnTutorialComplete.RemoveListener(Tut15Comp);
        TutorialLoader.instance.Load("fTut16", true);
        TutorialEvents.OnTutorialComplete.AddListener(Tut16Comp);

    }

    void Tut16Comp()
    {
        TutorialEvents.OnTutorialComplete.RemoveAllListeners();
        TutorialLoader.instance.Load("fTut17", true);
        PlayerPrefs.SetInt("factoryTutorialPart2Competed", 1);
    }
}
