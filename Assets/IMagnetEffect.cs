﻿public interface IMagnetEffect
{
    void StartMagnetEffect();
    void StopMagnetEffect();
}