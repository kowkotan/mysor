﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationsController : MonoBehaviour
{
    public void OpenLocationsUI()
    {
        string pathToResource = "GlobalWindows/LocationControllerUI";
        Instantiate(Resources.Load(pathToResource));
    }
}
