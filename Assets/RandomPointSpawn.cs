﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPointSpawn : MonoBehaviour
{
    [SerializeField] List<Transform> spawnedObjects;

    [SerializeField] List<Transform> spawnPoints; 

    void Start()
    {
        for(int i = 0; i < spawnedObjects.Count;i++)
        {
            int randomPointIndex = Random.Range(0, spawnPoints.Count - 1);
            spawnedObjects[i].position = spawnPoints[randomPointIndex].position;
            spawnPoints.RemoveAt(randomPointIndex);
        }
    }

}
