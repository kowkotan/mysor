﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Adic;

public class LevelPopup : MonoBehaviour
{
    [SerializeField] LevelUpProgressBar levelModel;

    [SerializeField] Text popupText;
    [SerializeField] GameObject popupButton;

    float testr;
    public void Show()
    {
        this.gameObject.SetActive(true);
        popupText.text = "Заработайте ещё " + (int)levelModel.levelDifference + " для перехода на следующий уровень!";
        popupButton.SetActive(false);
        Invoke("StartTimer", 1.5f);
    }

    void StartTimer()
    {
        this.gameObject.SetActive(false);
        popupButton.SetActive(true);
    }
}
